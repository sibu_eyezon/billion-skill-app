<section class="p-0">
	<div class="swiper-container height-700-responsive swiper-arrow-hover swiper-slider-fade">
		<div class="swiper-wrapper">
			<!-- slide 1-->
			<div class="swiper-slide bg-overlay-dark-2" style="background-image:url(<?php echo base_url()?>assets/images/bg/05.jpg); background-position: center center; background-size: 100% 100%;">
				<div class="container h-100">
					<div class="row d-flex h-100">
						<div class="col-lg-12 col-xl-12 mr-auto slider-content justify-content-center align-self-center align-items-start text-center">
							<h5 class="animated fadeInUp dealy-500 display-12 display-md-12 display-lg-5 font-weight-bold text-white">Your Search for a Better Career End Here.</h5>
							<h5 class="animated fadeInUp dealy-1000 text-white display-8 display-md-6  mb-2 my-md-4">Enhance your skills, Enrich your career</h5>
							<div class="animated fadeInUp mt-3 dealy-1500"><a href="https://learn.techmagnox.com/" class="btn btn-grad">Register Now</a> </div>
						</div>
					</div>
				</div>
			</div>
			<!-- slide 2-->
			<div class="swiper-slide bg-overlay-dark-2" style="background-image:url(<?php echo base_url()?>assets/images/bg/01.jpg); background-position: center top; background-size: 100% 100%;">
				<div class="container h-100">
					<div class="row d-flex h-100">
						<div class="col-md-10 justify-content-center align-self-center align-items-start mx-auto">
							<div class="slider-content text-center ">
								<h6 class="animated fadeInUp dealy-500 display-8 display-md-12 display-lg-5 font-weight-bold text-white ">Learn Anywhere, Anything, Anytime  </h6>
								<h6 class="animated fadeInUp dealy-1000 display-12 display-md-12 display-lg-6  text-white">Associate with Big Brands for better Recognitions.</h6>
								<div class="animated fadeInUp mt-3 dealy-1500"><a href="https://learn.techmagnox.com/" class="btn btn-grad">Register Now</a></div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- Slider buttons -->
		<div class="swiper-button-next"><i class="ti-angle-right"></i></div>
		<div class="swiper-button-prev"><i class="ti-angle-left"></i></div>
		<div class="swiper-pagination"></div>
	</div>
</section>

<section class="client bg-grad px-5">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<h2  style="color:#fff">Associations </h2>
				<h3 class="feature-box-title text-white"> 20+ Institutes </h3>
				<a class="mt-3" href="<?php echo site_url('home/portfolio'); ?>" style="color:#fff">Know more!</a>
			</div>
			
			<div class="col-md-10">
				
				<div class="owl-carousel owl-grab arrow-hover arrow-dark" data-margin="40" data-arrow="true" data-dots="false" data-items-xl="6" data-items-lg="5" data-items-md="4" data-items-sm="3" data-items-xs="2">
					<div class="item"><img src="<?php echo base_url();?>assets/images/clients/01-light.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/clients/02-light.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/clients/03-light.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/clients/04-light.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/clients/5433.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/clients/06-light.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/clients/icfai.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/clients/logo.png" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog bg-light">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="title text-left">
					
					<h3>Certification Programs</h3>
					<p>Learn the Certification Training Programs from the Best Teachers on the most updated topics.</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel post-style-3 arrow-dark arrow-hover" data-dots="false" data-items-xl="3" data-items-lg="3" data-items-md="2" data-items-sm="2" data-items-xs="1">
					<?php
					if(!empty($programs)){
						$i=1;
						foreach($programs as $prow){
							$prog_id = $prow->id;
							$title = str_replace(" ","_",strtolower(trim($prow->title)));
							$fee = trim($prow->feetype);
					?>
					<div class="item">
						<div class="post">
							<div class="post-info">
                            
								
								<div class="post-author"><?php echo "Start Date:". date('jS M Y',strtotime($prow->start_date)); ?></div><span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!"><?php echo trim($prow->category); ?></a></span>
                                <a class="post-title" href="<?php echo 'https://learn.techmagnox.com/programDetails/?id='.base64_encode($prow->id); ?>" target="_blank"><?php echo ucwords(str_ireplace("_"," ",$title)); ?></a>
								
								<p class="mb-0">
								<?php
									  $dur = intval(trim($prow->duration));
									  echo 'Duration: '.$dur.' '.trim($prow->dtype).(($dur==1)? '':'s').'
									  <h6 class="text-left">'.(($fee=='Paid')? 'Rs '.$prow->total_fee : $fee).'</h6>
									  <h6 class="text-left">Start Date: '.date('jS M Y',strtotime($prow->start_date)).'</h6>';
								?>
								</p>
                                
							</div>
						</div>
					</div>
					<?php 
						$i++; }
						}
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog bg-white">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12 mx-auto">
				<div class="title text-left">
					<h3>Enhance your Skills and Knowledge </h3>
					<p>Enhance your skills through the Post Graduate Programs, Workshops and more.. </p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel arrow-dark arrow-hover" data-dots="false" data-items-xl="3" data-items-lg="3" data-items-md="2" data-items-sm="2" data-items-xs="1">
                	<div class="item">
							<div class="post">
								<img src="<?php echo base_url();?>assets/images/program/sparc.jpg" alt="">
								<div class="post-info">
									<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Workshop</a></span>
									
									<a class="post-title" href="#!">SPARC online Workshop</a>
									<p class="mb-0">Indo US SPARC online Workshop 
on
APPLICATIONS OF ELECROTHERAPY 
IN HEALTHCARE </p><br />
<a href="https://sparcaeh2021.in/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Website</a> 
<a href="https://learn.techmagnox.com/purdueRegister" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Enroll</a>
<a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
								</div>
							</div>
						</div>
                        <div class="item">
							<div class="post">
								<img src="<?php echo base_url();?>assets/images/program/indogfoe.jpg" alt="">
								<div class="post-info">
									<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Symposium</a></span>
									
									<a class="post-title" href="#!">INDOGFOE 2020</a>
									<p class="mb-0">11th Indo German Frontiers of Engineering Symposium organized by IIT Kharagpur & the Alexander von Humboldt Foundation </p><br />
                                    <a href="https://indogfoe2020.in/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Website</a> 
<a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
								</div>
							</div>
						</div>
					<!-- post --><div class="item">
							<div class="post">
								<img src="<?php echo base_url();?>assets/images/program/adms.jpg" alt="">
								<div class="post-info">
									<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Program</a></span>
									
									<a class="post-title" href="#!">MMST 2020 @ IIT Kharagpur</a>
									<p class="mb-0">Interested in Research in Medical Science and Technology Join MMST @ SMST, IIT Kharagpur </p>
                                    <br /><a href="http://gate.iitkgp.ac.in/mmst/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">GATE Website</a> <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
								</div>
							</div>
						</div>
						<!-- post --><div class="item">
							<div class="post">
								<img src="<?php echo base_url();?>assets/images/program/sathi.jpg" alt="">
								<div class="post-info">
									<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Training</a></span>
									
									<a class="post-title" href="#!">SATHI DST, Govt of India</a>
									<p class="mb-0">Avail the Facilities of SATHI, DST & IIT Kharagpur</p>
                                    <br />
                                    <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a> <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
								</div>
							</div>
						</div>
					
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog bg-light">
	<div class="container">
		<div class="row">
			<!-- info -->
			<div class="col-md-6 col-lg-5 align-self-center">
				<h6 class="bg-grad p-2 border-radius-3 d-inline-block text-white">Explore Offers Now !!</h6>
				<h2  style="line-height:38px" >Corporate Training <br>& Certification Program with Live Projects</h2>
				<p>We have the Best Instructors which consists of Experienced Faculty Members and Alumni from the best Institutes of the World. To view about our Instructors Click here. </p>
			</div>
			<!-- image -->
			<div class="col-md-6 col-lg-7 bg-light" >
	
				<div class="row mt-4 text-center">
				
					<!-- service item -->
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/1.jpg" height="75px" /></div>
						</div>
					</div>
					<!-- service item -->
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/2.png"  height="75px" /></div>
						</div>
					</div>
					<!-- service item -->
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/3.png"  height="75px" /></div>
						</div>
					</div>
					<!-- service item -->
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/4.png"  height="75px" /></div>
						</div>
					</div>
					<!-- service item -->
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/5.jpg"  height="75px" /></div>
						</div>
					</div>
					<!-- service item -->
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/7.png"  height="45px" /></div>
						</div>
					</div>
					<!-- service item -->
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/8.png"  height="75px" /></div>
						</div>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/9.png"  height="75px" /></div>
						</div>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/10.png"  height="75px" /></div>
						</div>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/11.png"  height="75px" /></div>
						</div>
					</div>
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/12.png"  height="75px" /></div>
						</div>
					</div>
					<!-- service item -->
					<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
						<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
							<div><img src="<?php echo base_url();?>assets/images/skills/14.png"  height="75px" /></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-sm-12 mt-2 mt-md-4">
						<div class="text-center">
							<a class="btn btn-grad mb-0 mr-3" href="<?php echo site_url('home/skills'); ?>">See All Skills</a>
							
						</div>
					</div>
				</div>
	
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-6">
				<h4 class="team-name text-center">Students Feedback</h4>
				<div class="col-md-12 testimonials testimonials-border py-6">
					<div class="owl-carousel testi-full owl-grab" data-arrow="false" data-dots="false" data-items-xl="1" data-items-md="1" data-items-xs="1">
						<!-- testimonial item -->
						<div class="item">
							<div class="testimonials-wrap">
								<div class="testi-text">
									<p>An Absolutely Wonderful Course! I Learned A Lot About Iot And Importance Of Cloud Computing With The Technologies . I Think Everyone Must-take Course For Those Who Are In Interest In Future. </p>
									<div class="testi-avatar"> <img src="<?php echo base_url();?>assets/images/thumbnails/avatar-01.jpg" alt="avatar"> </div>
									<h6 class="mb-0 mt-3">Supratik Dev</h6>
									<h6 class="small">Student, ICFAI University (Tripura)</h6>
								</div>
							</div>
						</div>
						<!-- testimonial item -->
						<div class="item">
							<div class="testimonials-wrap">
								<div class="testi-text">
									<p>The Course Content Is Excellent. The Topics Are Very Informative And The Faculty Abhinab Sir Is Just Outstanding He Explained Everything .
 I Would Like To Say Special Thanks To Mr. Jong-moon Chung For Explaining All Topics In Simplify Way. Thank You.</p>
									<div class="testi-avatar"> <img src="<?php echo base_url();?>assets/images/thumbnails/avatar-02.jpg" alt="avatar"> </div>
									<h6 class="mb-0 mt-3">Kherenbar Debbarma</h6>
									<h6 class="small">Student, ICFAI University (Tripura)</h6>
								</div>
							</div>
						</div>
						<!-- testimonial item -->
						<div class="item">
							<div class="testimonials-wrap">
								<div class="testi-text">
									<p>"The Professor Does A Great Job In Explaining The Concepts And The Content Of The Lecture. The Lecture Is Very Clear And Concise.
The Professor Does Not Simply Read Off The Slides But Provides Much More In Depth Explanation Of Each Point Covered In The Slides So You Will Need To Watch The Video Or Read The Transcript While Reading Off The Slides. This Can Be Both A Good Or Bad Depending On Your Viewpoint Because The Slides Are Not Sufficient Enough To Study From But It Does A Great Job In Reminding You And Summarizing What Was Covered In Lecture."</p>
									<div class="testi-avatar"> <img src="<?php echo base_url();?>assets/images/thumbnails/avatar-03.jpg" alt="avatar"> </div>
									<h6 class="mb-0 mt-3">Hritayan Debnath</h6>
									<h6 class="small">Student, ICFAI University (Tripura)</h6>
								</div>
							</div>
						</div>
						<!-- testimonial item -->
						<div class="item">
							<div class="testimonials-wrap">
								<div class="testi-text">
									<p>This Is My First Course From Outside Vendor. All The Material Is Updated (i Always Googled What Was Delivered And Matched With The Latest Trends And Standards).
Last But Not Least, Professor Abhinaba Is A Great Teacher, His Tone And Explanations Are Out Of This World. I Admire His Passion And Knowledge Of Everything That He Delivered."</p>
									<div class="testi-avatar"> <img src="<?php echo base_url();?>assets/images/thumbnails/avatar-04.jpg" alt="avatar"> </div>
									<h6 class="mb-0 mt-3">Meril Das</h6>
									<h6 class="small">Student, ICFAI University (Tripura)</h6>
								</div>
							</div>
						</div>
                        <div class="item">
							<div class="testimonials-wrap">
								<div class="testi-text">
									<p>I Did The Certificate Course. I Have Taken Embedded Systems During College But Didn't Understand Anything. The Teacher Here Made It So Easy To Understand And Also The Assignments Encouraged Us To Do Self Discovery.
This Was The First Course I Completed On Coursera. I Am Looking Forward To More Such Courses And Completing Them With My New Found Confidence And Discipline.</p>
									<div class="testi-avatar"> <img src="<?php echo base_url();?>assets/images/thumbnails/avatar-04.jpg" alt="avatar"> </div>
									<h6 class="mb-0 mt-3">Pastur Das</h6>
									<h6 class="small">Student, ICFAI University (Tripura)</h6>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-6 col-md-6">
				<h4 class="team-name text-center">Advisors and Instructors<br /><br /></h4>
				<div class="row">
					<div class="col-12">
						<div class="owl-carousel owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="2" data-items-xs="1">
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/nrm.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Prof N R Mandal</h4>
										<span class="team-position">Chief Advisor</span>
										<p>Ex Professor & Dean of Students Affairs of IIT Kharagpur. He is the Fellow of Royal Institution of Naval Architects, Institution of Engineers. His specialized fields are Energy, Engineering Design, Alumninum Wealding and more.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
											<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/raj.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Dr. Rajarshee Ray</h4>
										<span class="team-position">Senior Technical Advisor</span>
										<p>Founder and CTO of SoS Corp. joins our Board. He has done his PhD from The University of Texas & Health Mgmt Course from Harvard University. He was the Chief R&D Architect in Sun Microsystems, Senior Engineer at Intel Corp., USA etc. He will be the key player to introduce AI & Deep Learning.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
											<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/guha.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Dr. Debasish Guha</h4>
										<span class="team-position">Director of TCS</span>
										<p>Debasish Guha has completed his ME and PhD from IIT Kharagpur. He has more than 25 years experience in Telecommunication.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
											<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/arindam.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Dr. Arindam Dasgupta</h4>
										<span class="team-position">Software Designer</span>
										<p>I am passionate about learning new things, especially programming skills. I have done my MS and PhD from IIT Kharagpur. I have 18 years experience in this computer Science field.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
											<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="client bg-light pt-6">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12 mx-auto">
				<div class="title text-left">
					
					<h3>Awards and Achievements </h3>
					
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel owl-grab arrow-hover arrow-gray" data-margin="40" data-arrow="true" data-dots="false" data-items-xl="5" data-items-lg="5" data-items-md="5" data-items-sm="3" data-items-xs="2">
					<div class="item"><img src="<?php echo base_url();?>assets/images/awards/iew.png" alt=""><br><p>Indian Excellance Awards 2020</p></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/awards/apacI.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/collaboration/7.jpg" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/collaboration/8.jpg" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/collaboration/9.jpg" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/collaboration/10.jpg" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/collaboration/1.jpg" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/collaboration/2.jpg" alt=""></div>
				</div>
			</div>
            <a href="" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">View All Awards</a>
		</div>
	</div>
</section>