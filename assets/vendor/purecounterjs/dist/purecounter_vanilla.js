/*!
 * purecounter.js - A simple yet configurable native javascript counter which you can count on.
 * Author: Stig Rex
 * Version: 1.3.0
 * Url: https://github.com/srexi/purecounterjs
 * License: MIT
 */
!function(e,o){"object"==typeof exports&&"object"==typeof module?module.exports=o():"function"==typeof define&&define.amd?define([],o):"object"==typeof exports?exports.PureCounter=o():e.PureCounter=o()}(self,(function(){return{}}));
//# sourceMappingURL=purecounter_vanilla.js.map