var config = {
	form: 'form',
	validate: {
		otpcode: {
			'validation': "required length",
			'length': "min4",
			'error-msg': "Enter valid OTP"
		},
		new_pass: {
			'validation': "required length custom", 
			'length': "min8",
			'regexp': "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})",
			'error-msg': "Enter Valid Password"
		},
		cnf_pass: {
			'validation': "required confirmation",
			'confirm': "new_pass",
			'error-msg': "Password not matched"
		}
	}
}
$.validate({
	form : '#FrmAdmin',
	modules : 'jsconf, security',
	onModulesLoaded : function() {
		$.setupValidation(config);
	},
	onError : function($form) {
	  //alert('Validation of form '+$form.attr('id')+' failed!');
	},
	onSuccess : function($form) {
	  //alert('The form '+$form.attr('id')+' is valid!');
	  return true; // Will stop the submission of the form
	}
});

function SendMail()
{
	swal({
	  text: 'Enter Registered Email',
	  content: "input",
	  button: {
		text: "Send!",
		closeModal: false,
	  }
	})
	.then(name => {
	  if (!name) throw null;
	  
	  $.ajax({
		  url: baseURL+'IIEST/SendMailOTPConfirmation/?qemail='+name,
		  method: 'POST',
		  async: false,
		  beforeSend: function() {
			$('.swal-button .swal-button--confirm').addClass('swal-button--loading');
		  },
		  success: (data)=>{
			var json = JSON.parse(data);
			const status = json['status'];
			const message = json['msg'];
			const title = json['title'];
			const email = json['email'];
			const code = json['code'];
			//console.log(message)
			if(status!='success'){
				swal({
					title: title,
					text: message,
					icon: status,
				});
			}else{
				swal({
					title: title,
					text: message,
					icon: status,
				}).then(result=>{
					window.open(baseURL+'IIEST/ResetPassword/?email='+btoa(email)+'&vercode='+btoa(code), '_self');
				})
			}
		  },
		  error: (errors)=>{
			  console.log(errors)
		  }
	  });
	}).catch(err => {
	  if (err) {
		  
		swal("Oh no!", "The request failed!", "error");
	  }else {
		swal.stopLoading();
		swal.close();
	  }
	});
}