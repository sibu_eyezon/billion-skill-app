$('.select2').select2();
		
function setFacultyData(input){
	$('#facid').val(input);
	setFiltersOnPapers();
}

setFiltersOnPapers();

function setFiltersOnPapers()
{
	$('#filterData').hide();
	$("#loader").show();
	var dinput = $('#ddl_dept').val();
	var dept = dinput.split('_');
	var deptId = '';
	var year = $('#ddl_year').val();
	var type = $('#ddl_type').val();
	var facid = $('#facid').val();
	var faculty = facid.split('-');
	var title = '';
	//alert("D: "+deptId+"; Y: "+year+"; T: "+type)
	var str = "";
	if(faculty[0]==='0'){
		title+='';
	}else{
		title+=faculty[1]+'_';
	}
	if(dept[0]==='0'){
		deptId = null;
		title+='';
	}else{
		deptId = dept[0];
		title+=dept[1]+'_';
	}
	if(year.length==0 || year[0]=="all"){
		year = null;
		title+='';
	}else{
		title+=year+'_';
	}
	if(type.length==0 || type[0]=='all'){
		type = null;
		str = 'Journal,Conference,Books';
	}else{
		str = type.toString();
	}
	if(title.length>0){
		title = title.substring(0, title.length-1);
	}
	var typeName = str.split(',');
	var tcount = typeName.length;
	var j=1;
	$.ajax({
		url: baseURL+'IIEST/GetPublicationByFilter',
		type: 'GET',
		data: {did: deptId, yr: year, tp: type, faculty: facid},
		success: function(response){
			//console.log(data)
			var papers = JSON.parse(response);
			var publications = papers['papers'];
			console.log(publications);
			var tableData = '<h4>'+papers['msg']+'</h4><div class="table-responsive-sm">';
			
			for(var i=0; i<tcount; i++){
				tableData+='<table class="table table-lg table-noborder table-striped example12" id="'+typeName[i]+'" width="100%;">';
				tableData+='<thead><tr><th scope="col" class="all-text-white bg-grad" colspan="2" style="color:#fff;" align="center">'+typeName[i]+'</th></tr><tr><td>Sl#</td><td>Details</td></tr></thead><tbody>';
				j=1;
				$.each(papers['papers'], (k, val)=>{
					if(val['type']==typeName[i]){
						tableData+='<tr><td>'+j+'</td>';
						tableData+='<td>'+val['authors']+', <em>'+val['title']+'</em>';
						if(val['volume']!=""){
							tableData+=', '+val['volume'];
						}
						if(val['pages']!=""){
							tableData+=', '+val['pages'];
						}
						tableData+=', '+val['name'];
						if(val['year']!=""){
							tableData+=', '+val['year'];
						}
						tableData+='</td></tr>';
						j++;
					}
				});
				tableData+='</tbody></table>';
			}
			tableData+='</div>';
			$("#loader").hide();
			
			$('#filterData').html(tableData);
			$('#filterData').show();
		},
		complete:function(data){
			$("#loader").hide();
			$('#filterData table').each(function(){ 
			   $('#'+this.id).DataTable( {
					"ordering": true,
					"info": true,
					"colReorder": true,
					dom: 'Bfrtip',
					buttons:
					[
						{ extend: 'excel', className: 'bg-white border-0', text: '<i class="fa fa-file-excel-o text-success" style="font-size:xx-large;"></i>', title: this.id+'_'+title},
						{ extend: 'pdf', className: 'bg-white border-0', text: '<i class="fa fa-file-pdf-o text-danger" style="font-size:xx-large;"></i>', title: this.id+'_'+title },
						{ extend: 'print', className: 'bg-white border-0', text: '<i class="fa fa-print text-primary" style="font-size:xx-large;"></i>', title: this.id+'_'+title }
					]
				} );
			});
		},
		error: function(errors){
			console.log(errors);
		}
	});
}