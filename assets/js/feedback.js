$('#other').on('change', function(){
	if($('#other').is(':checked')){
		$('#others').css('display', 'block');
	}else{
		$('#others').css('display', 'none');
	}
});
$(document).ready(function($)
{
	// Validation and Ajax action
	$("form#frmfeedback").validate({
		errorPlacement: function(error, element) {
		  $(element).closest('.form-group').append(error);
		},
		rules: {
			ddl_title: {
				required: true
			},
			pageurl: {
				url: true
			},
			feedback: {
				required: true
			},
			firstname: {
				required: true,
				minlength: 3
			},
			lastname: {
				required: true,
				minlength: 1
			},
			email: {
				required: true,
				email: true
			},
			mobile: {
				digits: true,
				minlength: 10,
				maxlength: 10
			},
			enroll: {
				digits: true,
				minlength: 9,
				maxlength: 9
			},
			declaration: {
				required: true
			}
		},

		messages: {
			ddl_title: {
				required: 'Must select one option.'
			},
			pageurl: {
				url: 'Please enter valid URL link.'
			},
			feedback: {
				required: 'Must enter some feedback.'
			},
			firstname: {
				required: 'Must enter your firstname.',
				minlength: 'Must have atleast 3 characters'
			},
			lastname: {
				required: 'Must enter your lastname.[If none, type "X"].',
				minlength: 'Must have atleast 1 character'
			},
			email: {
				required: 'Must select your email.',
				email: 'Please enter valid email address.'
			},
			mobile: {
				digits: 'Please enter only numbers.',
				minlength: 'Mobile number must be 10 digits only.',
				maxlength: 'Mobile number must be 10 digits only.'
			},
			enroll: {
				digits: 'Please enter only numbers.',
				minlength: 'Enrollment number must be 9 digits only.',
				maxlength: 'Enrollment number must be 9 digits only.'
			},
			declaration: {
				required: 'Must accept declarations.'
			}
		},

		// Form Processing via AJAX
		submitHandler: function(form, e)
		{
			e.preventDefault();
			var frndata = new FormData($('#frmfeedback')[0]);
			$.ajax({
				url: baseURL+'IIEST/SubmitFeedback',
				method: 'POST',
				dataType: 'json',
				processData: false,
				contentType: false,
				data: frndata,
				success: function(resp)
				{
					//console.log(resp)
					if(resp.accessGranted)
					{
						swal({
						  title: "Thank You!",
						  text: "Your Feedback has been received.",
						  icon: "success",
						  buttons: "Ok",
						  timer: 2000,
						})
					}else{
						swal({
						  title: "Sorry!",
						  text: "Your Feedback hasn't been received.",
						  icon: "danger",
						  buttons: "Ok",
						  timer: 2000,
						})
					}
					$('#frmfeedback')[0].reset();
					$('#others').css('display', 'none');
				}
			});
		}
	});
	
	// Set Form focus
	$("form#frmfeedback .form-group:has(.form-control):first .form-control").focus();
});