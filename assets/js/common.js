$(document).ready(function(){
	$('#cautionAlert').modal('show');
});

$('#nt-news').newsTicker({
	row_height: 45,
	max_rows: 6,
	duration: 4000,
	prevButton: $('#nt-news-prev'),
	nextButton: $('#nt-news-next')
});
$('#nt-achieve').newsTicker({
	row_height: 45,
	max_rows: 6,
	duration: 4000,
	prevButton: $('#nt-achieve-prev'),
	nextButton: $('#nt-achieve-next')
});
$('#nt-happens').newsTicker({
	row_height: 45,
	max_rows: 6,
	duration: 4000,
	prevButton: $('#nt-happens-prev'),
	nextButton: $('#nt-happens-next')
});
$('#nt-events').newsTicker({
	row_height: 45,
	max_rows: 6,
	duration: 4000,
	prevButton: $('#nt-events-prev'),
	nextButton: $('#nt-events-next')
});
