/* ===================
    Table Of Content
======================
01 PRELOADER
02 COUNTER
03 PORTFOLIO ISOTOPE
04 WOW
05 OWL CAROUSEL
06 SWIPER SLIDER
07 BACK TO TOP
08 STICKY BAR
09 STICKY HEADER
10 FIT VIDEO
11 PARALLAX
12 WAVE
13 CONTACT FORM
14 IE DETECTION
15 MEGA MENU

======================*/
// *** TO BE CUSTOMISED ***

var manual_or_random="manual" //"manual" or "random"
var randomsetting="3 days" //"eachtime", "sessiononly", or "x days (replace x with desired integer)". Only applicable if mode is random.

//////No need to edit beyond here//////////////

function getCookie(Name) { 
var re=new RegExp(Name+"=[^;]+", "i"); //construct RE to search for target name/value pair
if (document.cookie.match(re)) //if cookie found
return document.cookie.match(re)[0].split("=")[1] //return its value
return null
}

function setCookie(name, value, days) {
var expireDate = new Date()
//set "expstring" to either future or past date, to set or delete cookie, respectively
var expstring=(typeof days!="undefined")? expireDate.setDate(expireDate.getDate()+parseInt(days)) : expireDate.setDate(expireDate.getDate()-5)
document.cookie = name+"="+value+"; expires="+expireDate.toGMTString()+"; path=/";
}

function deleteCookie(name){
setCookie(name, "moot")
}


function setStylesheet(title, randomize){ //Main stylesheet switcher function. Second parameter if defined causes a random alternate stylesheet (including none) to be enabled
var i, cacheobj, altsheets=[""]
for(i=0; (cacheobj=document.getElementsByTagName("link")[i]); i++) {
if(cacheobj.getAttribute("rel").toLowerCase()=="alternate stylesheet" && cacheobj.getAttribute("title")) { //if this is an alternate stylesheet with title

cacheobj.disabled = true
altsheets.push(cacheobj) //store reference to alt stylesheets inside array
if(cacheobj.getAttribute("title") == title) //enable alternate stylesheet with title that matches parameter
cacheobj.disabled = false //enable chosen style sheet
}
}
if (typeof randomize!="undefined"){ //if second paramter is defined, randomly enable an alt style sheet (includes non)
var randomnumber=Math.floor(Math.random()*altsheets.length)
altsheets[randomnumber].disabled=false
}
//jQuery.each(jQuery.browser, function(i, val) {
//									// alert(i+"==="+val);
//      if(i == "safari" && val == true){
//		 // alert(i);
//		  if(title == "change"){
//		  	$("link[title=change]").attr("rel","stylesheet");
//			$("link[title=change]").attr("href","../css/override.css");
//		  }else{
//			  $("link[title=change]").attr("rel","alternate stylesheet");
//			  $("link[title=change]").attr("href","../common_css/common_stylesheet.css");
//		  }
//	  }
//    });
return (typeof randomize!="undefined" && altsheets[randomnumber]!="")? altsheets[randomnumber].getAttribute("title") : "" //if in "random" mode, return "title" of randomly enabled alt stylesheet
}

function chooseStyle(styletitle, days){ //Interface function to switch style sheets plus save "title" attr of selected stylesheet to cookie
	if (document.getElementById){
		if(styletitle=="blind"){
			if(getCookie("mysheet")!="change"){
				setCookie("theme", getCookie("mysheet"), days)
			}
			setCookie("mysheet", styletitle, days)
			setStylesheet(styletitle)
		}else{ 	
			if(styletitle == "color") {
				if(getCookie("theme") !="none") {
					setCookie("mysheet", getCookie("theme"), days)
					setStylesheet(getCookie("theme"))
				} else {
					setCookie("mysheet", styletitle, -1)
					setCookie("theme", getCookie("mysheet"), -1)
				}
			} else {
				setCookie("mysheet", styletitle, days)
				setCookie("theme", getCookie("mysheet"), days)
				setStylesheet(styletitle)
			}	
		}
	}
}
function getActiveStyleSheet() {
  var styleTag;
  var styleSwitch;
  var styleTitle='';
  for(var i=0; (styleTag = document.getElementsByTagName("link")[i]); i++) {
	if(styleTag.getAttribute("title")=="switch") {styleSwitch=styleTag;}
  }
  for(var i=0; (styleTag = document.getElementsByTagName("link")[i]); i++) {
	if(styleTag.getAttribute("rel").indexOf("alternate stylesheet")!=-1 && styleTag.getAttribute("title")) {
		if(styleTag.getAttribute("href")==styleSwitch.getAttribute("href ")) {styleTitle=styleTag.getAttribute("title");}
	}
	}
  return styleTitle;
}
function indicateSelected(element){ //Optional function that shows which style sheet is currently selected within group of radio buttons or select menu
	if (selectedtitle!=null && (element.type==undefined || element.type=="select-one")){ //if element is a radio button or select menu
		var element=(element.type=="select-one") ? element.options : element
		for (var i=0; i<element.length; i++){
		if (element[i].value==selectedtitle){ //if match found between form element value and cookie value
			if (element[i].tagName=="OPTION") //if this is a select menu
				element[i].selected=true
				else //else if it's a radio button
				element[i].checked=true
				break
			}
		}
	}
}

if (manual_or_random=="manual"){ //IF MANUAL MODE
	var selectedtitle=getCookie("mysheet")
	if (document.getElementById && selectedtitle!=null) //load user chosen style sheet from cookie if there is one stored
	setStylesheet(selectedtitle)
}
else if (manual_or_random=="random"){ //IF AUTO RANDOM MODE
	if (randomsetting=="eachtime")
		setStylesheet("", "random")
	else if (randomsetting=="sessiononly"){ //if "sessiononly" setting
		if (getCookie("mysheet_s")==null) //if "mysheet_s" session cookie is empty
		document.cookie="mysheet_s="+setStylesheet("", "random")+"; path=/" //activate random alt stylesheet while remembering its "title" value
		else
		setStylesheet(getCookie("mysheet_s")) //just activate random alt stylesheet stored in cookie
	}
	else if (randomsetting.search(/^[1-9]+ days/i)!=-1){ //if "x days" setting
		if (getCookie("mysheet_r")==null || parseInt(getCookie("mysheet_r_days"))!=parseInt(randomsetting)){ //if "mysheet_r" cookie is empty or admin has changed number of days to persist in "x days" variable
			setCookie("mysheet_r", setStylesheet("", "random"), parseInt(randomsetting)) //activate random alt stylesheet while remembering its "title" value
			setCookie("mysheet_r_days", randomsetting, parseInt(randomsetting)) //Also remember the number of days to persist per the "x days" variable
		}
	else
		setStylesheet(getCookie("mysheet_r")) //just activate random alt stylesheet stored in cookie
	} 
}

$('#selector_btn').click(function() {
	if($('#selector_menu').hasClass('mntoggle')){
		$('#selector_btn').removeClass('btntoggle', 1000);
		$('#selector_btn i').addClass('fa-caret-left', 1000);
		$('#selector_btn i').removeClass('fa-caret-right', 1000);
		$('#selector_menu').removeClass('mntoggle', 1000);
	}else{
		$('#selector_btn').addClass('btntoggle', 1000);
		$('#selector_btn i').removeClass('fa-caret-left', 1000);
		$('#selector_btn i').addClass('fa-caret-right', 1000);
		$('#selector_menu').addClass('mntoggle', 1000);
	}
});

(function ($) {
"use strict";


    // BEGIN: 01 Preloader
    var preLoader = function() {
        if($('.preloader').length){
            var $preloader = $('.preloader');
            $preloader.delay(200).fadeOut(600);
        }
    };
    // END: Preloader
	
	var mobileMenu = function() {
		if(screen.width<=992){
			alert(screen.width)
			$('.navbar-top').toggleClass('d-none');
		}
	};

    // BEGIN: 02 Counter
    var CountTo = function () {
        var initInstances = function () {
          var $count = $('.counter-item-digit');
          if($count.length) {
            $count.appear(function (direction) {
                $(this).countTo();
            }, {
                offset: '100%',
                triggerOnce: true
            });
          }
        };

        return {
            init: function () {
                initInstances();
            }
        };
    }();
    // END: Counter

    // BEGIN: 03 Portfolio Isotope
    var portfolioIsotope = function() {
        if ( $().isotope ) {
            var $container = $('.portfolio-wrap');
            $container.imagesLoaded(function(){
                $container.isotope({
                    itemSelector: '.isotope-item',
                    transitionDuration: '0.5s',
                });
            });
            $('.nav-tabs .nav-filter').on('click',function() {
                var selector = $(this).attr('data-filter');
                $('.nav-tabs .nav-filter').removeClass('active');
                $(this).addClass('active');
                $container.isotope({ filter: selector });
                return false;
            });
        }
    };
    // END: Portfolio Isotope

    // BEGIN: 04 Wow
    var wow = function() {
        if($('.wow').length){
            wow = new WOW( {
                mobile: true  // Turn on/off WOW.js on mobile devices.
            }).init();
        }
    };
    // END: Wow Js

    // BEGIN: 05 Owl Carousel
    var owlCarousel = function() {
        var $carousel = $('.owl-carousel');
        if( $carousel.length < 1 ){
            return true;
        }
        $carousel.each( function(){
            var slider = $(this);
            var sliderArrow = slider.attr('data-arrow') == 'false' ? false : true; //option: true or false
            var sliderDots = slider.attr('data-dots') == 'false' ? false :true; //option: true or false
            var sliderAutoPlay = slider.attr('data-autoplay') ? false : true; //option: number in ms
            var sliderAutoPlayTime = slider.attr('data-autoplay') ? Number(sliderAutoPlay) : 4000;
            var sliderSpeed = slider.attr('data-speed') ? slider.attr('data-speed') : 800; //option: number in ms (Smart speed)
            var sliderMargin = slider.attr('data-margin') ? slider.attr('data-margin') : 30; //option: number in px
            var sliderLoop = slider.attr('data-loop') == 'false' ? false : true; //option: true or false
            var sliderStart = slider.attr('data-start') ? slider.attr('data-start') : 0; //option: number
            var sliderSlideBy = slider.attr('data-slideby') ? sliderSlideBy == 'page' ? 'page' : Number(slider.attr('data-slideby')) : Number(1); //option: number
            var sliderHoverPause = slider.attr('data-pause') == 'false' ? false : true; //option: true or false
            var sliderMerge = slider.attr('data-merge') == 'true' ? true : false; //option: number (use in slider items DIV)
            var sliderDrag = slider.attr('data-drag') == 'false' ? false : true; //option: true or false
            var sliderRewind = slider.attr('data-rewind') == 'true' ? true : false; //option: true or false
            var sliderCenter = slider.attr('data-center') == 'true' ? true : false; //option: true or false
            var sliderVideo = slider.attr('data-video') == 'true' ? true : false; //option: true or false
            var sliderLazy = slider.attr('data-lazyload') == 'true' ? true : false; //option: true or false
            var sliderRTL = slider.attr('data-rtl'); //option: true (false by default)
            var sliderItems = slider.attr('data-items') ? slider.attr('data-items') : 4; //option: number (items in all device)
            var sliderItemsXl = slider.attr('data-items-xl')? slider.attr('data-items-xl') : Number(sliderItems) ; //option: number (items in 1200 to end )
            var sliderItemsLg = slider.attr('data-items-lg')? slider.attr('data-items-lg') : Number(sliderItemsXl) ; //option: number (items in 992 to 1199 )
            var sliderItemsMd = slider.attr('data-items-md')? slider.attr('data-items-md') : Number(sliderItemsLg) ; //option: number (items in 768 to 991 )
            var sliderItemsSm = slider.attr('data-items-sm')? slider.attr('data-items-sm') : Number(sliderItemsMd) ; //option: number (items in 576 to 767 )
            var sliderItemsXs = slider.attr('data-items-xs') ? slider.attr('data-items-xs') : Number(sliderItemsSm) ; //option: number (items in start to 575 )
            slider.owlCarousel({
                margin: Number(sliderMargin),
                loop: sliderLoop,
                merge: sliderMerge,
                mouseDrag: sliderDrag,
                startPosition: Number(sliderStart),
                rewind: sliderRewind,
                slideBy: sliderSlideBy,
                center: sliderCenter,
                lazyLoad: sliderLazy,
                nav: sliderArrow,
                navText: [
                            '<i class="ti-angle-left"></i>',
                            '<i class="ti-angle-right"></i>'
                ],
                autoplay: sliderAutoPlay,
                autoplayTimeout: sliderAutoPlayTime,
                autoplayHoverPause: sliderHoverPause,
                dots: sliderDots,
                smartSpeed: Number(sliderSpeed),
                video: sliderVideo,
                rtl: sliderRTL,
                responsive:{
                   0:{ items:Number(sliderItemsXs) },
                  576:{ items:Number(sliderItemsSm) },
                  768:{ items:Number(sliderItemsMd) },
                  992:{ items:Number(sliderItemsLg) },
                  1200:{ items:Number(sliderItemsXl) }
                },
            });
        });
    };
    // END: Owl Carousel

    // BEGIN: 06 Swiper Slider
    var swiperSlider = function() {
        if($(".swiper-slider-fade").length !== 0) {
            var swiper = new Swiper('.swiper-container', {
                effect: 'fade', //other supported effects: coverflow, flip, cube, slide
                pagination: null,
                navigation: {
                    nextEl: '.swiper-button-next',
                    prevEl: '.swiper-button-prev',
                },
                nextButton: '.swiper-button-next',
                prevButton: '.swiper-button-prev',
                autoplay: 5000,
                speed: 1000,
                spaceBetween: 0,
                loop: true,
                simulateTouch: true,
                onSlideChangeEnd: function(swiper) {
                    $('.swiper-slide').each(function() {
                        if ($(this).index() === swiper.activeIndex) {
                            // Fadein in active slide
                            $(this).find('.slider-content').fadeIn(25);
                        } else {
                            // Fadeout in inactive slides
                            $(this).find('.slider-content').fadeOut(25);
                        }
                    });
                }
            });
        }
    };
    // END: Swiper Slider

    // BEGIN: 07 Back To top
     var backTotop = function(){
        var $backtotop = $('.back-top');
        $(window).on('scroll', function() {
            if ($(this).scrollTop() > 500) {
                $backtotop.addClass('btn-show');
            } else {
                $backtotop.removeClass('btn-show');
            }
        });
        $backtotop.on('click', function() {
            $('html, body').animate({ scrollTop: 0}, 900, 'easeInOutCirc');
            return false;
        });
    };
    // END: Back To top

    // BEGIN: 08 Sticky bar
    var stickyBar = function() {

        if($(".sticky-element").length) {
          var $stickyElement = $(".sticky-element");
          if ($(window).width() <= 1024) {
              $stickyElement.trigger('sticky_kit:detach');
          }
          else {
              $stickyElement.stick_in_parent({
                  offset_top: 100
              });
          }
        }
    };
    // END: Sticky bar

    // BEGIN: Sticky Header
    var stickyHeader = function() {
        var sticky = $('.navbar-sticky'),
        stickyStatic = $('.header-static');
            $(window).scroll(function () {
                var scTop = $(document).scrollTop();
                if (scTop > 400) {
                    if (!$(".sticky-space").length) {
                        stickyStatic.after('<div class="sticky-space"></div>');
                        $(".sticky-space").css({'height': sticky.height() + 'px'});
                    }
                    sticky.addClass('navbar-sticky-on');
                } 
                else {
                    $(".sticky-space").remove();
                    sticky.removeClass('navbar-sticky-on');
            }
        });
    };
    // END: Sticky Header

    // BEGIN: 10 Fit Video
    var fullWithvideo = function(){
        // Target your .container, .wrapper, .post, etc.
        if($(".fit-video").length){
            var $fitvideo = $(".fit-video");
            $fitvideo.fitVids();
        }
      };
    // END: Fit Video

    // BEGIN: 11 Parallax
    var jarallax = function(){
	    if($('.parallax-bg').length){
	        $('.parallax-bg').jarallax({
	            speed: 0.5
	        });
	    }
    };
    // END: Parallax

    // BEGIN: 12 wave
    var myWave  = function(){
        if($('#wave-one').length){
          var wave_one = $('#wave-one');
          wave_one.wavify({
                  height: 60,
                  bones: 5,
                  amplitude: 20,
                  color: '#fff',
                  speed: .15
          });
        }
        if($('#wave-two').length){
          var wave_two = $('#wave-two');
          wave_two.wavify({
                height: 40,
                bones: 4,
                amplitude: 20,
                color: 'rgba(255, 255, 255, .8)',
                speed: .25
            });
        }
    };
    // End: wave

    // BEGIN: 13 Contact Form
    var form = $('.contact-form');
    var message = $('.contact-msg');
    var form_data;
    // Success function
    function done_func(response) {
        message.fadeIn().removeClass('alert-danger').addClass('alert-success');
        message.text(response);
        setTimeout(function () {
            message.fadeOut();
        }, 50000);
        form.find('input:not([type="submit"]), textarea').val('');
    }
    // fail function
    function fail_func(data) {
            message.fadeIn().removeClass('alert-success').addClass('alert-danger');
            message.text(data.responseText);
            setTimeout(function () {
                message.fadeOut();
            }, 5000);
        }
    form.submit(function (e) {
            e.preventDefault();
            form_data = $(this).serialize();
            $.ajax({
                type: 'POST',
                url: form.attr('action'),
                data: form_data
            })
            .done(done_func)
            .fail(fail_func);
        });
    // END: Contact Form

    // BEGIN: 14 IE Detection
    /**
     * detect IE
     * returns version of IE or false, if browser is not Internet Explorer
     */
    (function detectIE() {
        var ua = window.navigator.userAgent;

        var msie = ua.indexOf('MSIE ');
        if (msie > 0) {
            // IE 10 or older => return version number
            var ieV = parseInt(ua.substring(msie + 5, ua.indexOf('.', msie)), 10);
            document.querySelector('body').className += ' ie-browser';
        }

        var trident = ua.indexOf('Trident/index.html');
        if (trident > 0) {
            // IE 11 => return version number
            var rv = ua.indexOf('rv:');
            var ieV = parseInt(ua.substring(rv + 3, ua.indexOf('.', rv)), 10);
            document.querySelector('body').className += ' ie-browser';
        }

        var edge = ua.indexOf('Edge/index.html');
        if (edge > 0) {
           // IE 12 (aka Edge) => return version number
           var ieV = parseInt(ua.substring(edge + 5, ua.indexOf('.', edge)), 10);
            document.querySelector('body').className += ' ie-browser';
        }

        // other browser
        return false;
    })();
    // END: IE Detection

    // BEGIN: 15 Mega Menu
    $('.dropdown-menu a.dropdown-toggle').on('click', function(e) {
        if (!$(this).next().hasClass('show')) {
          $(this).parents('.dropdown-menu').first().find('.show').removeClass("show");
        }
        var $subMenu = $(this).next(".dropdown-menu");
        $subMenu.toggleClass('show');

        $(this).parents('li.nav-item.dropdown.show').on('hidden.bs.dropdown', function(e) {
          $('.dropdown-submenu .show').removeClass("show");
        });

        return false;
    });

    // END: Mega Menu

    // Initialze all functions

    $(window).on('scroll', function (){
      stickyBar();
    }).on('load', function() {
      preLoader();
    });
	
	var lastUpdate = function() {
		var dt = new Date(document.lastModified);
		const dateTimeFormat = new Intl.DateTimeFormat('en', { year: 'numeric', month: 'long', day: '2-digit' }) 
		const [{ value: month },,{ value: day },,{ value: year }] = dateTimeFormat .formatToParts(dt) 
		$('#lu').append(`<i class="fa fa-clock-o"></i> Last Updated: ${day} ${month} ${year}`);
	};

    //Document ready functions
    $(document).ready(function () {
          stickyHeader();
          CountTo.init();
          portfolioIsotope();
          owlCarousel();
          swiperSlider();
          wow();
          backTotop();
          fullWithvideo();
          jarallax();
          myWave();
		  lastUpdate();
		  $('[data-toggle="tooltip"]').tooltip();
    });
	

})(jQuery);

