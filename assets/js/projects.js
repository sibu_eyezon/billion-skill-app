$('.select2').select2({
  placeholder: 'Select an option'
});

setFiltersOnProjects();
var tbID;
function setFiltersOnProjects()
{
	$('#filterData').hide();
	$("#loader").show();
	var dinput = $('#ddl_dept').val();
	var dept = dinput.split('_');
	var deptId = '';
	var type = $('#ddl_type').val();
	var title = '';
	var str = "";
	var controls='';
	if(dept[0]==='0'){
		deptId = null;
		title+='';
	}else{
		deptId = dept[0];
		title+=dept[1]+'_';
	}
	if(type.length==0 || type[0]=='all'){
		type = null;
		str = 'Sponsored,Consultancy';
	}else{
		str = type.toString();
	}
	var typeName = str.split(',');
	var tcount = typeName.length;
	for(var i=0; i<tcount; i++){
		controls+='DataTables_Table_'+i+' ';
	}
	controls = controls.trim();
	var j=1;
	//alert(deptId+" "+year+" "+type)
	$.ajax({
		url: baseURL+'IIEST/GetProjectsByFilter',
		type: 'GET',
		data: {did: deptId, tp: type},
		success: function(data){
			//console.log(data)
			var papers = JSON.parse(data);
			var tableData = '<div class="table-responsive-sm">';
			for(var i=0; i<tcount; i++){
				tableData+='<table class="table table-lg table-noborder table-striped example12 mb-5" id="'+typeName[i]+'" width="100%;">';
				tableData+='<thead><tr><th scope="col" class="all-text-white bg-grad" colspan="4" style="color:#fff;" align="center">'+typeName[i]+'</th></tr>';
				tableData+='<tr><th scope="col" width="5%">Sl</th><th scope="col" width="45%">Title</th><th scope="col" width="30%">Members</th><th scope="col" width="20%">Details</th></tr></thead><tbody>';
				j=1;
				$.each(papers, (k, val)=>{
					if(val['type']==typeName[i]){
						tableData+='<tr><td>'+j+'</td>';
						tableData+='<td>'+val['title']+'</td>';
						tableData+='<td><b>Clients: </b>'+val['client']+'<br><b>PI: </b>'+val['pi']+'<br><b>Co-PI: </b>'+val['consult']+'</td>';
						tableData+='<td><b>Duration: </b>'+val['duration']+'<br><b>Amount: </b>'+val['amount']+'<br><b>Status: </b>'+val['status']+'</td>';
						j++;
						tableData+='</tr>';
					}
				});
				tableData+='</tbody></table>';
				
			}
			$("#loader").hide();
			
			$('#filterData').html(tableData);
			$('#filterData').show();
			
		},
		complete:function(data){

			$("#loader").hide();
			$('#filterData table').each(function(){ 
			   $('#'+this.id).DataTable( {
					"ordering": true,
					"info": true,
					"colReorder": true,
					dom: 'Bfrtip',
					buttons:
					[
						{ extend: 'excel', className: 'bg-white border-0', text: '<i class="fa fa-file-excel-o text-success" style="font-size:xx-large;"></i>', title: this.id+'_'+title},
						{ extend: 'pdf', className: 'bg-white border-0', text: '<i class="fa fa-file-pdf-o text-danger" style="font-size:xx-large;"></i>', title: this.id+'_'+title },
						{ extend: 'print', className: 'bg-white border-0', text: '<i class="fa fa-print text-primary" style="font-size:xx-large;"></i>', title: this.id+'_'+title }
					]
				} );
			});
		},
		error: function(errors){
			console.log(errors);
		}
	});
}