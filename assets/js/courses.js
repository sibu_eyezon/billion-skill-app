$('#academic').on('change', ()=>{
	var dpt_id = $('#academic').val();
	var faculty = '<option value="">----Select a faculty----</option>';
	$.ajax({
		url: baseURL+'IIEST/getFaculties',
		type: 'POST',
		data: {id: dpt_id},
		success: function(data){
			var obj = JSON.parse(data);
			$(obj).each((i, val)=>{
				faculty+='<option value="'+val['sl']+'">'+val['fname']+" "+val['lname']+'</option>';
			});
			//console.log(faculty)
			$('#faculty').html(faculty);
		},
		error: function (errors){
			console.log(errors);
		}
	});
});

$('#faculty').on('change', ()=>{
	var fac_id = $('#faculty').val();
	$.ajax({
		url: baseURL+'IIEST/getFacultyCourses',
		type: 'POST',
		data: {id: fac_id},
		success: function(data){
			var obj = JSON.parse(data);
			$('#courses').html(obj);
		},
		error: function (errors){
			console.log(errors);
		}
	});
})

function getCourseDetails(cid)
{
	$.ajax({
		url: baseURL+'IIEST/CourseDetails',
		type: 'POST',
		data: {id: cid},
		success: function(data){
			$('#courseDetails').html(data);
		},
		error: function(errors){
			console.log(errors)
		}
	});
}