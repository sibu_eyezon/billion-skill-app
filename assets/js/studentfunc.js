var config = {
	form: 'form',
	validate: {
		otpcode: {
			'validation': "required length",
			'length': "min6",
			'error-msg': "Enter valid OTP"
		},
		new_pass: {
			'validation': "required length custom", 
			'length': "min9",
			'regexp': "^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{9,})",
			'error-msg': "Enter valid Password"
		},
		cnf_pass: {
			'validation': "required confirmation",
			'confirm': "new_pass",
			'error-msg': "Password not matched"
		}
	}
}
$.validate({
	form : '#FrmAdmin',
	modules : 'jsconf, security',
	onModulesLoaded : function() {
		$.setupValidation(config);
	},
	onError : function($form) {
	  //alert('Validation of form '+$form.attr('id')+' failed!');
	},
	onSuccess : function($form) {
	  //alert('The form '+$form.attr('id')+' is valid!');
	  return true; // Will stop the submission of the form
	}
});

var formTemplate = $('#form-template > form').clone()[0];
$('#form-template').remove();

// prepare SweetAlert configuration
var swalConfig = {
  title: 'Enter valid Enrollment and Phone number',
  content: formTemplate,
  button: {
	text: 'Send OTP',
	closeModal: false
  }
};
function SendMail()
{

	swal({
	  title: 'Enter valid Enrollment and Phone number',
	  content: formTemplate,
	  button: {
		text: 'Send OTP',
		closeModal: false
	  }
	})
	.then(result => {
	  if (!result) throw null;
	  var reg_no  = $('#reg_no').val();
	  var phone = $('#phone').val();
	  return fetch(baseURL+`IIEST/SendOTPStudent/?reg_no=${reg_no}&phone=${phone}`);
	})
	.then(results => {
	  return results.json();
	})
	.then(json => {
		console.log(json);
		
		var stat = json.status;
		if(stat!='success'){
			const message = json.msg;
			const title = json.title;
			swal({
				title: title,
				text: message,
				icon: stat,
			})
		}else{
			$('#reg_no').val("");
			$('#phone').val("");
			swal({
				title: 'SMS',
				text: 'OTP has been send!',
				icon: stat,
			}).then((result)=>{
				window.open(baseURL+'IIEST/StudentPassword/?userid='+btoa(json.enroll), '_self');
			})
		}
	})
	.catch(err => {
	  if (err) {
		  console.log(err);
		swal("Oh no!", "The request failed!", "error");
	  } else {
		swal.stopLoading();
		swal.close();
	  }
	});
}