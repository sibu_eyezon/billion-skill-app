<section class="bg-white">
	<div class="container h-100">
	
		<div class="row">
			<div class="col-12">
				<div class="banner-text">
					<?php 
						echo '<h3 class="font-weight-bold text-center">'.$prog[0]->title.'</h3>';
						$type = trim($prog[0]->ptype);
						$category = trim($prog[0]->category);
						$curdate = strtotime(date('d-m-Y'));
						$ldt = strtotime(date('d-m-Y',strtotime($prog[0]->aend_date)));
						$sdt = strtotime(date('d-m-Y',strtotime($prog[0]->astart_date)));
					?>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-md-6">
				<?php if($curdate<=$ldt){ ?>
				<div class="card">
					<div class="card-header border-bottom">
						<h4 class="card-title font-weight-bold">Sign Up and Complete your Registration <small>(<span class="text-danger">*</span> are mandatory)</small></h4>
					</div>
					<form action="#" method="POST" id="frmsrm">
					<div class="card-body">
						<input type="hidden" name="pid" id="pid" value="<?php echo $prog[0]->id; ?>"/>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="fname" class="bmd-label-floating">First Name <span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="fname" name="fname">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="lname" class="bmd-label-floating">Last Name <span class="text-danger">*</span></label>
									<input type="text" class="form-control" id="lname" name="lname">
								</div>
							</div>
						</div>	
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email" class="bmd-label-floating">Mobile <span class="text-danger">*</span></label>
									<input type="number" class="form-control" id="mobile" name="mobile">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email" class="bmd-label-floating">Email Address <span class="text-danger">*</span></label>
									<input type="email" class="form-control" id="email" name="email">
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email" class="bmd-label-floating">University Register No.</label>
									<input type="text" class="form-control" id="identity_num" name="identity_num">
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email" class="bmd-label-floating">Category <span class="text-danger">*</span></label>
									<select class="form-control" name="category" id="category">
										<option value="">Select an option</option>
										<option value="Student">Student</option>
										<option value="Faculty">Faculty</option>
										<option value="Research Scholar">Research Scholar</option>
									</select>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-6">
								<div class="form-group">
									<label for="email" class="bmd-label-floating">Department <span class="text-danger">*</span></label>
									<select class="form-control" name="department" id="department">
										<option value="Management" selected>Management</option>
									</select>
								</div>
							</div>
							<div class="col-sm-6">
								<div class="form-group">
									<label for="newpass" class="bmd-label-floating">Password <span class="text-danger">*</span></label>
									<input type="password" class="form-control" id="newpass" name="newpass">
								</div>
							</div>
						</div>

						<div class="form-group">
							<input type="reset" value="reset" style="display:none;"/>
							<button type="submit" id="btn_save" class="btn btn-block btn-primary">Register</button>
						</div>
					</div>
					</form>
				</div>
				<?php }else{ ?>
				<div class="card bg-warning">
					<div class="card-body">
						<h4 class="text-center">Application is CLOSED.</h4>
						 <h6 class="text-center text-danger">Deadline : <?php echo ($prog[0]->aend_date)? date('jS F Y',strtotime($prog[0]->aend_date)): '';?>
						 <br>
						 <a href="<?= base_url(); ?>home/explore_programs" class="btn btn-md btn-success">Explore more programs</a>
						 </h6>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="col-md-6" style="background-image:url('<?= base_url('assets/images/srm.jpg'); ?>'); background-position: center; background-repeat: no-repeat; background-size: 100% 100%;"></div>
		</div>
		
	</div>
</section>
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>
<script>
	$('#frmsrm').on('submit', (e)=>{
		e.preventDefault();
		var frmData = new FormData($('#frmsrm')[0]);
		$.ajax({
			beforeSend: ()=>{
				$('#btn_save').attr('disabled', 'disabled');
				$('#btn_save').html('<i class="fa fa-spinner fa-spin"></i>Loading');
			},
			url: baseURL+'specials/user_registration',
			type: 'POST',
			data: frmData,
			processData: false,
			contentType: false,
			success: (res)=>{
				$('#btn_save').removeAttr('disabled');
				$('#btn_save').html('Register');
				var obj = JSON.parse(res);
				if(obj['status']){
					Swal.fire(
					  'Successfull',
					  obj['msg'],
					  'success'
					).then(()=>{window.href='https://learn.techmagnox.com/';});
				}else{
					Swal.fire(
					  'Error',
					  obj['msg'],
					  'error'
					);
				}
			},
			error: (err)=>{
				$('#btn_save').removeAttr('disabled');
				$('#btn_save').html('Register');
			}
		});
	});
</script>