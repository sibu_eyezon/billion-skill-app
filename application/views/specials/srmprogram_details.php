<style>
.is-invalid::placeholder {
    color: #dc3545 !important;
}
.list-columns{
	columns: 2 !important;
	-webkit-columns: 2 !important;
    -moz-columns: 2 !important;
}
.list-columns li {
	padding: 5px;
}
</style>
<?php
	if(!empty(json_decode($prog[0]->prog_skills))){
		$skills = $this->program_details_model->getAllProgramSkills(json_decode($prog[0]->prog_skills));
	}else{
		$skills = array();
	}
	$duration = trim($prog[0]->duration).' '.trim($prog[0]->dtype);
	//banner img setting
	$src = '';
	$banner = trim($prog[0]->banner);
	$intro = trim($prog[0]->intro_video_link);
	//Admission deadline
	$curdate = strtotime(date('d-m-Y'));
	$ldt = strtotime(date('d-m-Y',strtotime($prog[0]->aend_date)));
	$sdt = strtotime(date('d-m-Y',strtotime($prog[0]->astart_date)));
	//Fee Details
	$fee = trim($prog[0]->feetype);
	$amt = (int)$prog[0]->total_fee;
	$dis = (int)($prog[0]->discount);
	if($dis!=0){
		$famt = floatval($amt*(1-(floatval($dis/100))));
	}else{
		$famt = 0;
	}
	$cert = trim($prog[0]->certificate_sample);
	if($cert!=""){
		$remoteFile = 'https://learn.techmagnox.com//uploads/programs/'.$cert;
		$handle = @fopen($remoteFile, 'r');
		if(!$handle){
			$csrc = 'https://learn.techmagnox.com/uploads/programs/'.$cert;
		}else{
			$csrc = 'https://learn.techmagnox.com/uploads/programs/certificate20.jpg';
		}
	}else{
		$csrc = 'https://learn.techmagnox.com/uploads/programs/certificate20.jpg';
	}
	$eddt = strtotime(date('Y-m-d',strtotime($prog[0]->end_date)));
	$stdt = strtotime(date('Y-m-d',strtotime($prog[0]->start_date)));
	$category = trim($prog[0]->category);
?>
<section class="bg-white">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12 col-lg-6 align-self-center ">
				<div class="title text-left">
					<h2><?php echo trim($prog[0]->title);?></h2>
					<p class="mb-0">
					<?php 
						if($stdt != $eddt){ echo '<b>'.date('jS M Y',strtotime($prog[0]->start_date)).' to '.date('jS M Y',strtotime($prog[0]->end_date)).'</b>,'; } 
						if($category=='Seminar Program' || $category=='Webinar Program'){
							echo 'Webinar';
						}else{
							echo 'Instructor Led Training Program (Online)';
						}
						?> 
					</p>
					<div class="row my-3">
						<div class="col-12">
							<ul class="list-unstyled list-columns mb-3">
								<?php
								
									echo '<li class=""><i class="fa fa-check"></i> Starts on '.date('jS M Y',strtotime($prog[0]->start_date)).'</li>';
									if($duration != '1 day'){
										echo '<li class=""><i class="fa fa-check"></i> Duration '.$duration.'</li>';
									}
									echo '<li class=""><i class="fa fa-check"></i> '.$prog[0]->prog_hrs.' Hour(s)</li>';
									if($prog[0]->prog_level!="" && ($category!='Seminar Program' && $category!='Webinar Program')){
										if($prog[0]->prog_level=='3'){
											echo '<li class=""><i class="fa fa-check"></i> Level: Advanced</li>';
											echo '<li class=""><i class="fa fa-check"></i> Paid Internship</li>';
											echo '<li class=""><i class="fa fa-check"></i> Free Job Consultancy</li>';
											echo '<li class=""><i class="fa fa-check"></i> Assignments and Exams</li>';
										}else if($prog[0]->prog_level=='2'){
											echo '<li class=""><i class="fa fa-check"></i> Level: Beginners</li>';
											echo '<li class=""><i class="fa fa-check"></i> Assignments and Exams</li>';
										}else if($prog[0]->prog_level=='1'){
											echo '<li class=""><i class="fa fa-check"></i> Level: Intermediate</li>';
											echo '<li class=""><i class="fa fa-check"></i> Paid Internship</li>';
											echo '<li class=""><i class="fa fa-check"></i> Assignments and Exams</li>';
											echo '<li class=""><i class="fa fa-check"></i> Projects</li>';
										}
									}
									if(!empty($courses)){
										echo '<li class=""><i class="fa fa-check"></i> '.count($courses).' '.(($category=='Seminar Program' || $category=='Webinar Program')? 'Topic(s)' : 'Course(s)').'</li>';
									}
									if(!empty($skills)){
										echo '<li class=""><i class="fa fa-check"></i> '.count($skills).' Skills and Tools</li>';
									}
									if($category!='Seminar Program' && $category!='Webinar Program'){
										if(!empty($ind_proj)){
											echo '<li class=""><i class="fa fa-check"></i> Live Industry Project</li>';
										}
									}
								?>
							</ul>
						</div>
					</div>
					
					<div class="text-center">
						<p>
                        <?php
                            if(!empty($prog[0]->feetype)){
                                if($prog[0]->feetype === 'Paid'){
                                    if(!empty($prog[0]->discount) || $prog[0]->discount > 0){
                                        $percent = ($prog[0]->total_fee * $prog[0]->discount) / 100;
                                        $descount_price = $prog[0]->total_fee - (int)$percent;                                        
                                        echo "<h6><b>Fee : <del>Rs {$prog[0]->total_fee}</del>&nbspRs {$descount_price}</b></h6>";
                                    }else{
                                        echo "<h6><b>Fee : Rs {$prog[0]->total_fee}</b></h6>";
                                    }
                                }
                            }

							if($curdate<=$ldt){
								echo '<a class="btn btn-grad mr-2" href="'.base_url('srm_programAdmission/?id='.base64_encode($prog[0]->id)).'">Enroll Now</a>';
							}
                            if($prog[0]->program_brochure!=null){
                                if(file_exists('https://learn.techmagnox.com/uploads/programs/'.$prog[0]->program_brochure)){
                                    echo '<a href="https://learn.techmagnox.com/uploads/programs/'.$prog[0]->program_brochure.'" class="btn btn-dark">Download Brochure</a>';
                                }
                            }
                        ?>
                        <h6 >Deadline : <?php echo ($prog[0]->aend_date)? date('jS F Y',strtotime($prog[0]->aend_date)): '';?></h6>
						</p>
                    </div>
				</div>
			</div>
			<div class="col-md-10 col-lg-6 mx-md-auto align-self-center ">
				<img class="rounded img-responsive w-100" src="<?php echo 'https://learn.techmagnox.com/assets/img/banner/'.$banner; ?>" onerror="this.src=`https://learn.techmagnox.com/assets/img/sample.jpg`" alt="">
				<?php
					if($intro!=""){
						echo '<div class="position-absolute left-0 bottom-0 ml-4 ml-md-n2 mb-3">
								<a class="btn btn-grad" data-fancybox href="'.$intro.'"> <i class="fa fa-play text-white"></i>Play Video </a>
							</div>';
					}
				?>
				
			</div>
		</div>
	</div>
</section>
<section class="bg-light">
    <div class="container h-100 ">
        <div class="row">
            <div class="col-md-12 col-lg-8">
                <?php if(!empty($prog[0]->overview)):?>
                <div class="title ">
                    <h2 style="text-align:left">Overview</h2>
                </div>
                <p class="mb-0" style="text-align:justify">
                    <?php echo (!empty($prog[0]->overview))? $prog[0]->overview : '';?>
                </p>
                <?php endif;?>
                <br /><br />
                <?php if(!empty($benefits)):?>
				<div class="title ">
                    <h2 style="text-align:left">Benefits</h2>
                </div>
                <div class="row">
                <?php foreach($benefits as $row):?>
                    <div class="col-md-6 mt-4">
                        <div class="feature-box f-style-5 h-100 icon-grad">
                            <h3 class="feature-box-title text-primary "><?php echo(!empty($row->txt_benefit))? $row->txt_benefit : '';?></h3>
                            <p class="feature-box-desc"><?php echo(!empty($row->txt_benefit_dtls))? $row->txt_benefit_dtls : '';?></p>
                        </div>
                    </div>
                <?php endforeach;?>
                </div>
                <?php endif;?>
            </div>
            <div class="col-md-12 col-lg-4">
                <div style="text-align:center; position:relative">
                    <?php
                        if(!empty($prog[0]->feetype)){
                            if($prog[0]->feetype === 'Paid'){
                                if(!empty($prog[0]->discount) || $prog[0]->discount > 0){
                                    $percent = ($prog[0]->total_fee * $prog[0]->discount) / 100;
                                    $descount_price = $prog[0]->total_fee - (int)$percent;
                                    echo "<h6><b>Fee : <del>Rs {$prog[0]->total_fee}</del>&nbspRs {$descount_price}</b></h6>";
                                }else{
                                    echo "<h6><b>Fee : Rs {$prog[0]->total_fee}</b></h6>";
                                }
                            }
                        }
						if($curdate<=$ldt){
							echo '<a class="btn btn-grad mr-2" href="'.base_url('srm_programAdmission/?id='.base64_encode($prog[0]->id)).'">Enroll Now</a>';
						}
                        if($prog[0]->program_brochure!=null){
                            if(file_exists('./uploads/programs/'.$prog[0]->program_brochure)){
                                echo '<a href="'.base_url('uploads/programs/'.$prog[0]->program_brochure).'" class="btn btn-dark">Download Brochure</a>';
                            }
                        }
                    ?>
                    <h6 >Deadline : <?php echo ($prog[0]->aend_date)? date('jS F Y',strtotime($prog[0]->aend_date)): '';?></h6>
                </div>
                <div class="col-12">
                    <div class="title">
                        <br /><br />
                        <h4 style="text-align:center">Alumni Speaks</h4>
                    </div>
                </div>
                <div class="col-md-12 testimonials testimonials-border">
                    <div class="owl-carousel testi-full owl-grab" data-arrow="false" data-dots="false" data-items-xl="1" data-items-md="1" data-items-xs="1">
                        <?php
						foreach($alumnispk as $fbow){
							echo '<div class="item">
								<div class="testimonials-wrap">
									<div class="testi-text">
										<div>'.trim($fbow->txt_message).'</div>
										<div class="testi-avatar"> <img src="https://learn.techmagnox.com'.$fbow->txt_profile_pic.'" onerror="this.src=`'.base_url().'assets/images/thumbnails/default-avatar.png`" alt="avatar"> </div>
										<h6 class="mb-0 mt-3">'.trim($fbow->txt_name).'</h6>
										<h6 class="mb-0 mt-3">'.trim($fbow->txt_organization).'</h6>
										<h6 class="mb-0 mt-3">'.trim($fbow->txt_position).'</h6>
									</div>
								</div>
							</div>';
						}
						?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="team team-grid " style="background-color:#a098ea;">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12 mx-auto">
                <div class="title ">
                    <h2 style="text-align:left">Advisors and Instructors
					<a href="<?php echo site_url('home/instructor/ilist'); ?>" class="btn btn-grad btn-sm pull-right">All Instructors</a>
					</h2>
                </div>
            </div>
        </div>
        <!--<div class="row">
            <div class="col-12">
                <div class="owl-carousel owl-grab dots-primary" data-loop="false" data-arrow="false" data-dots="true" data-items-xl="3" data-items-xs="1">

                    <?php /*
                        if(!empty($instructors)):
                            foreach($instructors as $row):
                    ?>
                    <div class="item bg-white">
                        <div class="team-item">
                            <div class="team-avatar">
                                <img class="rounded" src="https://learn.techmagnox.com/assets/img/users/<?php echo $row->photo_sm;?>" alt="" onerror="this.src='https://learn.techmagnox.com/assets/img/default-avatar.png'">
                            </div>
                            <div class="team-desc">
                                <h4 class="team-name"><?php echo (!empty($row->name))? $row->name : ''?></h4>
                                <span class="team-position"><?php echo($row->designation)? $row->designation : ''?><?php echo($row->organization)? ' of '.$row->organization : ''?></span>
                                <p><?php echo (!empty($row->about_me))? $row->about_me : ''?></p>
                                <ul class="social-icons light si-colored-bg-on-hover no-pb">
                                    <li class="social-icons-item social-facebook"><a class="social-icons-link" href="<?php echo (!empty($row->facebook_link))? $row->facebook_link : 'javascript:void(0)';?>"><i class="fa fa-facebook"></i></a></li>
                                    <li class="social-icons-item social-instagram"><a class="social-icons-link" href="<?php echo (!empty($row->linkedin_link))? $row->linkedin_link : 'javascript:void(0)'?>"><i class="fa fa-instagram"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <?php
                            endforeach;
                        endif;
						*/
                    ?>
                </div>
            </div>

        </div>-->
    </div>
</section>
<section class="team team-grid bg-light" >
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12 mx-auto">
                <div class="title ">
                    <h2 style="text-align:left"><?= ($category=='Seminar Program' || $category=='Webinar Program')? 'Topic Contents' : 'Course Contents'; ?></h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-8 mb-5">
				<div class="accordion" id="accordion1">
					<?php
					if(count($sems)<= 1){
						if(!empty($courses)){
							$ci = 1;
							foreach($courses as $row){
					?>
					<div class="accordion-item">
						<div class="accordion-title">
							<a class="<?php echo ($ci == 1)? 'h6 mb-0' : 'collapsed'; ?>" data-toggle="collapse" href="#collapse-<?php echo $ci; ?>"><?php echo trim($row->title);?></a>
						</div>
						<div class="collapse <?php if($ci == 1){ echo 'show'; } ?>" id="collapse-<?php echo $ci; ?>" data-parent="#accordion1">
							<div class="accordion-content text-justify"> 
								<?php echo (!empty($row->overview))? $row->overview : '';?>
							</div>
						</div>
					</div>
					<?php
							$ci++;
							}
						}
					}else{
						
					}
					?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php if(!empty($skills)){ ?>
<section class="team team-grid" >
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12 mx-auto">
                <div class="title ">
                    <h2 style="text-align:left">Skills and Tools Covered</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 mb-5">
				<div class="list-group-inline list-group-number list-unstyled">
					<?php
							foreach($skills as $data): 
					?>
					<li class="list-group-item"><img src="<?php echo "https://learn.techmagnox.com/assets/img/skills/{$data->slogo}";?>" width="100" height="100"></li>
					<?php
							endforeach;
					?>
				</div>
			</div>
		</div>
	</div>
</section>
<?php } ?>
<section class="team team-grid bg-white" >
    <div class="container">
        <div class="row mt-1">
            <!-- feature 1 -->
            <div class="col-md-12">
                <div class="title ">
                    <h2 style="text-align:left">Sample Certificate</h2>
                </div>
            </div>
        </div>
        <div class="row mt-1">
            <div class="col-md-6"> <a class="mb-4 d-block" href="<?php echo $csrc; ?>" data-fancybox="portfolio" data-caption="Kitchen design"><img src="<?php echo $csrc; ?>" alt=""></a></div>
            <div class="col-md-6">
                <h3 class="mb-4 h1">Magnox Certificate Holders Work On Companies Like</h3>
				<div class="list-group-inline list-group-number list-unstyled">
					<li class="list-group-item"><img src="<?php echo base_url();?>assets/images/brand/cap.png" alt="" width="100"></li>
					<li class="list-group-item"><img src="<?php echo base_url();?>assets/images/brand/ibm.jpg" alt="" width="100"></li>
					<li class="list-group-item"><img src="<?php echo base_url();?>assets/images/brand/micro.png" alt="" width="100"></li>
					<li class="list-group-item"><img src="<?php echo base_url();?>assets/images/brand/tcs.png" alt="" width="100"></li>
				</div>
				<h5 style="text-align:left; line-height:32px">Industry recognized course completion certificate which will have a lifelong validity. Certificate can be verified from</h5>
            </div>
        </div>
    </div>
</section>
<section class="team team-grid">
	<?php
		$steps = ($prog[0]->apply_type === "0")? 2 : 3;
	?>
    <div class="container">
        <div class="row mt-1">
            <!-- feature 1 -->
            <div class="col-md-12">
                <div class="title ">
                    <h2 style="text-align:left">Application Process</h2>
                    <p>Candidates can apply to this <?php echo trim($prog[0]->title);?> in <?= $steps; ?> steps.</p>
                </div>
            </div>
        </div>
        <div class="row justify-content-center">
			<?php if($steps==2){ ?>
				<div class="col-md-3 mt-0 mr-3">
					<div class="feature-box f-style-2 icon-grad h-100">
						<h3 class="feature-box-title">Apply Now</h3>
						<p class="feature-box-desc">Apply for the program by clicking the link below. Incase of paid program, make necessary payment to complete your registration.</p>
						<a class="mt-3" href="https://learn.techmagnox.com/programAdmission/?id='<?= base64_encode($prog[0]->id); ?>" target="_blank">Enroll Now</a>
					</div>
				</div>
				<div class="col-md-3 mt-0">
					<div class="feature-box f-style-2 icon-grad h-100">
						<h3 class="feature-box-title">Approval</h3>
						<p class="feature-box-desc">The Administrator will check your application and will approve it soon. The approval confirmation will send through email.</p>
					</div>
				</div> 
			<?php }else{ ?>
				<div class="col-md-3 mt-0">
					<div class="feature-box f-style-2 icon-grad h-100">
						<h3 class="feature-box-title">Apply Now</h3>
						<p class="feature-box-desc">Apply for the program by clicking the link below. Incase of paid program, make necessary payment to complete your registration.</p>
						<a class="mt-3" href="https://learn.techmagnox.com/programAdmission/?id='<?= base64_encode($prog[0]->id); ?>" target="_blank">Enroll Now</a>
					</div>
				</div>
				<div class="col-md-3 mt-0">
					<div class="feature-box f-style-2 icon-grad h-100">
						<h3 class="feature-box-title">Application Review</h3>
						<p class="feature-box-desc">Once your application is received, the Administrator will review acccording to the criteria. You will be notify about your first approval/rejection via email or you can check your program status by login to your dashboard below.</p>
						<a class="mt-3" href="https://learn.techmagnox.com/" target="_blank">Login Now</a>
					</div>
				</div>
				<div class="col-md-3 mt-0">
					<div class="feature-box f-style-2 icon-grad h-100">
						<h3 class="feature-box-title">Admission</h3>
						<p class="feature-box-desc">After the payment is completed, the application will be finally approved and you can start learning.</p>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
</section>
<?php if(!empty($ind_proj) && ($category!='Seminar Program' && $category!='Webinar Program')){ ?>
<section class="team team-grid bg-light" >
    <div class="container">
        <div class="row mt-1">
            <!-- feature 1 -->
            <div class="col-md-12">
                <div class="title ">
                    <h2 style="text-align:left">Live Industry Projects
					<a href="<?php echo base_url('home/program_projects/?pid='.base64_encode($prog[0]->id)); ?>" target="_blank" class="btn btn-grad text-white ml-3 btn-sm pull-right">View All Projects</a>
					</h2>
                </div>
            </div>
        </div>
        <div class="row">
			<?php foreach($ind_proj as $iproj){ ?>
            <div class="col-md-4 mt-0">
                <div class="feature-box f-style-2 icon-grad h-100">
                    <h3 class="feature-box-title"><?php echo trim($iproj->txt_project); ?></h3>
                    <div class="feature-box-desc"><?php echo trim($iproj->txt_project_dtls); ?></div>
                </div>
            </div>
			<?php } ?>
        </div>
    </div>
</section>
<?php } ?>
<section class="team team-grid " >
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-12 mx-auto">
                <div class="title ">
                    <h2 style="text-align:left">FAQs</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12 col-lg-12 mx-auto">
                <div class="col-md-8 mb-5">
                    <div class="accordion accordion-line toggle-icon-left toggle-icon-round" id="accordion5">
                    <?php
                        if(!empty($faq)):
                            $fi = 1;
                            foreach($faq as $row):
                    ?>
                        <!-- item -->
                        <div class="accordion-item">
                            <div class="accordion-title">
                                <a class="<?php echo ($fi == 1)? 'h6 mb-0' : 'collapsed'?>" data-toggle="collapse" href="<?php echo '#collapse-faq-'.$fi;?>"><?php echo(!empty($row->txt_question))?$row->txt_question:''?></a>
                            </div>
                            <div class="<?php echo ($fi == 1)? 'collapse show' : 'collapse'?>" id="<?php echo 'collapse-faq-'.$fi;?>" data-parent="#accordion5">
                                <div class="accordion-content"><?php echo(!empty($row->txt_answer))?$row->txt_answer:''?></div>
                            </div>
                        </div>
                    <?php
                            $fi++;
                            endforeach;
                        endif;
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="contact-page  bg-light">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8 mx-auto">
                <div class="title text-center">
                    <h2>Still have Queries ? Contact Us</h2>
                </div>
            </div>
        </div>
        <div class="row mb-5">
            <div class="col-md-6">
                <div class="contact-box h-100 bg-overlay-dark-7 px-3 py-4" style="background:url(<?php echo base_url();?>assets/images/bg/04.jpg) no-repeat; background-size: cover; background-position: center center; ">
                    <!-- Phone -->
                    <div class="contact-info all-text-white">
                        <h5 class="mb-2">Address</h5>
                        <p>Magnox Technologies Pvt, Ltd. 7th Floor, Nasscom Warehouse, Mani Bhandar (7th Floor), Webel Bhaban, Sector V, Saltlake
                            Kolkata - 91 
                        </p>
                    </div>
                    <!-- Email -->
                    <div class="contact-info all-text-white">
                        <h5 class="mb-2">E-mail</h5>
                        <p>sougam@techmagnox.com, magnox.iitkgp@gmail.com</p>
                    </div>
                    <!-- Phone -->
                    <div class="contact-info all-text-white">
                        <h5 class="mb-2">Phone</h5>
                        <p>(+91) 9932242598, (+91) 6295622155</p>
                    </div>
                </div>
            </div>
            <!-- google map -->
            <!-- contact form -->
            <div class="col-md-6">
                <div class="h-100">
                    <p>Please fill in the form and an expert from the admissions office will call you in the next 4 working hours. </p>
                        <div class="row contact-msg" style="display: none;">
                            <div class="col-12">
                                <div class="alert alert-success" role="alert">
                                    Your message was sent successfully.
                                </div>
                            </div>
                        </div>
                        <div class="alert" id="form_submit_msg" style="display: none;"></div>
                        <div class="row" >
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- name -->
                                <span class="form-group">
                                <input id="first_name" name="name" type="text" class="form-control" placeholder="First Name">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- email -->
                                <span class="form-group">
                                <input id="last_name" name="email" type="text" class="form-control" placeholder="Last Name">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- name -->
                                <span class="form-group">
                                <input id="phone" name="name" type="text" class="form-control" placeholder="Phone">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- email -->
                                <span class="form-group">
                                <input id="email" name="text" type="email" class="form-control" placeholder="E-mail">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- name -->
                                <span class="form-group">
                                <input id="place" name="name" type="text" class="form-control" placeholder="Place">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- email -->
                                <span class="form-group">
                                    <select class="form-control mb-3" id="experience">
                                        <option value="">Select Experience Level</option>
                                        <option value="School Student">School Student</option>
                                        <option value="College Student">College Student</option>
                                        <option value="Freshers">Freshers</option>
                                        <option value="Less than 2 Yrs Exp">Less than 2 Yrs Exp</option>
                                        <option value="Less than 5 Yrs Exp">Less than 5 Yrs Exp</option>
                                        <option value="Less than 10 Yrs Exp">Less than 10 Yrs Exp</option>
                                        <option value="More than 10 Yrs Exp">More than 10 Yrs Exp</option>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-12" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <span class="form-group">
                                    <textarea class="form-control" id="message" rows="3" placeholder="message"></textarea>
                                </span>
                            </div>
                            <!-- submit button -->
                            <div class="col-md-12 text-center"><button class="btn btn-dark btn-block" onclick="sendMessage()">Send Message</button></div>
                        </div>
                        <!-- End main form -->
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#first_name').keyup(function(){
            $(this).removeClass('is-invalid');
            $(this).attr('placeholder','First Name');
        });

        $('#last_name').keyup(function(){
            $(this).removeClass('is-invalid');
            $(this).attr('placeholder','Last Name');
        });

        $('#experience').change(function(){
            $(this).removeClass('is-invalid');
            $('#experience option:first').html('Select Experience Level');
            $(this).removeAttr('style');
        });

        $('#email').keyup(function(){
            let value = $(this).val();
            let emailRegx = /^[A-za-z0-9._$#]{1,}@[A-Za-z0-9]{3,}[.]{1}[A-Za-z0-9.]{1,}$/
            if(emailRegx.test(value)){
                $(this).removeClass('is-invalid'); 
                $(this).addClass('is-valid');
                $(this).attr('style','color:#28a745');
            }else{
                $(this).addClass('is-invalid');
                $(this).attr('style','color: #dc3545');
            }
        });

        $('#phone').keyup(function(){
            let value = $(this).val();
            let phoneRegx = /^[4-9][0-9]{9}$/
            if(phoneRegx.test(value)){
                $(this).removeClass('is-invalid'); 
                $(this).addClass('is-valid');
                $(this).attr('style','color:#28a745');
            }else{
                $(this).addClass('is-invalid');
                $(this).attr('style','color: #dc3545');
            }            
        });

        $('#place').keyup(function(){
            $(this).removeClass('is-invalid');
            $(this).attr('placeholder','Place');
        });
    });

    function sendMessage(){
        let first_name  = $('#first_name').val();
        let last_name   = $('#last_name').val();
        let phone       = $('#phone').val();
        let email       = $('#email').val();
        let place       = $('#place').val();
        let experience  = $('#experience').val();
        let message     = $('#message').val();
        
        let isFirstName  = false;
        let isLastName   = false;
        let isEmail      = false;
        let isPhone      = false;
        let isPlace      = false;
        let isExperience = false;

        if(first_name){
            $('#first_name').removeClass('is-invalid');
            $('#first_name').attr('placeholder','First Name');
            isFirstName  = true;
        }else{
            $('#first_name').addClass('is-invalid');
            $('#first_name').attr('placeholder','First Name is required');
        }

        if(last_name){
            $('#last_name').removeClass('is-invalid');
            $('#last_name').attr('placeholder','Last Name');
            isLastName   = true;
        }else{
            $('#last_name').addClass('is-invalid');
            $('#last_name').attr('placeholder','Last Name is required');
        }

        if(phone){
            let phoneRegx = /^[4-9][0-9]{9}$/
            if(phoneRegx.test(phone)){
                $('#phone').removeClass('is-invalid'); 
                $('#phone').addClass('is-valid');
                $('#phone').attr('style','color:#28a745');
                isPhone = true;
            }else{
                $('#phone').addClass('is-invalid');
                $('#phone').attr('style','color: #dc3545');
            }
        }else{
            $('#phone').addClass('is-invalid');
            $('#phone').attr('placeholder','Phone Number is required');
        }

        if(email){
            let emailRegx = /^[A-za-z0-9._$#]{1,}@[A-Za-z0-9]{3,}[.]{1}[A-Za-z0-9.]{1,}$/
            if(emailRegx.test(email)){
                $('#email').removeClass('is-invalid'); 
                $('#email').addClass('is-valid');
                $('#email').attr('style','color:#28a745');
                isEmail      = true;
            }else{
                $('#email').addClass('is-invalid');
                $('#email').attr('style','color: #dc3545');
            }
        }else{
            $('#email').addClass('is-invalid');
            $('#email').attr('placeholder','Email is required');
        }

        if(place){
            $('#place').removeClass('is-invalid');
            $('#place').attr('placeholder','Place');
            isPlace = true;
        }else{
            $('#place').addClass('is-invalid');
            $('#place').attr('placeholder','Place is required');
        }

        if(experience){
            $('#experience').removeClass('is-invalid');
            $('#experience option:first').html('Select Experience Level');
            $('#experience').removeAttr('style');
            isExperience = true;
        }else{
            $('#experience').addClass('is-invalid');
            $('#experience option:first').html('Experience Level is required');
            $('#experience').attr('style','color: #dc3545');
        }

        if(isFirstName && isLastName && isPhone && isEmail && isPlace && isExperience ){
            $.ajax({
                url  : baseURL+'home/sendMessage',
                type : 'POST',
                data : {
                    first_name : first_name,
                    last_name  : last_name,
                    phone      : phone,
                    email      : email,
                    place      : place,
                    experience : experience,
                    message    : message
                },
                success: function(data){
                    $('#first_name').val('');
                    $('#last_name').val('');
                    $('#phone').val('');
                    $('#email').val('');
                    $('#place').val('');
                    $('#experience').val('');
                    $('#message').val('');  
                    $('#email').removeClass('is-valid');                  
                    $('#phone').removeClass('is-valid');
                    let resp = JSON.parse(data);
                    $('#form_submit_msg').fadeIn('slow').addClass(resp.class);
                    $('#form_submit_msg').fadeIn('slow').html(resp.msg);
                    setTimeout(function(){
                        $('#form_submit_msg').removeClass(resp.class).fadeOut('slow');
                        $('#form_submit_msg').html('').fadeOut('slow');
                    }, 10000);
                }
            })
        }
    }
</script>
