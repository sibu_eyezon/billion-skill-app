<div class="row">
	<?php
	if(!empty($programs)){
		$i=1;
		foreach($programs as $prow){
			$prog_id = $prow->id;
			$title = trim($prow->title);
			$fee = trim($prow->feetype);
			$amt = (int)$prow->total_fee;
			$dis = (int)($prow->discount);
			if($dis!=0){
				$famt = floatval($amt*(1-(floatval($dis/100))));
			}else{
				$famt = 0;
			}
			$curdate = strtotime(date('Y-m-d'));
		    $ldt = strtotime(date('Y-m-d',strtotime($prow->aend_date)));
		    $sdt = strtotime(date('Y-m-d',strtotime($prow->astart_date)));
			$eddt = strtotime(date('Y-m-d',strtotime($prow->end_date)));
		    $stdt = strtotime(date('Y-m-d',strtotime($prow->start_date)));
			$category = trim($prow->category);
			if($category!='Seminar Program' && $category!='Webinar Program'){
				$category = (($prow->prog_level=='3')? 'Certification' : (($prow->prog_level=='2')? 'Mentorship' : 'Live Buddies')).' Program';
			}
	?>
	<div class="col-sm-4 mb-2 item" data-title="<?= strtolower($title); ?>" data-price="<?= strtolower($fee); ?>" data-type="<?= strtolower($category); ?>">
		<div class="post">
			<a class="post-title" style="margin:0 !important;" href="<?php echo base_url('get_program_details/?id='.base64_encode($prog_id)); ?>">
			<img src="https://learn.techmagnox.com/assets/img/banner/<?php echo $prow->banner; ?>" style="width:100%; height: 230px;" alt="" onerror="this.src='https://learn.techmagnox.com/assets/img/sample.jpg'">
			</a>
			<div class="post-info" style="height:210px;">
				<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!"><?php echo $category; ?></a></span>
				
				<a class="post-title" href="<?php echo base_url('get_program_details/?id='.base64_encode($prog_id)); ?>"><?php echo $title; ?></a>
				<p class="mb-0" style="font-size:14px;">
				<?php
					  $dur = intval(trim($prow->duration));
					  echo 'Duration: '.$dur.' '.trim($prow->dtype).(($dur==1)? '':'s').';	Total Hours: '.trim($prow->prog_hrs).' Hrs<br>';
					  /*if($prow->prog_level!="" && ($category!='Seminar Program' && $category!='Webinar Program')){
						//								
							echo 'Level: <strong>'.(($prow->prog_level=='3')? 'Certification' : (($prow->prog_level=='2')? 'Mentorship' : 'Live Buddies')).'</strong>; ';
						}*/
					  echo (($fee=='Paid')? (($famt==0)? 'Rs '.$amt : 'Rs. strike>'.$amt.'</strike> '.$famt) : $fee).'<br>';
					  if($stdt == $eddt){
						  echo 'On: '.date('jS M Y',strtotime($prow->start_date)).'<br>';
					  }else{
						  echo 'From: '.date('jS M Y',strtotime($prow->start_date)).' To '.date('jS M Y',strtotime($prow->end_date)).'<br>';
					  }
					  echo 'Deadline: <span class="text-danger">'.(($curdate<=$ldt)? date('jS M Y',strtotime($prow->aend_date)) : 'Expired').'</span>';
				?>
				</p>
			</div>
		</div>
	</div>
	<?php 
		$i++; }
		}else{
			echo '<div class="col-sm-12"><h4 class="title text-center font-weight-bold">No program found. Please try different filters.</h4></div>';
		}
	?>
</div>