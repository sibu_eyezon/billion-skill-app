<!-- =======================
Banner innerpage -->
<section id="corporate" class="about-area pt-100 pb-70 bg-white">
	<div class="container">
		<div class="row justify-content-center">

			<div class="col-md-6 mb-3">
				<div class="card shadow">
					<div class="card-body">
						<h4 class="card-title" style="line-height:40px; color:#099"><b>Accelerate your Career, Achieve your Dreams through continious Upskilling and Career Support </b></h4>

						<ul class="list-group list-group-borderless list-group-icon-primary-bg mb-4">
							<li class="list-group-item"><i class="fa fa-check"></i> Analyze Your Ability </li>
							<li class="list-group-item"><i class="fa fa-check"></i> Sharpen Your Skills through Certification Program and Mentors Connect</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Mock Interviews, Challenges, Group Discussion, Doubt Clearning</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Internships with Live Projects </li>
							<li class="list-group-item"><i class="fa fa-check"></i> Interviews </li>
							<li class="list-group-item"><i class="fa fa-check"></i><b>Your Dream Job / Startups</b> </li>
						</ul>
					</div>
				</div>
			</div>
			<div class="col-md-6 mb-3">
				<div class="card-body">
					<h3 class="card-title" style="line-height:40px; color:#099; text-align:center"><b>Collaborations   </b></h3>
					<div class="row justify-content-center">
						<div class="col-sm-6 col-xs-6 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/mazars.png" width="100" />
						</div>
						<div class="col-sm-6 col-xs-6 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/pmaps.png" width="100" />
						</div>
						<div class="col-sm-6 col-xs-6 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/monnet.png" width="100" />
						</div>
						<div class="col-sm-6 col-xs-6 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/xigmapro.png" width="100" />
						</div>
						<div class="col-sm-6 col-xs-6 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/exide.png" width="100" />
						</div>
						<div class="col-sm-6 col-xs-6 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/thetaone.png" width="100" />
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="about-area pt-100 pb-70 bg-light">
		<div class="container">
			<div class="row justify-content-between align-items-center">
				<!--left-->
				<div class="col-lg-6 col-md-12">
					<div class="title text-left pb-0">

						<h2><span class="text-primary">Personalized </span> Solution</h2>
						<p> We Identify your problem and provide personalised solution to upgrade your skills and Career. Through the feedback identified We provide the following solution to upgrade yourself : </p>
                         <ul class="list-group list-group-borderless list-group-icon-primary-bg mb-4">
							<li class="list-group-item"><i class="fa fa-check"></i> 1-1 Mentoring </li>
							<li class="list-group-item"><i class="fa fa-check"></i> Doubt Clearning</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Mock Interviews</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Group Discussions</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Hands-on and Practice Projects</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Free Webinars </li>
						</ul>
                        <a class="btn btn-grad mr-3" href="<?php echo site_url('home/explore_programs'); ?>">Explore Programs</a>
						<a class="primary-hover text-light-gray" href="<?php echo site_url('home/events'); ?>">Free Webinars</a>
					</div>
				</div>
				<!--right-->
				<div class="col-md-6">
					<div class="row mt-4 mt-md-0">
						<div class="col-7 pl-4 pr-2 mb-3">
							<img class="border-radius-3 wow fadeInLeft" data-wow-duration="0.8s" data-wow-delay="0s" src="<?php echo base_url();?>assets/images/service/02.jpg" alt="">
						</div>
						<div class="col-5 align-self-end pl-2 mb-3">
							<img class="border-radius-3 wow fadeInDown" data-wow-duration="0.8s" data-wow-delay="0.2s" src="<?php echo base_url();?>assets/images/service/01.jpg" alt="">
						</div>
						<div class="col-5 offset-1 px-2 mb-3">
							<img class="border-radius-3 wow fadeInUp" data-wow-duration="0.8s" data-wow-delay="0.4s" src="<?php echo base_url();?>assets/images/service/03.jpg" alt="">
						</div>
						<div class="col-5 px-2 mb-3">
							<img class="border-radius-3 wow fadeInRight" data-wow-duration="0.8s" data-wow-delay="0.6s" src="<?php echo base_url();?>assets/images/service/04.jpg" alt="">
						</div>
					</div>
				</div>
			</div>
		</div>
</section>

<section id="corporate" class="about-area pt-100 pb-70 bg-white">
	<div class="container">

		<div class="row">
			<div class="col-xl-6">
				<h1 class="text-center"> Alumni Testimonials </h1>
				<div class="col-md-12 testimonials testimonials-border py-6">
					<div class="owl-carousel testi-full owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="1" data-items-md="1" data-items-xs="1">
						<?php
							foreach($alumnispk as $fbow){
								echo '<div class="item">
									<div class="testimonials-wrap">
										<div class="testi-text">
											<div>'.trim($fbow->txt_message).'</div>
											<div class="testi-avatar"> <img src="<?php echo base_url();?>https://learn.techmagnox.com'.$fbow->txt_profile_pic.'" onerror="this.src=`'.base_url().'assets/images/thumbnails/default-avatar.png`" alt="avatar"> </div>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_name).'</h6>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_organization).'</h6>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_position).'</h6>
										</div>
									</div>
								</div>';
							}
						?>
					</div>
				</div>
			</div>
            <div class="col-xl-6">
				<div class="section-title mb-50 text-left">
					<h4 class="mb-0">Magnox Degree Holders Work in Companies like:

					</h4><br />
					<div class="row">
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/tcs.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/wipro.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/pws.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/micro.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/ibm.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/cap.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/thetaone.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/nasscom.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/indusnet.png" width="100" />
						</div>
							<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/mazars.png" width="100" />
							<!--<img src="<?php //echo base_url();?>assets/images/brand/sun.png" width="100" />-->
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/monnet.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/exide.png" width="100" />
						</div>
					</div>
				</div>
				<br><a href="<?php echo site_url('home/hiring_partners'); ?>" class="btn btn-grad btn-sm pull-right">Know More</a><br>
			</div>
		</div>
	</div>
</section>

<section id="corporate" class="pt-100 pb-70 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="section-title mb-50 text-center">
					<div class="section-title-heading mb-20">
						<h1 class="text-middle"><br />30 + Programs, 50 + Skills, 4500 + Alumni, 7800 + Users
                        <br /><br />
                        <a href="<?php echo base_url('home/explore_programs'); ?>" class="btn btn-grad mb-0 mr-3 float-center">Explore Programs Now</a>

						</h1>
					</div>

				</div>
			</div>
		</div>
	</div>
</section>
