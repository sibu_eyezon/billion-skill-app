<style>
.kicon{
	font-size: 1.2rem;
    font-weight: 900;
}
</style>
<section class="p-0">
	<div class="container-fluid p-0">
		<div class="swiper-container height-350-responsive swiper-arrow-hover ">
			<div class="swiper-slide " style="background-image:url(<?php echo base_url()?>assets/images/bg/07.jpg); background-position: center center; background-size: cover;">
				<div class="container d-flex h-100">
					
					<div class="col-12 justify-content-center text-center py-5">
						<h1 class="display-5.5 font-weight-bold; text-align:center;"> <span class="text-primary">Upskilling Ecosystem</span> for better Career <span class="text-primary">and Jobs</span></h1>
						<h5 class="display-9 font-weight-normals" style="line-height:30px"><b>Learn <span class="text-primary">High Demand Skill Sets</span> from <span class="text-primary">top Industry Professionals</span> and increase chances of getting <span class="text-primary">better jobs</span> by 85%</b></h5>
						
						<div class="d-flex justify-content-center align-content-center">
						  
						  <div class="p-2 flex-fill" style="background-color:#f7e394; padding:8px">
							<h5 style="line-height:32px;"><b>Directly Connect with <span class="text-primary">Industry</span> Mentor</b><br><a href="<?php echo site_url('home/explore_programs'); ?>"><u>Joining Starts at Rs 999/- PM </u></a>   </h5>
                            <a class="mt-3 pull-left" href="<?php echo base_url().'home/lms/mentor_connect';?>"  style=" display:inline; color:#000"> Know More <span class="fa fa-angle-right kicon"></span></a>
						  </div>
						  <div class="p-2 flex-fill" style="background-color:#b7ebfc; padding:8px">
							<h5 style="line-height:32px;"><b> <span class="text-primary">Webinars </span> and <span class="text-primary">Workshops </span>  </b><br><a href="<?php echo site_url('home/events'); ?>"> <u>Join for Free</u> </a></h5>
							<a class="mt-3 pull-left" href="<?php echo base_url().'home/lms/webinars-workshops';?>"  style=" display:inline; color:#000"> Know More <span class="fa fa-angle-right kicon"></span></a>
						  </div>
						  <div class="p-2 flex-fill" style="background-color:#aaf7be; padding:8px">
							<h5 style="line-height:32px;"><b><span class="text-primary">Certification</span> Program </b>with Paid Internship.<br><a href="<?php echo site_url('home/explore_programs'); ?>"><u>Explore Now </u></a> </h5>
                            <a class="mt-3 pull-left" href="<?php echo base_url().'home/lms/certifications';?>"  style=" display:inline; color:#000"> Know More <span class="fa fa-angle-right kicon"></span></a>
						  </div>
						  
						</div>
						
						<h5 class="p-1 font-weight-bold">Are you an <span class="text-primary">Industry Professional?</span> 	<a class="btn btn-grad btn-sm " href="<?php echo site_url('home/instructor/ibenefits'); ?>" style="color:#fff">Join as an Mentor</a></h5>
						
					</div>
					
				</div>
			</div>
		</div>
	</div>
</section>



<section class="mt-n6 bg-light">
	<div class="container position-relative" style="z-index:1;">
		
		<div class="row">
			<div class="col-md-4">
				<div class="feature-box f-style-2 icon-grad" style="background-color:#32ae53; padding-top:5px; margin-top:5px">
					<h5 class="feature-box-title" style="color:#fff"><b>Institutes/ Colleges </b></h5>
					<a class="mt-3" href="<?php echo base_url().'home/organizations';?> " style="display:inline; color:#fff"> Know how can we minimize the Skill Gap > </a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="feature-box f-style-2 icon-grad" style="background-color:#4285fa; padding-top:5px; margin-top:5px">
					<h4 class="feature-box-title" style="color:#fff"><b>Industry</b></h4>

                    	<a class="mt-3" href="<?php echo site_url('home/hiring'); ?>"  style=" display:inline; color:#fff"> Know how can we benefit each other ></a>
				</div>
			</div>

			<div class="col-md-4">
				<div class="feature-box f-style-2 icon-grad" style="background-color:#fbb305; padding-top:5px; margin-top:5px">
					<h4 class="feature-box-title" style="color:#ffffff"><b>Hands-on Facilities</b></h4>

					<a class="mt-3" href="<?php echo base_url().'home/corporate';?>" style="display:inline; color:#fff"> Better facility for you or your organization ></a>
					</div>
			</div>
		</div>
		
	</div>
</section>

<section class="client py-5 bg-light">
		<div class="container">
        <div class="row">
			<div class="col-12 col-lg-12">
				<div class="title text-center">
					
					<h2>Association
						<a class="btn btn-grad btn-sm pull-right" href="<?php echo site_url('home/explore_programs'); ?>" style="color:#fff">Explore All</a>
					</h2>
					<p>Our Association with different Organizations including Companies, MNCs, Institutes and Universities </p>
				</div>
			</div>
		</div>
			<div class="row">
				<div class="col-md-12">
				<data-margin="10" data-loop="true" data-callback="true" data-mergeFit="false" data-arrow="false" data-dots="false" data-items-xl="6" data-items-lg="6" data-items-md="6" data-items-sm="3" data-items-xs="2">
					<div class="owl-carousel owl-grab collaborations">
						<div class="item"><img src="assets/images/collaboration/1.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/2.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/3.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/4.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/5.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/6.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/7.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/8.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/9.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/10.jpg" alt=""></div>
						<div class="item"><img src="assets/images/collaboration/11.jpg" alt=""></div>
	    	            <div class="item"><img src="assets/images/collaboration/12.jpg" alt=""></div>
	    	            <div class="item"><img src="assets/images/collaboration/13.jpg" alt=""></div>
					</div>
				</div>
			</div>
		</div>
	</section>
<section class="blog ">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="title text-center">
					
					<h2>All Programs</h2>
					<p>Learn Certification programs from top industrial experts on the most in-demand skillsets.</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel post-style-3 arrow-dark arrow-hover" data-dots="false" data-items-xl="3" data-items-lg="3" data-items-md="2" data-items-sm="2" data-items-xs="1">
					<?php
					if(!empty($programs)){
						$i=1;
						foreach($programs as $prow){
							$prog_id = $prow->id;
							$title = str_replace(" ","_",strtolower(trim($prow->title)));
							$fee = trim($prow->feetype);
					?>
					<div class="item">
						<div class="post">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!"><?php echo trim($prow->category); ?></a></span>
								<div class="post-author"><a href="#!"><a></div>
								<a class="post-title" href="<?php echo base_url('get_program_details/?id='.base64_encode($prog_id)); ?>"><?php echo trim($prow->title); ?></a>
								<p class="mb-0">
								<?php
									  $curdate = strtotime(date('Y-m-d'));
									  $ldt = strtotime(date('Y-m-d',strtotime($prow->aend_date)));
									  $sdt = strtotime(date('Y-m-d',strtotime($prow->astart_date)));
									  $dur = intval(trim($prow->duration));
									  echo 'Duration: '.$dur.' '.trim($prow->dtype).(($dur==1)? '':'s').'	;	Total hrs: '.trim($prow->prog_hrs).' Hrs.
									  <h6 class="text-left">'.(($fee=='Paid')? 'Rs '.$prow->total_fee : $fee).'</h6>
									  <h6 class="text-left">From: '.date('jS M Y',strtotime($prow->start_date)).' To '.date('jS M Y',strtotime($prow->end_date)).'</h6>
									  <h6 class="text-left text-danger">Deadline: '.(($curdate<=$ldt)? date('jS M Y',strtotime($prow->aend_date)) : 'Expired').'</h6>';
									  if($sdt!=19800 || $ldt!=19800){
										if($curdate<$sdt){
											echo '<h5 class="text-center text-primary">Registration will start on '.date('jS M Y',strtotime($prow->astart_date)).'</h5>';
										}
										
									}
								?>
								</p>
							</div>
						</div>
					</div>
					<?php 
						$i++; }
						}
					?>
				</div>
				<a class="btn btn-grad btn-sm pull-right" href="<?php echo site_url('home/explore_programs'); ?>" style="color:#fff">Explore All</a>
			</div>
		</div>
	</div>
</section>

 <section class="bg-light">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="title text-center">

					<h2>Advisors and Mentors</h2>
					<p>World Class Advisors and Industry Mentors to strengthen your base, guide you.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="5" data-items-xs="1">
					<?php if(!empty($instructor)){ 
						foreach($instructor as $irow){ 
							$name = trim($irow->txt_instructor_name);
							$designation = trim($irow->txt_instructor_desg);
							$photo_sm = trim($irow->txt_profile_pic); 
					?>
					<div class="item">
						<div class="team-item">
							<div class="team-avatar">
								<img class="img-responsive" src="<?php echo 'https://learn.techmagnox.com/'.$photo_sm; ?>" onerror="this.src='<?= base_url('assets/images/people/default-avatar.png'); ?>'">
							</div>
							<div class="team-desc">
								<h5 class="team-name"><?= $name; ?></h5>
								<span class="team-position"><?= $designation; ?></span>
								<p class="text-justify"><?= substr(trim($irow->txt_instructor_dtls), 0, 60).'...'; ?></p>
							</div>
						</div>
					</div>
					<?php } } ?>

				</div>
				<a href="<?php echo site_url('home/instructor/ilist'); ?>" class="btn btn-grad btn-sm pull-right">All Instructors</a>
			</div>
		</div>
	</div>
</section>

<section class="blog">
	<div class="container">
                <div class="row">
			<div class="col-12 col-lg-12">
				<div class="title text-center">
					
					<h2>Industry Demand Skill Sets</h2>
					<p>These are the most in-demand skillsets at present in the Industry, here getting jobs can be increased by 70%.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<!-- info -->
			
			<!-- image -->
			<div class="col-md-12 col-lg-8 bg-light mx-auto">
	
				<div class="row mt-1 text-top">
					<?php
						foreach($skills as $skow){
							echo '<div class="col-lg-2 col-md-2 col-sm-12 col-xs-12 mt-30">
									<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
										<div><img src="https://learn.techmagnox.com/assets/img/skills/'.trim($skow->slogo).'" width="70px" /></div>
									</div>
								</div>';
						}
					?>
				</div>
				<div class="row">
					<div class="col-sm-12 mt-2 mt-md-4">
						<div class="text-center">
							<a class="btn btn-grad mb-0 mr-3" href="<?php echo site_url('home/skills'); ?>">See All Skills</a>
							
						</div>
					</div>
				</div>
	
			</div>
		</div>
	</div>
</section>

<section class="bg-light">
	<div class="container h-100">
		<div class="row py-4 counter counter-light counter-big counter-grad-text justify-content-center mb-5 mb-lg-0">
			<div class="col-sm-3">
				<div class="counter-item text-center">
					<i class="counter-item-icon material-icons">auto_stories</i>
					<h2 class="counter-item-digit mb-0" data-from="0" data-to="<?= $counter[0]->program_count; ?>" data-speed="3000" data-refresh-interval="10"><?= $counter[0]->program_count; ?></h2>
					<p class="counter-item-text">Programs</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="counter-item text-center">
					<i class="counter-item-icon material-icons">account_circle</i>
					<h2 class="counter-item-digit mb-0" data-from="0" data-to="<?= $counter[0]->student_count; ?>" data-speed="3000" data-refresh-interval="10"><?= $counter[0]->student_count; ?></h2>
					<p class="counter-item-text">Students</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="counter-item text-center">
					<i class="counter-item-icon material-icons">school</i>
					<h2 class="counter-item-digit mb-0" data-from="0" data-to="<?= $counter[0]->intrsuctor_count; ?>" data-speed="3000" data-refresh-interval="10"><?= $counter[0]->intrsuctor_count; ?></h2>
					<p class="counter-item-text">Instructors</p>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="counter-item text-center">
					<i class="counter-item-icon material-icons">event_note</i>
					<h2 class="counter-item-digit mb-0" data-from="0" data-to="<?= $counter[0]->event_count; ?>" data-speed="3000" data-refresh-interval="10"><?= $counter[0]->event_count; ?></h2>
					<p class="counter-item-text">Workshops</p>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-6 text-center"><div class="title text-center">
				<h2 class="team-name text-center">Alumni Speak</h2></div>
				<div class="col-md-12 testimonials testimonials-border py-6">
					<div class="owl-carousel testi-full owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="1" data-items-md="1" data-items-xs="1">
						<?php
							foreach($alumnispk as $fbow){
								echo '<div class="item">
									<div class="testimonials-wrap">
										<div class="testi-text">
											<div>'.trim($fbow->txt_message).'</div>
											<div class="testi-avatar"> <img src="https://learn.techmagnox.com'.$fbow->txt_profile_pic.'" onerror="this.src=`'.base_url().'assets/images/thumbnails/default-avatar.png`" alt="avatar"> </div>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_name).'</h6>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_organization).','.trim($fbow->txt_position).'</h6>
											
										</div>
									</div>
								</div>';
							}
						?>
					</div>
				</div>
                              
				<a class="btn btn-grad mb-0 ml-3 btn-sm text-center" href="<?php echo site_url('home/view_feedback'); ?>">View All Feedbacks</a>
			</div>
			<div class="col-sm-6 col-md-6 text-center">
				<div class="section-title mb-50 text-center">
					<div class="title text-center">
						<h2 class="mb-0">Our Alumni are Working in Companies like:  </h2>
					</div>
					<div class="row">
						<div class="col-sm-3 col-xs-6 text-center">
							<img src="<?= base_url(); ?>assets/images/brand/tcs.png" />
						</div>
						<div class="col-sm-3 col-xs-6 text-center">
							<img src="<?= base_url(); ?>assets/images/brand/wipro.png" />
						</div>
						<div class="col-sm-3 col-xs-6 text-center">
							<img src="<?= base_url(); ?>assets/images/brand/pws.png" />
						</div>
						<div class="col-sm-3 col-xs-6 text-center">
							<img src="<?= base_url(); ?>assets/images/brand/cognizant.png" />
						</div>
						<div class="col-sm-3 col-xs-6 text-center">
							<img src="<?= base_url(); ?>assets/images/brand/micro.png" />
						</div>
						<div class="col-sm-3 col-xs-6 text-center">
							<img src="<?= base_url(); ?>assets/images/brand/cap.png" />
						</div>
						<div class="col-sm-3 col-xs-6 text-center">
							<img src="<?= base_url(); ?>assets/images/brand/amazon.png" />
						</div>
						<div class="col-sm-3 col-xs-6 text-center">
							<img src="<?= base_url(); ?>assets/images/brand/ibm.png" />
						</div>
					</div>
				</div>
                <a class="btn btn-grad mb-0 ml-3 btn-sm text-center" href="<?php echo site_url('home/hiring_partners'); ?>">Know More</a>
            </div>		
		</div>
	</div>
</section>

<section class="client bg-light pt-6">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12 mx-auto">
				<div class="title text-center">
					<h2>Awards and Achievements </h2>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel owl-grab arrow-hover arrow-gray" data-margin="40" data-arrow="true" data-dots="false" data-items-xl="5" data-items-lg="3" data-items-md="3" data-items-sm="3" data-items-xs="2">
					<div class="item"><img src="<?php echo base_url();?>assets/images/awards/iew.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/awards/europe.jpg" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/awards/apacI.png" alt=""></div>
                    <div class="item"><img src="<?php echo base_url();?>assets/images/awards/iaw.jpg" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</section>
<script>
	$(function() {
		$('.collaborations').owlCarousel({
			nav: false,
			loop: true,
			margin: 30,
			dots: true,
			autoplay: false,
			autoplaySpeed: 2200,
			autoplayTimeout: 2200,
			autoplayHoverPause: true,
			slideTransition: 'linear',
			responsive: {
				0: {
					items: 3
				},
				576: {
					items: 3
				},
				768: {
					items: 6
				},
				992: {
					items: 8
				}
			}
		});
	})
</script>
