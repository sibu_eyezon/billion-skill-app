<?php
if(!empty($jobs)){
foreach($jobs as $jbs){
	$title = trim($jbs->title);
	$org_name = trim($jbs->org_name);
	$exp = trim($jbs->experience);
	$salary = trim($jbs->salary);
	$city = trim($jbs->location);
	$desc = strip_tags(trim($jbs->desc));
?>
<div class="card shadow mb-4">
	<div class="card-body">
		<h5 class="card-title"><a href="<?= base_url('home/job_details/?id='.base64_encode($jbs->id)); ?>"><?= $title; ?></a></h5>
		<p class="mb-0 text-uppercase"><?= $org_name; ?></p>
		<p class="mb-0">
			<span class="fa fa-briefcase"></span> <?= (($exp!="")? $exp : "Not disclosed"); ?>
			<span class="fa fa-inr ml-2"></span> <?= (($salary!="-")? $salary : "Not disclosed"); ?>
			<span class="fa fa-location-arrow ml-2"></span> <?= (($city!="")? $city : "Not disclosed"); ?>
		</p>
		<div class="mb-0">
			<span class="fa fa-file-text-o"></span> <?= substr($desc, 0, 80).'...'; ?>
		</div>
	</div>
	<div class="card-footer">
		<div class="stats">
			<span class="fa fa-clock-o"></span> <?= date('jS M Y', strtotime($jbs->create_date_time)); ?>
		</div>
	</div>
</div>
<?php } }else{ echo '<h4 class="text-center text-danger">No Jobs Found!!!</h4>'; } ?>