<?php defined('BASEPATH') OR exit('No direct script access allowed'); ?>
<!doctype html>
<html lang="en">
<head>
	<meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title><?php echo $pageTitle; ?></title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="shortcut icon" type="image/x-icon" href="<?php echo site_url('assets/img/bslogo.png'); ?>">
	<meta name="author" content="techmagnox.com">
	<meta name="description" content="Skill Assessment, Learning">

	<!-- Favicon -->
	<link rel="shortcut icon" href="<?php echo base_url();?>assets/images/favicon.ico">

	<!-- Plugins CSS -->
	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/font-awesome/css/font-awesome.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/themify-icons/css/themify-icons.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/animate/animate.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/fancybox/css/jquery.fancybox.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/owlcarousel/css/owl.carousel.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/vendor/swiper/css/swiper.min.css" />
	<!-- Theme CSS -->
	<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/css/style.css" />
	<script src="<?php echo base_url();?>assets/vendor/jquery/jquery.min.js"></script>
	<script>var baseURL = '<?php echo site_url(); ?>'; </script>
</head>

<body>
	<div class="preloader">
		<img src="<?php echo base_url();?>assets/images/Preloader_3.gif" alt="Pre-loader">
	</div>
	
	<?php
		include 'header.php';
		include $pageName.'.php';
		include 'footer.php';
	?>
	
	<div> <a href="#" class="back-top btn btn-grad"><i class="ti-angle-up"></i></a> </div>
	<!--Global JS-->
	<script src="<?php echo base_url();?>assets/vendor/popper.js/umd/popper.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/jquery-easing/jquery.easing.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/jquery-countTo/jquery.countTo.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/jquery-appear/jquery.appear.js"></script>
	<!--Vendors-->
	<script src="<?php echo base_url();?>assets/vendor/fancybox/js/jquery.fancybox.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/owlcarousel/js/owl.carousel.min.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/swiper/js/swiper.js"></script>
	<script src="<?php echo base_url();?>assets/vendor/wow/wow.min.js"></script>
	<!--Template Functions-->
	<script src="<?php echo base_url();?>assets/js/functions.js"></script>
</body>
</html>