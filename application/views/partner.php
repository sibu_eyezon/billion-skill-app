<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4">Become a Partner</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
		<div class="row">
			<div class="col-md-4 mb-2">
				<?php
					if($this->session->flashdata('errors')!=""){
						echo '<div class="alert alert-warning alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
						  '.$this->session->flashdata('errors').'
						</div>';
					}
				?>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Fill out the Partnership form</h4>
					</div>
					<div class="card-body">
						<form action="<?php echo site_url('home/insertPartner'); ?>" method="POST">
							<div class="form-group">
								<label>First name <span class="text-danger">*</span></label>
								<input type="text" name="fname" id="fname" class="form-control"/>
							</div>
							<div class="form-group">
								<label>Last name <span class="text-danger">*</span></label>
								<input type="text" name="lname" id="lname" class="form-control"/>
							</div>
							<div class="form-group">
								<label>Email <span class="text-danger">*</span></label>
								<input type="email" name="email" id="email" class="form-control"/>
							</div>
							<div class="form-group">
								<label>Phone <span class="text-danger">*</span></label>
								<input type="text" name="phone" id="phone" class="form-control"/>
							</div>
							<div class="form-group">
								<label>Organization <span class="text-danger">*</span></label>
								<input type="text" name="org" id="org" class="form-control"/>
							</div>
							<div class="form-group">
								<label>Designation <span class="text-danger">*</span></label>
								<input type="text" name="position" id="position" class="form-control"/>
							</div>
							<div class="form-group">
								<label>Address</label>
								<textarea rows="10" cols="80" name="address" id="address" class="form-control w-100"></textarea>
							</div>
							<div class="form-group justify-content-center">
								<button type="submit" class="btn btn-md btn-success">Submit</button>
							</div>
						</form>
					</div>
				</div>
				
			</div>
			<div class="col-md-8">
			
			</div>
		</div>
	</div>
</section>