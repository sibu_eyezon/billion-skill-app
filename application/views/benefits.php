<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4">LMS - Benefits</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
    <div class="row">
			<div class="col-sm-9 col-md-9">
<ul class="list-group list-group-borderless list-group-icon-primary-bg mb-4">
    <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Brand Value: </b> Magnox Cloud Learning has been titled as the Best Cloud Learning Platform of East India by APAC Insiders ( one of the Worlds Learning Business Magazine). We are also being awarded as Indian Excellance Award 2020. We have more than 50 happy b2b Clients and  10 Organizations using our LMS Platform.</p>  </li>
	<li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Cutomizable system, Plugin Based Architechture : </b> Magnox Learning is an customizable platform and is made for all types of Programs whether it is certification or degree program or corporate training or tution. We can customize anything for your organization. The architecture is build is such a way that it will fit to any organization, whether it is College, School or any other organizations. </p> </li>
	<li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Integrated : </b> Our LMS is integrated with different process management system which helps in smooth running of the Academics in an organization. The processes are Pre-Admission, Admission, Student Management - Promotion, Faculty Management, Course and Routine Management, Examination etc. </p>  </li>
    <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Scalable : </b> Installed in AWS Cloud and is capable of hadeling any no of requests. Automatic backups are taken and we can assure no loss will be there in data.  </p>  </li>
    
      <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Excellent UX, Easy to use  : </b> The hierarchy structure of the users are maintained in the system.  </p>  </li>

     <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Hierarchy  : </b> The hierarchy structure of the users are maintained in the system.  </p>  </li>
     <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Online Class  : </b> Excellent Platform for Online Class, can work in low bandwidth as well. Attendance will be automatically taken in the online Class.  </p>  </li>
     <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Virtual Labs   : </b> The Virtual labs will facilitate to carry out the hands-on experience. Now we have coding and database labs. In coding we have java, python, php etc. </p>  </li>
	 <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Examination   : </b> The Examination is one of the most vital module of this product. Different type of Tests or Assessments like Single Answer MCQ, Multi Answer MCQ, Essay Type Questions, Text, Numeric, Matrix etc are there. Camera detection for Cheating prevention are also there. </p>  </li>	
      <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Communication   : </b> We have different means and different types of communication. Notice, Message, Chat, Group Chat etc. One Way Communication, Both Way Communication, Chat Room, Notification etc. </p>  </li>
      <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Assessment and Repositories: </b> For Assessment we have Examination and Assignment. For Repositories we have Lectures, Resources. We are also slowly bringing the concept of Digital Locker for Students.</p>  </li>					
					</ul>
		</div>
			<div class="col-sm-3 col-md-3 bg-light"><img src="<?php echo base_url();?>assets/images/1.jpg" alt=""></div></div>
	</div>
</section>