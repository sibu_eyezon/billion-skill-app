<?php
	$org_name = trim($job[0]->org_name);
	$exp = trim($job[0]->experience);
	$salary = trim($job[0]->salary);
	$city = trim($job[0]->location);
	$skills = $this->user_model->get_jobs_skills($job[0]->id);
?>
<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4"><?= trim($job[0]->title); ?></h2>
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section id="about" class="about-area pt-100 pb-70">
    <div class="container">
		<div class="row">
		
			<div class="col-md-8">
				<h2 class="mb-4">Job description</h2>
				<?= trim($job[0]->desc); ?>
				<?php if(!empty($skills)){ ?>
				<h6 class="mt-4 mb-2 font-weight-bold">Skills</h6>
				<ul>
					<?php foreach($skills as $skill){
						echo '<li>'.$skill->name.'</li>';
					} ?>
				</ul>
				<?php } ?>
				<h6 class="mt-4 mb-2 font-weight-bold">Educational requirements</h6>
				<ul>
					<li><?= trim($job[0]->degree_name); ?></li>
				</ul>
				
				<div class="row mt-5">
					<div class="col-md-12">
						<h2 class="mb-3">Apply for this Job</h2></div>
					<div class="col-md-6"><div class="form-group"><input type="text" class="form-control" placeholder="Name"></div></div>
					<div class="col-md-6"><div class="form-group"><input type="email" class="form-control" placeholder="E-mail"></div></div>
					<div class="col-md-6"><div class="form-group"><input type="text" class="form-control" placeholder="Mobile number"></div></div>
					<div class="col-md-6 input-group mb-4">
						<div class="custom-file">
							<input type="file" class="custom-file-input" id="inputGroupFile01">
							<label class="custom-file-label" for="inputGroupFile01">Upload your CV</label>
						</div>
					</div>
					<div class="col-md-12"><div class="form-group"><textarea cols="40" rows="6" class="form-control" placeholder="Message"></textarea></div></div>
					<div class="col-md-12 text-center"><button class="btn-block btn btn-dark">Apply now</button></div>
				</div>
			</div>
			<div class="col-md-4 sidebar">
				<div class="sticky-element">
					<h2 class="mt-3 mt-md-0 mb-3">Job details</h2>
					<ul class="list-unstyled p-0">
						<li class="mb-3"><strong>Posted:</strong> <?= date('jS M Y', strtotime($job[0]->create_date_time)); ?></li>
						<li class="mb-3"><strong>Location:</strong> <?= $city; ?> </li>
						<li class="mb-3"><strong>Specialism:</strong> IT </li>
						<li class="mb-3"><strong>Job type:</strong> <?= implode(", ",json_decode($job[0]->type)); ?> </li>
						<!--<li class="mb-3"><strong>Contact:</strong> Jessica Mores</li>
						<li class="mb-3"><strong>Phone:</strong> (+251) 854-6308 </li>-->
					</ul>
				</div>
			</div>
			
		</div>
	</div>
</section>