<!-- =======================
header Start-->
<header class="header-static navbar-sticky navbar-light">

	
	
	<nav class="navbar navbar-expand-lg bg-light">
		<div class="container">
			<a class="navbar-brand py-3" href="<?php echo  base_url()?>">
				<!-- SVG Logo Start -->
				<img src="<?php echo base_url();?>assets/images/logo.png" width="180"/>
				<!-- SVG Logo End -->
			</a>
			<!-- Menu opener button -->
			<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"> </span>
			</button>
			<!-- Main Menu Start -->
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav ml-auto">
					
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('home/explore_programs'); ?>"  id="demosMenu"><b>Explore Programs </b></a>
					</li>
                    <li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('home/events'); ?>" id="demosMenu"><b>Events </b></a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('home/career'); ?>" id="demosMenu"><b>Career Support </b></a>
					</li>
					
                    <li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('home/hiring'); ?>" id="demosMenu"><b>Industry</b></a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="demosMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b>Mentors</b></a>
						<div class="dropdown-menu pb-3 pb-lg-0" aria-labelledby="demosMenu" >
							<div class="d-block d-sm-flex">
								<ul class="list-unstyled w-100 w-sm-50 pr-0 pr-lg-5">
                                    <li> <a class="dropdown-item" href="<?php echo site_url('home/instructor/ilist'); ?>">Mentors and Advisors </a> </li>
									
									<li> <a class="dropdown-item" href="<?php echo site_url('home/instructor/ibenefits'); ?>">Why join here?</a> </li>
									
								</ul>
								
							</div>
						</div>
					</li>

					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="demosMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><b>More </b></a>
						<div class="dropdown-menu pb-3 pb-lg-0" aria-labelledby="demosMenu" >
							<div class="d-block d-sm-flex">
								<ul class="list-unstyled w-100 w-sm-50 pr-0 pr-lg-5">
									<li> <a class="dropdown-item" href="<?php echo site_url('home/blogs'); ?>">Blog</a> </li>
									<li> <a class="dropdown-item" href="<?php echo site_url('home/platform'); ?>">Platform +</a> </li>
									<li> <a class="dropdown-item" href="<?php echo site_url('home/success_stories'); ?>">Success Stories</a> </li>
									<li> <a class="dropdown-item" href="<?php echo site_url('home/gallery'); ?>">Galleria</a> </li>
									<li> <a class="dropdown-item" href="<?php echo site_url('home/associate/business'); ?>">Become a Business Associate </a> </li>
                                    <li> <a class="dropdown-item" href="<?php echo site_url('home/associate/client'); ?>">Become a Client Associate </a> </li>
                                    <li> <a class="dropdown-item" href="<?php echo site_url('home/collaborations'); ?>">Collaborate Now </a> </li>
                                    <li> <a class="dropdown-item" href="<?php echo site_url('home/contact'); ?>">Contact Us </a> </li>
								</ul>
							</div>
						</div>
					</li>
					
				</ul>
			</div>
			 <li class="nav-item">
				<a class="nav-link" href="<?php echo site_url('home/careers'); ?>" id="demosMenu"><b>&nbsp; </b></a>
			</li>
			<div class="navbar-nav">
				<div class="nav-item border-0 d-none d-lg-inline-block align-self-center">
					<a href="https://learn.techmagnox.com/register/student" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Join Now!</a>
				</div>
			</div>
		</div>
	</nav>
</header>
