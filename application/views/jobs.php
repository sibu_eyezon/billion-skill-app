<style>
#loader{
	position: relative;
	top:10%;
	left:45%;
	width: 200px;
	height: 200px;
}
</style>
<?php
	$city = $this->user_model->get_all_cities();
	$skills = $this->user_model->get_all_skills();
	$industry = $this->user_model->get_all_industries();
	$positions = $this->user_model->get_all_designations();
	$educations = $this->user_model->get_all_degrees();
?>
<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4">Jobs</h2>
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section id="about" class="about-area pt-100 pb-70">
    <div class="container">
		<div class="row">
		
			<div class="col-md-8">
				<div id='loader' style='display: none;'>
				  <img src='https://cdn.dribbble.com/users/563824/screenshots/3633228/untitled-5.gif'>
				</div>
				<div id="filterData"></div>
			</div>
			<aside class="col-md-4 sidebar">
				<h2>Refine your search</h2>
				<form id="frmFilter">
					<div class="input-group mb-3">
						<input class="form-control border-radius-right-0 border-right-0" type="text" name="search" id="search" placeholder="Search">
						<span class="input-group-btn">
							<button type="button" class="btn btn-grad border-radius-left-0 mb-0" onClick="getFilteredJobs();"><i class="ti-search m-0"></i></button>
						</span>
					</div>
					<div class="form-group">
						<select class="custom-select select-big" name="location" id="location" onChange="getFilteredJobs();">
							<option value="" selected>Locations</option>
							<?php
								foreach($city as $location){
									echo '<option value="'.$location->id.'">'.trim($location->name).'</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<select class="custom-select select-big" name="type" id="type" onChange="getFilteredJobs();">
							<option value="" selected>Job Type</option>
							<option value="Internship">Internship</option>
							<option value="Full Time">Full Time</option>
							<option value="Part Time">Part Time</option>
						</select>
					</div>
					<div class="form-group">
						<select class="custom-select select-big" name="skills" id="skills" onChange="getFilteredJobs();">
							<option value="" selected>Skills</option>
							<?php
								foreach($skills as $skill){
									echo '<option value="'.$skill->id.'">'.trim($skill->name).'</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<select class="custom-select select-big" name="industry" id="industry" onChange="getFilteredJobs();">
							<option value="" selected">Industries</option>
							<?php
								foreach($industry as $irow){
									echo '<option value="'.$irow->id.'">'.trim($irow->name).'</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<select class="custom-select select-big" name="position" id="position" onChange="getFilteredJobs();">
							<option value="" selected>Designation</option>
							<?php
								foreach($positions as $prow){
									echo '<option value="'.$prow->id.'">'.trim($prow->name).'</option>';
								}
							?>
						</select>
					</div>
					<div class="form-group">
						<select class="custom-select select-big" name="degree" id="degree" onChange="getFilteredJobs();">
							<option value="" selected>Education</option>
							<?php
								foreach($educations as $erow){
									echo '<option value="'.$erow->id.'">'.trim($erow->degree_name).' '.trim($erow->short).'</option>';
								}
							?>
						</select>
					</div>
				</form>
			</aside>
			
		</div>
	</div>
</section>
<script>
	function getFilteredJobs()
	{
		$('#loader').show();
		$('#filterData').html("");
		var frmData = $('#frmFilter').serialize();
		$.ajax({
			url: baseURL+'getFilteredjobs',
			type: 'GET',
			data: frmData,
			contentType: false,
			processData: false,
			success: (res)=>{
				$('#loader').hide();
				$('#filterData').html(res);
			},
			error: (errors)=>{
				$('#loader').hide();
			}
		});
	}
	
	$(window).on('load',function() {
		getFilteredJobs();
	});
</script>