<style>
.card-body{
	height: 200px;
	overflow-y: scroll;
	background: #fff;
	scrollbar-color: black #c3bebe;
	scrollbar-width: thin;
}
.thin-scroll::-webkit-scrollbar-track {
  background: linear-gradient(to right, #666 0%, #666 35%, black 55%, #e3e3e3 61%,#e3e3e3 100%);
}

.thin-scroll::-webkit-scrollbar {
	width: 10px;
}

.thin-scroll::-webkit-scrollbar-thumb {
	background-color: black;
	border-radius: 5px;
}
</style>
<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4">All Skills</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="bg-light">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12" >
	
				<div class="row mt-4">
				
					<?php $skills = $this->user_model->get_all_skills(); foreach($skills as $skow){ ?>
					<div class="col-md-6 mb-4">
						<div class="card">
							<div class="card-header">
								<h4 class="card-title"><?php echo trim($skow->name); ?>
									<img src="https://learn.techmagnox.com/assets/img/skills/<?php echo trim($skow->slogo); ?>" style="width:75px; float:right;"/>
								</h4>
								
							</div>
							<div class="card-body thin-scroll">
								<?php echo trim($skow->desc); ?>
							</div>
						</div>
					</div>
					<?php  } ?>
					
				</div>
	
			</div>
		</div>
	</div>
</section>