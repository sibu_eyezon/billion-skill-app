<!-- =======================
Banner innerpage -->


<section class="testimonilas-area pt-95 pb-70 white-bg">
	<div class="container">

		<div class="row">
			<div class="col-sm-12 text-center mt-5 mb-4">
				<h2 class=" display-4">Why join as a Mentor ?</h2>
				<h5>Know why Industry People love to join us as an Mentor in our Program. </h5>
			</div>
			<div class="col-md-2 mt-2">
				<div class="feature-box f-style-5 h-100 icon-grad">
					<div class="feature-box-icon"><i class="ti-panel"></i></div>
					<h5>Flexible Timing</h5>

				</div>
			</div>
			<div class="col-md-2 mt-2">
				<div class="feature-box f-style-5 h-100 icon-grad">
					<div class="feature-box-icon"><i class="ti-palette"></i></div>
					<h5>Freedom of Curriculum</h5>

				</div>
			</div>
			<div class="col-md-2 mt-2">
				<div class="feature-box f-style-5 h-100 icon-grad">
					<div class="feature-box-icon"><i class="ti-gift"></i></div>
					<h5>Freedom of Category</h5>

				</div>
			</div>
				<div class="col-md-2 mt-2">
				<div class="feature-box f-style-5 h-100 icon-grad">
					<div class="feature-box-icon"><i class="ti-panel"></i></div>
					<h5 >Excellent Earning </h5>

				</div>
			</div>
			<div class="col-md-2 mt-2">
				<div class="feature-box f-style-5 h-100 icon-grad">
					<div class="feature-box-icon"><i class="ti-palette"></i></div>
					<h5 >Excellent LMS</h5>

				</div>
			</div>
			<div class="col-md-2 mt-2">
				<div class="feature-box f-style-5 h-100 icon-grad">
					<div class="feature-box-icon"><i class="ti-gift"></i></div>
					<h5 >World Class Advisors</h5>

				</div>
			</div>

		</div>
	</div>
</section>
<!-- =======================
Banner innerpage -->
<section id="about" class="about-area pt-100 pb-70">
    <div class="container">
		<div class="row">

			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">What you can do here ?</h3>
      <ul class="list-group list-group-borderless list-group-icon-primary-bg">
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Live Classes</b> </li>
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Seminars </b></li>
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Interview Practice</b></li>
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Doubt Clearning</b></li>
					</ul>
					</div>
				</div>
			</div>

			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">How much you can earn ?</h3>
     <ul class="list-group list-group-borderless list-group-icon-primary-bg">
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Mentor Conn : (Rs 22k to Rs 75k /Mon)</b> </li>
						<li class="list-group-item"><i class="fa fa-check"></i> <b>2-4 Hours Per Week you have to give </b></li>
						<li class="list-group-item"><i class="fa fa-check"></i> <b> Certification : Rs 1000 - 5000 Per Hour </b></li>
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Refer & Earn option is also there </b></li>
					</ul>
					</div>
				</div>
			</div>

			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Extra Benefits</h3>
     <ul class="list-group list-group-borderless list-group-icon-primary-bg">
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Create own Program & Curriculum</b> </li>
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Choose any means to upskill  </b></li>
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Freedom of Pricing </b></li>
						<li class="list-group-item"><i class="fa fa-check"></i> <b>Earn Certificate, Goodies</b></li>
					</ul>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="testimonilas-area pt-95 pb-70 white-bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-10 offset-xl-1 col-lg-12 offset-lg-2 col-md-10 offset-md-1">
				<div class="section-title mb-50 text-center">
					<div class="section-title-para">
      	<h4>World Class Advisors and Mentors have joined our Program. <br><br>Will you like to Join our Mission to minimize the Skill Gaps and upskill students to get better jobs.  </h4>
                                                                <br>
                     <a class="btn btn-grad btn-sm pull-left" href="<?php echo site_url('home/explore_programs'); ?>" style="color:#fff">Explore All Programs</a>
<a class="btn btn-grad btn-sm pull-center" href="<?php echo site_url('home/explore_programs'); ?>" style="color:#fff">Explore All Mentors </a>
<a class="btn btn-grad btn-sm pull-right" href="<?php echo site_url('home/explore_programs'); ?>" style="color:#fff">Register Now</a>

                	</div>
				</div>
			</div>
		</div>
	</div>
</section>



<section>
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="title text-center">

					<h2>Advisors and Mentors</h2>
					<p>World Class Advisors and Industry Mentors to strengthen your base, guide you.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="5" data-items-xs="1">
					<?php if(!empty($ilist)){
						foreach($ilist as $irow){
							$name = trim($irow->txt_instructor_name);
							$designation = trim($irow->txt_instructor_desg);
							$photo_sm = trim($irow->txt_profile_pic);
					?>
					<div class="item">
						<div class="team-item">
							<div class="team-avatar">
								<img class="img-responsive" src="<?php echo 'https://learn.techmagnox.com/'.$photo_sm; ?>" onerror="this.src='<?= base_url('assets/images/people/default-avatar.png'); ?>'">
							</div>
							<div class="team-desc">
								<h5 class="team-name"><?= $name; ?></h5>
								<span class="team-position"><?= $designation; ?></span>
								<p class="text-justify"><?= substr(trim($irow->txt_instructor_dtls), 0, 60).'...'; ?></p>
							</div>
						</div>
					</div>
					<?php } } ?>

				</div>
				<a href="<?php echo site_url('home/instructor/ilist'); ?>" class="btn btn-grad btn-sm pull-right">All Instructors</a>
			</div>
		</div>
	</div>
</section>

<section class="team-area pt-95 pb-70 gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<div class="section-title mb-50 text-center">
					<div class="section-title-heading mb-20">
						<h1 class="primary-color">FAQ</h1>      <br>
					</div>

				</div>
			</div>
		</div>

			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="team-wrapper mb-30">
                      <div class="accordion accordion-grad" id="accordion3">
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="h6 mb-0" data-toggle="collapse" href="#collapse-7">How many free samples can i redeem?</a>
							</div>
							<div class="collapse show" id="collapse-7" data-parent="#accordion3">
								<div class="accordion-content">Due to the limited quantity, each member's account is only entitled to 1 unique free sample. You can check out up to 4 free samples in each checkout. We take such matters very seriously and will look into individual cases thoroughly. </div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-8">What are the payment methods available?</a>
							</div>
							<div class="collapse" id="collapse-8" data-parent="#accordion3">
								<div class="accordion-content"> At the moment, we only accept Credit/Debit cards and Paypal payments. Paypal is the easiest way to make payments online. While checking out your order. Be sure to fill in correct details for fast & hassle-free payment processing.</div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-9">Can i pay without a paypal account?</a>
							</div>
							<div class="collapse" id="collapse-9" data-parent="#accordion3">
								<div class="accordion-content"> Yes! It is commonly misunderstood that a Paypal account is needed in order to make payments through Paypal. The truth is you DO NOT need one, although we strongly recommend you sign up to enjoy the added ease of use. </div>
							</div>
						</div>
					</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="team-wrapper mb-30">
      <div class="accordion accordion-grad" id="accordion4">
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="h6 mb-0" data-toggle="collapse" href="#collapse-1">How many free samples can i redeem?</a>
							</div>
							<div class="collapse show" id="collapse-1" data-parent="#accordion4">
								<div class="accordion-content">Due to the limited quantity, each member's account is only entitled to 1 unique free sample. You can check out up to 4 free samples in each checkout. We take such matters very seriously and will look into individual cases thoroughly. </div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-2">What are the payment methods available?</a>
							</div>
							<div class="collapse" id="collapse-2" data-parent="#accordion4">
								<div class="accordion-content"> At the moment, we only accept Credit/Debit cards and Paypal payments. Paypal is the easiest way to make payments online. While checking out your order. Be sure to fill in correct details for fast & hassle-free payment processing.</div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-3">Can i pay without a paypal account?</a>
							</div>
							<div class="collapse" id="collapse-3" data-parent="#accordion4">
								<div class="accordion-content"> Yes! It is commonly misunderstood that a Paypal account is needed in order to make payments through Paypal. The truth is you DO NOT need one, although we strongly recommend you sign up to enjoy the added ease of use. </div>
							</div>
						</div>
					</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
