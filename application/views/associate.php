<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class="display-4"><?= ucfirst($params); ?> Form</h2>
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->

<section>
	<div class="container h-100">
		<div class="row">
			<div class="col-md-<?= ($params=='business')? '12' : '6'; ?>">
				<?php
					if($this->session->flashdata('errors')!=""){
						echo '<div class="alert alert-warning alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
						  '.$this->session->flashdata('errors').'
						</div>';
					}
				?>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title text-center">We are happly to receive your association with us.</h4>
					</div>
					<div class="card-body">
					<form action="#<?php //echo site_url('home/insertAssociation'); ?>" method="POST" style="width: 100%;">
						<div class="row">
							<div class="col-md-<?= ($params=='business')? '6' : '12'; ?>">
								<div class="card">
									<div class="card-header">
										<h4 class="card-title">Personal Details</h4>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Firstname</label>
													<input type="text" name="first_name" id="first_name" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Lastname</label>
													<input type="text" name="last_name" id="last_name" class="form-control"/>
												</div>
											</div>
											<input type="hidden" name="type" id="type" value="<?= ($params=='business')? 'BA' : 'CA'; ?>"/>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Email</label>
													<input type="email" name="email" id="email" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Mobile</label>
													<input type="text" name="phone" id="phone" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Facebook</label>
													<input type="url" name="facebook" id="facebook" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Linkedin</label>
													<input type="url" name="linkedin" id="linkedin" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="">Address</label>
													<input type="text" name="address" id="address" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">City</label>
													<input type="text" name="city" id="city" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Pincode</label>
													<input type="text" name="pincode" id="pincode" class="form-control"/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php if($params=="business"){ ?>
							<div class="col-md-6">
								<div class="card">
									<div class="card-header">
										<h4 class="card-title">Company Details</h4>
									</div>
									<div class="card-body">
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="">Comapny Name</label>
													<input type="text" name="company" id="company" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-12">
												<div class="form-group">
													<label for="">Website</label>
													<input type="url" name="website" id="website" class="form-control"/>
												</div>
											</div>
										</div>
										<!--<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Contact</label>
													<input type="text" name="alt_phone" id="alt_phone" class="form-control"/>
												</div>
											</div>
										</div>-->
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">PAN number</label>
													<input type="text" name="pan_no" id="pan_no" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">GST number</label>
													<input type="text" name="gst_no" id="gst_no" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-12">
												<div class="form-group">
													<label for="">Address</label>
													<input type="text" name="work_address" id="work_address" class="form-control"/>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">City</label>
													<input type="text" name="work_city" id="work_city" class="form-control"/>
												</div>
											</div>
											<div class="col-sm-6">
												<div class="form-group">
													<label for="">Pincode</label>
													<input type="text" name="work_pincode" id="work_pincode" class="form-control"/>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<div class="row mt-5">
							<div class="col-sm-12 justify-content-center text-center">
								<button type="submit" class="btn btn-md btn-success">Submit</button>
							</div>
						</div>
					</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
