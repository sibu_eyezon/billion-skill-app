<style>
.kicon{
	font-size: 1.2rem;
    font-weight: 900;
}
</style>
<section class="p-0 ">
	<div class="container-fluid h-100 p-0">
		<div class="swiper-container height-350-responsive swiper-arrow-hover swiper-slider-fade">
			<div class="swiper-wrapper">
				<!-- slide 1-->
				<div class="swiper-slide bg-overlay-dark-2" style="background-image:url(<?php echo base_url()?>assets/images/bg/05.jpg); background-position: center center; background-size: 100% 100%;">
					<div class="container h-100">
						<div class="row d-flex h-100">
							<div class="col-lg-12 col-xl-12 mr-auto slider-content justify-content-center align-self-center align-items-start text-left">
								<h6 class="animated fadeInUp dealy-500 display-12 display-md-12 display-lg-5 font-weight-bold text-white">Your Search for a Better Career End Here.</h6>
								<h6 class="animated fadeInUp dealy-1000 text-white display-8 display-md-6  mb-2 my-md-4">Enhance your skills, Enrich your career</h6>
								<div class="animated fadeInUp mt-3 dealy-1500"><a href="https://learn.techmagnox.com/" class="btn btn-outline-white">Register Now</a> </div>
							</div>
						</div>
					</div>
				</div>
				<!-- slide 2-->
				<div class="swiper-slide bg-overlay-dark-2" style="background-image:url(<?php echo base_url()?>assets/images/bg/01.jpg); background-position: center top; background-size: 100% 100%;">
					<div class="container h-100">
						<div class="row d-flex h-100">
							<div class="col-md-10 justify-content-center align-self-center align-items-start mr-auto">
								<div class="slider-content text-left ">
									<h6 class="animated fadeInUp dealy-500 display-8 display-md-12 display-lg-5 font-weight-bold text-white ">Learn Anywhere, Anything, Anytime  </h6>
									<h6 class="animated fadeInUp dealy-1000 display-12 display-md-12 display-lg-6 text-white">Associate with Big Brands for better Recognitions.</h6>
									<div class="animated fadeInUp mt-3 dealy-1500"><a href="https://learn.techmagnox.com/" class="btn btn-outline-white">Register Now</a></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- Slider buttons -->
			<div class="swiper-button-next"><i class="ti-angle-right"></i></div>
			<div class="swiper-button-prev"><i class="ti-angle-left"></i></div>
			<div class="swiper-pagination"></div>
		</div>
	</div>
</section>

<!--<section class="client bg-grad px-5">
	<div class="container">
		<div class="row">
			<div class="col-md-2">
				<h2  style="color:#fff">Associations </h2>
				<h3 class="feature-box-title text-white"> 20+ Institutes </h3>
				<a class="mt-3" href="<?php //echo site_url('home/portfolio'); ?>" style="color:#fff">Know more!</a>
			</div>
			
			<div class="col-md-10">
				<div class="owl-carousel owl-grab arrow-hover arrow-dark" data-margin="40" data-arrow="true" data-dots="false" data-items-xl="6" data-items-lg="5" data-items-md="4" data-items-sm="3" data-items-xs="2">
					<div class="item"><img src="<?php //echo base_url();?>assets/images/clients/01-light.png" alt=""></div>
					<div class="item"><img src="<?php ///echo base_url();?>assets/images/clients/02-light.png" alt=""></div>
					<div class="item"><img src="<?php //echo base_url();?>assets/images/clients/03-light.png" alt=""></div>
					<div class="item"><img src="<?php //echo base_url();?>assets/images/clients/04-light.png" alt=""></div>
					<div class="item"><img src="<?php //echo base_url();?>assets/images/clients/5433.png" alt=""></div>
					<div class="item"><img src="<?php //echo base_url();?>assets/images/clients/06-light.png" alt=""></div>
					<div class="item"><img src="<?php //echo base_url();?>assets/images/clients/icfai.png" alt=""></div>
					<div class="item"><img src="<?php //echo base_url();?>assets/images/clients/logo.png" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</section>-->

<section class="mt-n6 bg-light">
	<div class="container position-relative" style="z-index:1;">
		
		<div class="row">
			<div class="col-md-4">
				<div class="feature-box f-style-2 icon-grad" style="background-color:#fbc52f; padding-top:5px; margin-top:5px">
					<h5 class="feature-box-title" style="color:#fff">Free Webinars </h5>
					<!--<p style="line-height:25px; font-size:13.5 px; text-align:justify; color:#fff ">Update Yourself with the Latest Technolgoies for Free </p> -->
					<a class="mt-3" href="<?php echo base_url().'home/explore_programs';?> " style="color:#fff; display:inline"> KNOW MORE <span class="fa fa-angle-right kicon"></span></a>  
				</div>
			</div>
			<div class="col-md-4">
				<div class="feature-box f-style-2 icon-grad" style="background-color:#0aceab; padding-top:5px; margin-top:5px">
					<h4 class="feature-box-title" style="color:#fff">Certification Programs</h4>
					<!--<p style="line-height:25px; color:#000">Best Insitutes of India are using our Alumni Portal to keep in touch of the Alumni</p><a class="mt-3" href="<?php //echo base_url();?>assets/files/alumni.pdf" style="display:inline" target="_blank"> Download Brochure</a> -->
					<a class="mt-3" href="<?php echo base_url().'home/corporate';?>" style="display:inline"> KNOW MORE <span class="fa fa-angle-right kicon"></span></a>
				</div>
			</div>
			<div class="col-md-4">
				<div class="feature-box f-style-2 icon-grad" style="background-color:#FFF; padding-top:5px; margin-top:5px">
					<h4 class="feature-box-title" style="color:#efa915">Jobs and Internships</h4>
					<!--<p style="line-height:25px; color:#000">Best Insitutes of India are using our Alumni Portal to keep in touch of the Alumni</p>
					&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <a class="mt-3" href="<?php //echo base_url();?>assets/files/alumni.pdf" style="display:inline" target="_blank"> Download Brochure</a> -->
					<a class="mt-3" href="<?php echo base_url().'home/corporate';?>" style="display:inline"> KNOW MORE <span class="fa fa-angle-right kicon"></span></a>
				</div>
			</div>
		</div>
		
	</div>
</section>

<section class="blog bg-light">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="title text-left">
					
					<h3>Certification Programs
						<a class="btn btn-grad btn-sm pull-right" href="<?php echo site_url('home/explore_programs'); ?>" style="color:#fff">Explore All</a>
					</h3>
					<p>Learn the Certification Training Programs from the Best Teachers on the most updated subjects.</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel post-style-3 arrow-dark arrow-hover" data-dots="false" data-items-xl="3" data-items-lg="3" data-items-md="2" data-items-sm="2" data-items-xs="1">
					<?php
					if(!empty($programs)){
						$i=1;
						foreach($programs as $prow){
							$prog_id = $prow->id;
							$title = str_replace(" ","_",strtolower(trim($prow->title)));
							$fee = trim($prow->feetype);
					?>
					<div class="item">
						<div class="post">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!"><?php echo trim($prow->category); ?></a></span>
								<div class="post-author"><a href="#!"><a></div>
								<a class="post-title" href="<?php echo base_url('get_program_details/?id='.base64_encode($prog_id)); ?>"><?php echo trim($prow->title); ?></a>
								<p class="mb-0">
								<?php
									  $curdate = strtotime(date('Y-m-d'));
									  $ldt = strtotime(date('Y-m-d',strtotime($prow->aend_date)));
									  $sdt = strtotime(date('Y-m-d',strtotime($prow->astart_date)));
									  $dur = intval(trim($prow->duration));
									  echo 'Duration: '.$dur.' '.trim($prow->dtype).(($dur==1)? '':'s').'	;	Total hrs: '.trim($prow->prog_hrs).' Hrs.
									  <h6 class="text-left">'.(($fee=='Paid')? 'Rs '.$prow->total_fee : $fee).'</h6>
									  <h6 class="text-left">From: '.date('jS M Y',strtotime($prow->start_date)).' To '.date('jS M Y',strtotime($prow->end_date)).'</h6>
									  <h6 class="text-left text-danger">Deadline: '.(($curdate<=$ldt)? date('jS M Y',strtotime($prow->aend_date)) : 'Expired').'</h6>';
									  if($sdt!=19800 || $ldt!=19800){
										if($curdate<$sdt){
											echo '<h5 class="text-center text-primary">Registration will start on '.date('jS M Y',strtotime($prow->astart_date)).'</h5>';
										}
										
									}
								?>
								</p>
							</div>
						</div>
					</div>
					<?php 
						$i++; }
						}
					?>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog bg-white">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12 mx-auto">
				<div class="title text-left">
					<h3>Enhance your Skills and Knowledge 
					<a class="btn btn-grad btn-sm pull-right" href="<?php echo site_url('home/pg_programs'); ?>" style="color:#fff">Explore All</a>
					</h3>
					<p>Enhance your skills through the Post Graduate Programs, Workshops and more..</p>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel arrow-dark arrow-hover" data-dots="false" data-items-xl="3" data-items-lg="3" data-items-md="2" data-items-sm="2" data-items-xs="1">
					<div class="item">
							<div class="post">
								<img src="<?php echo base_url();?>assets/images/program/sparc.jpg" alt="">
								<div class="post-info">
									<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Workshop</a></span>
									
									<a class="post-title" href="#!">SPARC online Workshop</a>
									<p class="mb-0">Indo US SPARC online Workshop 
on
APPLICATIONS OF ELECROTHERAPY 
IN HEALTHCARE </p><br />
<a href="https://sparcaeh2021.in/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Website</a> 
<a href="https://learn.techmagnox.com/purdueRegister" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Enroll</a>
<a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
								</div>
							</div>
						</div>
                        <div class="item">
							<div class="post">
								<img src="<?php echo base_url();?>assets/images/program/indogfoe.jpg" alt="">
								<div class="post-info">
									<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Symposium</a></span>
									
									<a class="post-title" href="#!">INDOGFOE 2020</a>
									<p class="mb-0">11th Indo German Frontiers of Engineering Symposium organized by IIT Kharagpur & the Alexander von Humboldt Foundation </p><br />
                                    <a href="https://indogfoe2020.in/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Website</a> 
<a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="post">
								<img src="<?php echo base_url();?>assets/images/program/adms.jpg" alt="">
								<div class="post-info">
									<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Program</a></span>
									
									<a class="post-title" href="#!">MMST 2020 @ IIT Kharagpur</a>
									<p class="mb-0">Interested in Research in Medical Science and Technology Join MMST @ SMST, IIT Kharagpur </p>
                                    <br /><a href="http://gate.iitkgp.ac.in/mmst/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">GATE Website</a> <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
								</div>
							</div>
						</div>
						<div class="item">
							<div class="post">
								<img src="<?php echo base_url();?>assets/images/program/sathi.jpg" alt="">
								<div class="post-info">
									<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Training</a></span>
									
									<a class="post-title" href="#!">SATHI DST, Govt of India</a>
									<p class="mb-0">Avail the Facilities of SATHI, DST & IIT Kharagpur</p>
                                    <br />
                                    <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a> <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
								</div>
							</div>
						</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="blog bg-light">
	<div class="container">
		<div class="row">
			<!-- info -->
			<div class="col-md-6 col-lg-5 align-self-center">
				<h6 class="bg-grad p-2 border-radius-3 d-inline-block text-white">Explore Offers Now !!</h6>
				<h2  style="line-height:38px" >Corporate Training <br>& Certification Program with Live Projects</h2>
				<p>We have the Best Instructors which consists of Experienced Faculty Members and Alumni from the best Institutes of the World.<br>
				<a href="<?php echo site_url('home/instructor/ilist'); ?>" class="btn btn-grad btn-sm">All Instructors</a>
				</p>
			</div>
			<!-- image -->
			<div class="col-md-6 col-lg-7 bg-light" >
	
				<div class="row mt-4 text-center">
					<?php
						foreach($skills as $skow){
							echo '<div class="col-lg-3 col-md-4 col-sm-6 col-xs-6 mt-30">
									<div class="feature-box bg-white shadow-hover border-radius-3 f-style-5 h-100 icon-grad">
										<div><img src="https://learn.techmagnox.com/assets/img/skills/'.trim($skow->slogo).'" height="75px" /></div>
									</div>
								</div>';
						}
					?>
				</div>
				<div class="row">
					<div class="col-sm-12 mt-2 mt-md-4">
						<div class="text-center">
							<a class="btn btn-grad mb-0 mr-3" href="<?php echo site_url('home/skills'); ?>">See All Skills</a>
							
						</div>
					</div>
				</div>
	
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-sm-6 col-md-6">
				<h4 class="team-name text-center">Alumni Speak</h4>
				<div class="col-md-12 testimonials testimonials-border py-6">
					<div class="owl-carousel testi-full owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="1" data-items-md="1" data-items-xs="1">
						<?php
							foreach($alumnispk as $fbow){
								echo '<div class="item">
									<div class="testimonials-wrap">
										<div class="testi-text">
											<div>'.trim($fbow->txt_message).'</div>
											<div class="testi-avatar"> <img src="https://learn.techmagnox.com'.$fbow->txt_profile_pic.'" onerror="this.src=`'.base_url().'assets/images/thumbnails/default-avatar.png`" alt="avatar"> </div>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_name).'</h6>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_organization).'</h6>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_position).'</h6>
										</div>
									</div>
								</div>';
							}
						?>
					</div>
				</div>
				<a class="btn btn-grad mb-0 ml-3 btn-sm" href="<?php echo site_url('home/view_feedback'); ?>">View All Feedback</a>
			</div>
			<div class="col-sm-6 col-md-6">
				<h4 class="team-name text-center">Advisors and Instructors</h4>
				<div class="row">
					<div class="col-12">
						<div class="owl-carousel owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="2" data-items-xs="1">
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/nrm.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Prof N R Mandal</h4>
										<span class="team-position">Advisor & Instructor</span>
										<p class="text-justify">Ex Professor & Dean of Students Affairs of IIT Kharagpur. He is the Fellow of Royal Institution of Naval Architects, Institution of Engineers. His specialized fields are Energy, Engineering Design, Alumninum Wealding and more.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/raj.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Dr. Rajarshee Ray</h4>
										<span class="team-position">Advisor & Instructor</span>
										<p class="text-justify">Founder and CTO of SoS Corp. joins our Board. He has done his PhD from The University of Texas & Health Mgmt Course from Harvard University. He was the Chief R&D Architect in Sun Microsystems, Senior Engineer at Intel Corp., USA etc. He will be the key player to introduce AI & Deep Learning.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/guha.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Dr. Debasish Guha</h4>
										<span class="team-position">Mentor</span>
										<p class="text-justify">Debasish Guha has completed his ME and PhD from IIT Kharagpur. He has more than 25 years experience in Telecommunication.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/gautam.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Gautam Mukherjee</h4>
										<span class="team-position">Senior Advisor and Trainer</span>
										<p class="text-justify">Gautam Mukherjee has 36 years of experience in Telecom/IT. He was in Wipro Technologies as OSS-BSS Head, Tech Mahindra as Head of a complete IDU for British Telecom and CIO, in Bharti Airtel.  He has done  B. Tech (Hons.) and ii) M.Tech from IIT Chennai. He has worked in other companies like  Capgemini,  France Telecom (Orange), SFR (Vodafone), Telstra, British Telecom.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="https://in.linkedin.com/in/gautam-mukherjee "><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/arindam.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Dr. Arindam Dasgupta</h4>
										<span class="team-position">Advisor & Instructor</span>
										<p class="text-justify">I am passionate about learning new things, especially programming skills. I have done my MS and PhD from IIT Kharagpur. I have 18 years experience in this computer Science field.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/bikash.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Bikash Maiti</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">Bikash Maiti is having 16 Years of Experience in TCS and is currently the delivery head of TCS. He is a trainer of Ajile Technologies and has spend 5 Years in Australia and worked in different fields like Telecommunication, Healthcare, etc. </p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/tanmoy.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Tanmoy Kanungo</h4>
										<span class="team-position">Trainer</span>
										<p class="text-justify">Tanmoy Kanungo currently the senior consultant in Ericsson India Global Services Pvt. Ltd. is passionate about learning new things, especially programming skills. He has done B. Tech from College of Engineering and Management, Kolaghat and has around 14 years of experience in this software development field, especially in Telecom.<!--His specialization is in Data Engineering with Google Cloud and in Data Migration. He is involved in multiple transformation projects across the globe with some major Telecom operators and has worked with Indian Telecom Operators like Airtel, BSNL. Presently he working as Solution Architect for Enterprise Application.--></p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/fariuddin.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Fariduddin Masud</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">By profession, Fariduddin is a Digital Marketing Strategist Growth Hacker, Marketing, Content, and Product Manager having 7 years of experience. He has worked in a core team and leading to build India's one of the Largest Open-Source (MOOCs) EdTech Platform at IIT Bombay under Minister of Education, Govt. of India (funded) and scaled up a 6.7 million active user base. <!--He has also worked on some prestigious open-source Projects including Low-Cost Device at IIT Bombay. His research and areas of Expertise lines on MOOCs Technology, UX, UI, Digital Strategy, Customer Psychology, and Marketing.--></p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/sougam.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Sougam Maiti</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">Sougam has around  14 years of experience. He has done Research Project in IIT Kharagpur on NLP ( on Semi-Supervised Learning) and was associated with STEP IIT Kharagpur. He has worked in Projects like Virtual Laboratory (MHRD), DIC (MHRD), and SATHI (DST).</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/subhajit.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Subhajit Biswas</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">Subhajit has 13 years of experience in TCS and has been working in the Backend and the Database section. He is a master in Stored Procedures and Databases. Subhajit is the Core Technical Guy and currently the Technical Head of Magnox. He has extremely skilled in Oracle, .NET, and Node Js.  He is also experienced in giving Corporate Training when he was in TCS.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/shamit.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Shamit Patra</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">Shamit has completed his BTech and MS from IIT Kharagpur from the Department of Electrical Engineering. After being in Industry for some time, he has opened a Consultancy Farm named Electrosoft Consultant and has 3 Patents and 2 Copyright under his name.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/soumen.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Dr. Soumen Palit</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">Dr. Soumen Palit has completed his MS in Applied Botany from Agricultural and Food Engineering Department, IIT, Kharagpur and Doctorate (CSIR Fellow) as well as Post Doc in Agricultural Biotechnology from Agricultural and Food Engineering Department, IIT, Kharagpur. His area of research includes Agricultural Biotechnology, Tea Technology, Sustainable and Organic farming, Soil less cultivation etc. He spent more than 16 yrs of research experience.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/arnab.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Arnab Nath</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">Arnab Nath has Completed integrated MSc from IIT Kharagpur in the Dept of Mathematics. Arnab Nath has 15 years experience in the IT field. He has worked in Wipro, Deloittee. He is presently working as Senior Manager in Deloitte.He has around 15 years of experience in Software Architecture, Cloud Architecture, Database, .net and MSSQL</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/jahir.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Md. Jahir Pervez Tarafder</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">Jahir has more than 3 years of experience and is currently serving as the Senior Software Developer in Magnox Technologies Pvt. Ltd. He has worked in Node Js, PHP MVC, and HMVC. He is good in the backend as well as the front end.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/hazra.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Abhinaba Hazra</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">Abhinaba Hazra has 3+ years of experience in IoT and Robotics. He was the training instructor in SAK Robotics, and served as a full-stack in Digital Avenues, LB Roy Infotech earlier.  He has given Training in many programs and is very popular among the students.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>

							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/sardar.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Suvayan Sardar</h4>
										<span class="team-position">Instructor</span>
										<p class="text-justify">Suvayan has 2+ Years experience in PHP MVC and Codeignetor.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/shivam.jpg" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Shivam Mukherjee</h4>
										<span class="team-position">Game Developer</span>
										<p class="text-justify">He's passionate about games and technologies for making games. He's used Unity for game development, XR, and machine learning. He knows his way around Unreal Engine and is currently using it for a game project (to be announced!). He also contributes to open source projects occasionally, having done so for Godot Engine, a free and open source game engine for cross platform 2D and 3D games, among others - a website that teaches about shaders, a document listing supported languages in Godot Engine, and some plugins maintained by individuals from the Godot Community.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							<div class="item">
								<div class="team-item">
									<div class="team-avatar">
										<img class="img-responsive" src="<?php echo base_url();?>assets/images/people/default-avatar.png" alt="" style="height:240px !important;">
									</div>
									<div class="team-desc">
										<h4 class="team-name">Mr. Aadhitya Ravichandran</h4>
										<span class="team-position">Game Developer</span>
										<p class="text-justify">Tinkering with computer hardware since a very young age, he chose his path where technology & entertainment merged - video games. With a Bachelor in Design degree from IIT Guwahati (2020), specialising in Games, Animation, Film & Media studies, he has experience with a myriad of 3D software, Unity & Unreal Engine. Was a Creature Artist on the world's biggest game mod project- Beyond Skyrim Currently works independently with clients' work ranging from Game Design, 3D Art, Graphic Design & Logos for branding. Keen on learning & passing on what was learnt.</p>
										<ul class="social-icons light si-colored-bg-on-hover no-pb">
											<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
											<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
							
						</div>
						<a href="<?php echo site_url('home/instructor/ilist'); ?>" class="btn btn-grad btn-sm pull-right">All Instructors</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="client bg-light pt-6">
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12 mx-auto">
				<div class="title text-left">
					<h3>Awards and Achievements </h3>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="owl-carousel owl-grab arrow-hover arrow-gray" data-margin="40" data-arrow="true" data-dots="false" data-items-xl="5" data-items-lg="3" data-items-md="3" data-items-sm="3" data-items-xs="2">
					<div class="item"><img src="<?php echo base_url();?>assets/images/awards/iew.png" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/awards/europe.jpg" alt=""></div>
					<div class="item"><img src="<?php echo base_url();?>assets/images/awards/apacI.png" alt=""></div>
				</div>
			</div>
		</div>
	</div>
</section>