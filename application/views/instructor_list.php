<?php
if(!empty($ilist)){
	$i=1;
	foreach($ilist as $irow){
		$name = trim($irow->txt_instructor_name);
		$designation = trim($irow->txt_instructor_desg);
		$photo_sm = trim($irow->txt_profile_pic);
?>
<div class="col-sm-3 mb-2">
	<div class="item shadow">
		<div class="team-item">
			<div class="team-avatar text-center">
				<img class="img-responsive" src="<?php echo 'https://learn.techmagnox.com/'.$photo_sm; ?>" onerror="this.src='<?= base_url('assets/images/people/default-avatar.png'); ?>'" alt="" style="height:240px !important;" />
			</div>
			<div class="team-desc">
				<h4 class="team-name"><?= $name; ?></h4>
				<span class="team-position"><?= $designation; ?></span>
				<div class="text-justify" style="height: 25vh; overflow-y:auto;"><?= trim($irow->txt_instructor_dtls); ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php 
	$i++; }
	}else{
		echo '<div class="col-sm-12"><h4 class="title text-center font-weight-bold">No Instructor Found.</h4></div>';
	}
?>