<footer class="footer-dark pt-6 position-relative">
	<div class="footer-content">
		<div class="container">
			<div class="row">
				<div class="col-md-3 col-sm-3">
					<div class="widget">
						<a href="index-2.html" class="footer-logo">
							<img src="<?php echo base_url();?>assets/images/magnox.png"  height="100px" style="height:70px" />
							<!-- footer SVG logo End -->
						</a>
						<p><b>Magnox Technologies Pvt. Ltd.  </b></p>
						
						<ul class="list-unstyled">
							<!--<li class="media mb-3"><i class="mr-3 display-8 ti-map-alt"></i>7th Floor, Nasscom Warehouse, Mani Bhandar (7th Floor), Webel Bhaban, Sector V, Saltlake<br /> Kolkata - 91 </li>-->
							<li class="media mb-3"><i class="mr-3 display-8 ti-headphone-alt"></i> (91) 9932242598 </li>
							<li class="media mb-3"><i class="mr-3 display-8 ti-headphone-alt"></i> (91) 6295622155 </li>
                            <li class="media mb-3"><i class="mr-3 display-8 ti-headphone-alt"></i> (91) 9679372559 </li>
							<li class="media mb-3"><i class="mr-3 display-8 ti-email"></i> sougam@techmagnox.com</li>
							
						</ul>
						<ul class="social-icons si-colored-bg light">
							<li class="social-icons-item social-facebook"><a class="social-icons-link" href="https://www.facebook.com/magnox/" target="_blank"><i class="fa fa-facebook"></i></a></li>
							<li class="social-icons-item social-instagram"><a class="social-icons-link" href="https://www.linkedin.com/company/magnox-technologies-private-limited" target="_blank"><i class="fa fa-linkedin"></i></a></li>
							<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="social-icons-item social-youtube"><a class="social-icons-link" href="#"><i class="fa fa-youtube"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-2 col-sm-2">
					<div class="widget">
						<h6><b>Company</b></h6>
						<ul class="nav flex-column primary-hover">
							<li class="nav-item"><a class="nav-link" href="https://techmagnox.com/Magnox/About" target="_magnox">About Us</a></li>
							<li class="nav-item"><a class="nav-link" href="https://techmagnox.com/Magnox/People" target="_magnox">People</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/success_stories'); ?>">Success Stories</a></li>
							<li class="nav-item"><a class="nav-link" href="https://techmagnox.com/Magnox/Investors" target="_magnox">Investors</a></li>
							<li class="nav-item"><a class="nav-link" href="https://techmagnox.com/Magnox/Portfolio" target="_magnox">Portfolio</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/collaborations'); ?>">Collaboration</a></li>
							<li class="nav-item"><a class="nav-link" href="https://techmagnox.com/Magnox/Career" target="_magnox">Career</a></li>
							<li class="nav-item"><a class="nav-link" href="https://techmagnox.com/Magnox/Contact" target="_magnox">Contact Us</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/blogs'); ?>">Blogs</a></li>
						</ul>
					</div>
				</div>
				<div class="col-md-2 col-sm-2">
					<div class="widget">
						<h6><b>Lets Handshake</b></h6>
						<ul class="nav flex-column primary-hover">
							<li class="nav-item"><a class="nav-link" href="https://learn.techmagnox.com/register/teacher" target="_blank">Become an Instructor</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/partner'); ?>">Become a Partner</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/corporate'); ?>">Corporate Training</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/career'); ?>">Career</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/consultancy'); ?>">Consultancy</a></li>
                            <li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/consultancy'); ?>">Consultancy</a></li>
						</ul>
					</div>
				</div>
				<!-- Footer widget 2 -->
				<div class="col-md-2 col-sm-2">
					<div class="widget">
						<h6><b>Categories (Gen)</b></h6>
						<ul class="nav flex-column primary-hover">
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/engineering_drawing'); ?>">Engineering Drawing</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/english_communication'); ?>">Engish Communication</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/english_grammer'); ?>">English Grammer</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/banking_finance'); ?>">Banking and Finance</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/logical_reasoning_aptitude'); ?>">Logical Reasoning & Aptitude</a></li>
						</ul>
					</div>
				</div>
				
				<!-- Footer widget 3 -->
				<div class="col-md-3 col-sm-3">
					<div class="widget">
						<h6><b>Categories (IT & Es)</b></h6>
						<ul class="nav flex-column primary-hover">
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/data_science'); ?>">Data Science</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/machine_learning'); ?>">Machine Learning</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/web_app_php_mvc'); ?>">Web App with PHP MVC</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/node_react'); ?>">Node Js and React Js</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/iot'); ?>">Internet of Things</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/robotics'); ?>">Robotics</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/advance_database'); ?>">Advance Database</a></li>
							<li class="nav-item"><a class="nav-link" href="<?php echo site_url('home/category/frontend_angular_react'); ?>">Front End with Angular and React</a></li>
						</ul>
					</div>
				</div>
				<!-- Footer widget 4 -->
				
			</div>
		</div>
	</div>
	<div class="divider mt-3"></div>
	<div class="footer-copyright py-3">
		<div class="container">
			<div class="d-md-flex justify-content-between align-items-center py-3 text-center text-md-left">
				<!-- copyright text -->
				<div class="copyright-text">© 2021 All Rights Reserved by <a href="https://techmagnox.com/" target="_blank"> Magnox Technologies Pvt. Ltd.</a></div>
				<!-- copyright links-->
				<div class="copyright-links primary-hover mt-3 mt-md-0">
					<ul class="list-inline">
						<li class="list-inline-item pl-2"><a class="list-group-item-action text-white" href="<?php echo site_url('home/refund_policy'); ?>">Refund Policy</a></li>
						<li class="list-inline-item pl-2"><a class="list-group-item-action text-white" href="<?php echo site_url('home/privacy'); ?>">Privacy Policy</a></li>
						<li class="list-inline-item pl-2"><a class="list-group-item-action pr-0 text-white" href="<?php echo site_url('home/terms_conditions'); ?>">Use of Terms</a></li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</footer>
<div> <a href="#" class="back-top btn btn-grad"><i class="ti-angle-up"></i></a> </div>

	<!--Global JS-->
	<script src="assets/vendor/jquery/jquery.min.js"></script>
	<script src="assets/vendor/popper.js/umd/popper.min.js"></script>
	<script src="assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
	<script src="assets/vendor/jquery-easing/jquery.easing.min.js"></script>

	<!--Vendors-->
	<script src="assets/vendor/owlcarousel/js/owl.carousel.min.js"></script>
	<script src="assets/vendor/fitvids/jquery.fitvids.js"></script>
	<script src="assets/vendor/isotope/isotope.pkgd.min.js"></script>
	<script src="assets/vendor/imagesloaded/imagesloaded.pkgd.min.js"></script>

	<!--Template Functions-->
	<script src="assets/js/functions.js"></script>

</body></html>
<!-- =======================
footer  -->
