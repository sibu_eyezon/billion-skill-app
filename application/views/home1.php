
<main class="main">
	<div class="intro-slider-container">
		<div class="intro-slide" style="background-image: url(assets/images/banners/slide-1.jpg);">
			<div class="row justify-content-center">
				<div class="col-md-8">
					<div class="intro-content text-left">
		                <h2 class="intro-title">Find Your Future at Narendra Gurukul.</h2>
		                <h3 class="intro-subtitle">15% Discount on all courses</h3>
		                <a href="category.html" class="btn btn-primary">
		                    <span>Explore</span>
		                    <i class="icon-long-arrow-right"></i>
		                </a>
		            </div>
		            <div class="card search-box cta mt-3">
		            	<div class="card-header">
		            		<h4 class="card-title">Search By</h4>
		            	</div>
		            	<div class="card-body">
		            		<form action="#" id="frmSearch" method="POST">
		            			<div class="form-group">
		            				<div class="button-group-pills text-left" data-toggle="buttons">
								        <label class="btn btn-default active">
								          <input type="radio" name="options" id="colleges" value="colleges" checked="true" style="display: none;">
								          <div>Colleges</div>
								        </label>
								        <label class="btn btn-default">
								          <input type="radio" name="options" id="tutors" value="tutors" style="display: none;">
								          <div>Tutors</div>
								        </label>
								      </div>
		            			</div>
	                    		<div class="input-group">
									<input type="text" name="search" id="search" class="form-control" placeholder="&#xF002; Search by Name" aria-label="Search" style="font-family:'Poppins', FontAwesome">
									<div class="input-group-append">
										<button class="btn btn-primary btn-rounded" type="submit"><span>Search</span></button>
									</div>
								</div>
	                    	</form>
		            	</div>
		            </div>
				</div>
			</div>
            
        </div>
    </div>
    <section class="bg-light pt-6">
    	<div class="container categoriest">
	    	<div class="heading mb-3">
		    	<ul class="nav nav-pills menu-nav" role="tablist">
	                <li class="nav-item">
	                    <a class="nav-link active" id="top-college-link" data-toggle="tab" href="#top-college-tab" role="tab" aria-controls="top-college-tab" aria-selected="false">Colleges</a>
	                </li>
	                <li class="nav-item">
	                    <a class="nav-link" id="top-tutor-link" data-toggle="tab" href="#top-tutor-tab" role="tab" aria-controls="top-tutor-tab" aria-selected="false">Tutors</a>
	                </li>
	            </ul>
	    	</div>
	    	<div class="tab-content">
	    		<div class="tab-pane p-0 fade show active" id="top-college-tab" role="tabpanel" aria-labelledby="top-college-link">
	    			<?php $hcolleges = $this->crudm->get_all_colleges(); ?>
	    			<h2 class="title-lg text-left">Top <?= count($hcolleges); ?> Colleges</h2>
		    		<h4 class="title-sm text-left">The college with excellent values and top facilities</h4>
	    			<div class="owl-carousel owl-simple carousel-equal-height carousel-with-shadow" data-toggle="owl" 
	                    data-owl-options='{
	                        "nav": false, 
	                        "dots": true,
	                        "margin": 20,
	                        "loop": false,
	                        "responsive": {
	                            "0": {
	                                "items":2
	                            },
	                            "480": {
	                                "items":2
	                            },
	                            "992": {
	                                "items":3
	                            },
	                            "1200": {
	                                "items":4
	                            }
	                        }
	                    }'>
	                    <?php 
	                    
	                    if(!empty($hcolleges)){
	                    	foreach($hcolleges as $hcow){
								if(trim($hcow->profile_pic)!=""){
									$remoteFile = $remote_url.trim($hcow->profile_pic);
									$handle = @fopen($remoteFile, 'r');
									$bg_icon = ($handle)? '<img src="'.$remoteFile.'" alt="NGIIE"/>' : '<img src="" alt="NGIIE"/>';
								}else{
									$bg_icon = '<img src="" alt="NGIIE"/>';
								}
								if(trim($hcow->college_banner_link)!=""){
									$remoteFile = $remote_url.trim($hcow->college_banner_link);
									$handle = @fopen($remoteFile, 'r');
									$bg_banner = ($handle)? $remoteFile : site_url('assets/images/default_cover.jpg');
								}else{
									$bg_banner = site_url('assets/images/default_cover.jpg');
								}
	                    		
	                    		$ctitle = trim($hcow->college_name);
	                    		echo '<div class="product">
			        				<figure class="product-media">
			        					<span class="product-label">Admission Open</span>
			        					<span class="product-icon">'.$bg_icon.'</span>
			        					<a href="'.site_url('home/college/'.$hcow->id).'">
			        						<img src="'.$bg_banner.'" alt="'.$ctitle.'" class="product-image" onerror="this.src=`'.site_url('assets/images/default_cover.jpg').'`">
			        					</a>
			        				</figure>
			        				<div class="product-body">
			        					<h3 class="product-title"><a href="'.site_url('home/college/'.$hcow->id).'">'.$ctitle.'</a></h3>
			        					<div class="ratings-container">
			        						<div class="ratings ratings-primary">
			        							<div class="ratings-val" style="width: 40%;"></div>
			        						</div>
			        						<span class="ratings-text">( 4 K )</span>
			        					</div>
			        					<div class="product-cat">
			        						
			        					</div>
			        					
			        				</div>
			        				<div class="action-footer px-3"><a href="'.site_url('home/college/'.$hcow->id).'" class="btn btn-outline-info btn-sm">View Details</a></div>
			        			</div>';
	                    	}
	                    }
	                    ?>

	                </div>
	    		</div>
	    		<div class="tab-pane p-0 fade" id="top-tutor-tab" role="tabpanel" aria-labelledby="top-tutor-link">
	    			<h2 class="title-lg text-left">Top Tutors</h2>
		    		<h4 class="title-sm text-left text-warning">Coming soon ...</h4>
	    		</div>
	    		
	    	</div>
	    </div>
    </section>

    <section class="pt-6 pb-6 bg-white">
    	<div class="container">
	    	<div class="heading mb-6">
		    	<h2 class="title-lg text-left">Explore Collegs, Tutors, and more</h2>
		    	<h4 class="title-sm text-left">The platform with excellent values and top facilities</h4>
	    	</div>
	    	<div class="row justify-content-center">
                <!--<div class="col-xl-5col col-lg-4 col-md-3 col-6">
                    <a href="#" class="element-type">
                        <div class="element">
                            <i class="material-icons" style="font-size: 6rem !important;">house</i>
                            <p>Schools</p>
                        </div>
                    </a>
                </div>-->
                <div class="col-xl-5col col-lg-4 col-md-3 col-6">
                	<a href="#" class="element-type">
                        <div class="element">
                            <i class="material-icons" style="font-size: 6rem !important;">school</i>
                            <p>College</p>
                        </div>
                    </a>
                </div>
                <div class="col-xl-5col col-lg-4 col-md-3 col-6">
                    <a href="#" class="element-type">
                        <div class="element">
                            <i class="material-icons" style="font-size: 6rem !important;">cast_for_education</i>
                            <p>Tutor</p>
                        </div>
                    </a>
                </div>
            </div>
	    </div>
    </section>
    
</main>
<script>
	$('#frmSearch').on('submit', (e)=>{
		e.preventDefault();
		var options = $("input[name='options']:checked").val();
		var searches = $('#search').val().trim();
		window.open(baseURI+'home/search/?opt='+options+'&search='+searches, '_self');
	});
</script>