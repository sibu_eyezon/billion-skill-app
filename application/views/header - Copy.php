<!-- =======================
header Start-->
<header class="header-static navbar-sticky navbar-light">

	<!-- Navbar top start-->
	<div class="navbar-top d-none d-lg-block bg-dark ">
		<div class="container">
			<div class="d-flex justify-content-between align-items-center">
				
				<div class="d-flex align-items-center">
					<ul class="nav list-unstyled ml-3">
						<li class="nav-item mr-3">
							<a class="navbar-link" href="<?= base_url('home/contact'); ?>" style="color:#FFF">Contact Us</a>
						</li>
						<!--<li class="nav-item mr-3">
							<a class="navbar-link" href="tel:9932242598" style="color:#FFF"><strong>Phone:</strong> +91 9932242598</a>
						</li>
						<li class="nav-item mr-3">
							<a class="navbar-link" href="javascript:;" style="color:#FFF">Received Indian Excellance Award 2020</a>
						</li>-->
					</ul>
				</div>
				<div class="d-flex align-items-center">
					<div class="dropdown">
						<a class="dropdown-toggle" href="#" role="button"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:#FFF">Top Topics </a>
						<div class="dropdown-menu mt-2 shadow" aria-labelledby="dropdownAccount">
							<a class="dropdown-item" href="<?php echo site_url('home/category/data_science'); ?>">Data Science</a>
							<a class="dropdown-item" href="<?php echo site_url('home/category/machine_learning'); ?>">Machine Learning</a>
							<a class="dropdown-item" href="<?php echo site_url('home/category/web_app_php_mvc'); ?>">Web App with PHP MVC</a>
							<a class="dropdown-item" href="<?php echo site_url('home/category/node_react'); ?>">Node Js and React Js</a>
							<a class="dropdown-item" href="<?php echo site_url('home/category/iot'); ?>">Internet of Things</a>
							<a class="dropdown-item" href="<?php echo site_url('home/category/robotics'); ?>">Robotics</a>
							<a class="dropdown-item" href="<?php echo site_url('home/category/advance_database'); ?>">Advance Database</a>
							<a class="dropdown-item" href="<?php echo site_url('home/category/engineering_drawing'); ?>">Engineering Drawing</a>
							<a class="dropdown-item" href="<?php echo site_url('home/category/frontend_angular_react'); ?>">Front End with Angular and React</a>
						</div>
					</div>
					<ul class="nav"  style="color:#FFF">
					  <li class="nav-item" >
						  <a class="nav-link" href="<?php echo site_url('home/corporate'); ?>"  style="color:#FFF">Corporate Training</a>
					  </li>
					  <li class="nav-item" >
						  <a class="nav-link" href="<?php echo site_url('home/career'); ?>"  style="color:#FFF">Career Support</a>
					  </li>
					  <li class="nav-item" >
						  <a class="nav-link" href="<?php echo site_url('home/consultancy'); ?>"  style="color:#FFF">Consultancy</a>
					  </li>
					  <li class="nav-item" >
						  <a class="nav-link" href="<?php echo site_url('home/higher_education'); ?>"  style="color:#FFF">Higher Education</a>
					  </li>
					</ul>
					<ul class="social-icons">
						<li class="social-icons-item social-facebook m-0">
							<a class="social-icons-link w-auto px-2" href="#"><i class="fa fa-facebook"></i></a>
						</li>
						<li class="social-icons-item social-instagram m-0">
							<a class="social-icons-link w-auto px-2" href="#"><i class="fa fa-instagram"></i></a>
						</li>
						<li class="social-icons-item social-twitter m-0">
							<a class="social-icons-link w-auto pl-2" href="#"><i class="fa fa-twitter"></i></a>
						</li>
					</ul>
				</div>
				
			</div>
		</div>
	</div>
	
	<nav class="navbar navbar-expand-lg bg-white">
		<div class="container">
			<a class="navbar-brand py-3" href="<?php echo  base_url()?>">
				<!-- SVG Logo Start -->
				<img src="<?php echo base_url();?>assets/images/logo.png" width="150"/>
				<!-- SVG Logo End -->
			</a>
			<!-- Menu opener button -->
			<button class="navbar-toggler ml-auto" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
			  <span class="navbar-toggler-icon"> </span>
			</button>
			<!-- Main Menu Start -->
			<div class="collapse navbar-collapse" id="navbarCollapse">
				<ul class="navbar-nav ml-auto">
					
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('home/explore_programs'); ?>" id="demosMenu">Explore Programs </a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="<?php echo site_url('home/career'); ?>" id="demosMenu">Career Support </a>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="demosMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Instructors</a>
						<div class="dropdown-menu pb-3 pb-lg-0" aria-labelledby="demosMenu" >
							<div class="d-block d-sm-flex">
								<ul class="list-unstyled w-100 w-sm-50 pr-0 pr-lg-5">
									<li> <a class="dropdown-item" href="https://learn.techmagnox.com/register/teacher" target="_blank">Become an Instructor</a> </li>
									<li> <a class="dropdown-item" href="<?php echo site_url('home/instructor/ibenefits'); ?>">Benefits</a> </li>
									<li> <a class="dropdown-item" href="<?php echo site_url('home/instructor/ilist'); ?>">Instructors List</a> </li>
								</ul>
								
							</div>
						</div>
					</li>
					<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="demosMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Magnox LMS</a>
						<div class="dropdown-menu pb-3 pb-lg-0" aria-labelledby="demosMenu" >
							<div class="d-block d-sm-flex">
								<ul class="list-unstyled w-100 w-sm-50 pr-0 pr-lg-5">
									<li> <a class="dropdown-item" href="<?php echo site_url('home/lms/benefits'); ?>">Benefits</a> </li>
									<li> <a class="dropdown-item" href="<?php echo site_url('home/lms/features'); ?>">Features</a> </li>
									<li> <a class="dropdown-item" href="<?php echo site_url('home/lms/cloud'); ?>">Cloud LMS</a> </li>
									<li> <a class="dropdown-item" href="<?php echo site_url('home/lms/pricing'); ?>">Pricing </a> </li>
								</ul>
							</div>
						</div>
					</li>
					<!--<li class="nav-item">
						<a class="nav-link" href="<?php //echo site_url('home/contact'); ?>" target="_magnox" id="demosMenu">Contact Us</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="https://techmagnox.com/" target="_magnox" id="demosMenu">Company</a>
					</li>-->
					
					<!--<li class="nav-item dropdown">
						<a class="nav-link dropdown-toggle" href="#" id="portfolioMenu" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Company</a>
						<div class="dropdown-menu pb-3 pb-lg-0" aria-labelledby="demosMenu" >
							<div class="d-block d-sm-flex">
								<ul class="list-unstyled w-100 w-sm-50 pr-0 pr-lg-5">
									<li> <a class="dropdown-item" href="https://techmagnox.com/Magnox/About" target="_magnox">About Us</a> </li>
									<li> <a class="dropdown-item" href="<?php //echo site_url('home/people'); ?>">People</a> </li>
									<li> <a class="dropdown-item" href="<?php //echo site_url('home/portfolio'); ?>">Portfolio</a> </li>
									<li> <a class="dropdown-item" href="<?php //echo site_url('home/investors'); ?>">Investors</a> </li>
									<li> <a class="dropdown-item" href="<?php //echo site_url('home/success_stories'); ?>">Success Stories</a> </li>
									<li> <a class="dropdown-item" href="<?php //echo site_url('home/career'); ?>">Career</a> </li>        
									<li> <a class="dropdown-item" href="https://techmagnox.com/Magnox/Contact" target="_magnox">Contact Us</a> </li>
								</ul>
								
							</div>
							<div class="w-100 bg-grad pattern-overlay-2 p-4 mt-3 all-text-white d-none d-lg-flex">
								<div class="align-self-center mr-4">
									<a href="<?php //echo base_url('assets/files/magnox.pdf');?>" target="_blank" class="btn btn-outline-white btn-sm mb-0 align-self-center ml-auto">Download our Brochure</a>
								</div>
								
							</div>
						</div>
					</li>-->
					
				</ul>
			</div>
			<div class="navbar-nav">
				<!-- extra item Btn-->
				<div class="nav-item border-0 d-none d-lg-inline-block align-self-center">
					<a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Join Now!</a>
				</div>
			</div>
		</div>
	</nav>
</header>