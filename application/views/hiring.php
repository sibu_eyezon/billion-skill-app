<!-- =======================
Banner innerpage -->


<section class="testimonilas-area pt-95 pb-70 white-bg">
	<div class="container">

		<div class="row">
			<div class="col-sm-12 text-center mt-5 mb-4">
				<h2 class=" display-4">Corporates</h2>
				<h5>Lets join our hands for create a better Ecosystem for upskilling and jobs</h5>
			</div>
		</div>
	</div>
</section>
<!-- =======================
Banner innerpage -->
<section id="about" class="about-area pt-100 pb-70">
    <div class="container">
		<div class="row">

			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Corporate Programs</h3>
						<p>The Programs which we expertise in are as follows</p>
						<ul class="list-group list-group-borderless list-group-icon-primary-bg mb-4">
							<li class="list-group-item"><i class="fa fa-check"></i> React Js  </li>
							<li class="list-group-item"><i class="fa fa-check"></i> Node Js</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Database</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Ajile Technology</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Block Chain</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Data Science </li>
							<li class="list-group-item"><i class="fa fa-check"></i> Digital Marketing </li>
						</ul>
						<p>We are open to customize your Program as per your need. Please submit your requirement.</p>
						<a class="btn btn-grad mr-3" href="<?php echo site_url('home/explore_programs'); ?>">Submit Now</a>
					</div>
				</div>
			</div>

			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Better Hiring</h3>
      <p>Explore our skilled database in the following roles:  </p>
       <ul class="list-group list-group-borderless list-group-icon-primary-bg mb-4">
							<li class="list-group-item"><i class="fa fa-check"></i> Full Stack Developer  </li>
							<li class="list-group-item"><i class="fa fa-check"></i> IOT Engineer</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Data Analyst</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Digital Marketing</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Data Scientist</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Autocad Expert</li>
							<li class="list-group-item"><i class="fa fa-check"></i> HR and Sales </li>
						</ul>
						<p>Enroll now into our B2B Account. Post a job or internship for Free </p>
						<a class="btn btn-grad mr-3" href="<?php echo site_url('home/explore_programs'); ?>">Enroll Now</a>
					</div>
				</div>
			</div>

			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Mentors Connect</h3>
      Mentors connect Program is a Program where you can involve your employees for training the students as per your desired skills sets.
      Once the training is completed you can take them as intern. You can train the students in the following roles:
                <ul class="list-group list-group-borderless list-group-icon-primary-bg mb-4">
							<li class="list-group-item"><i class="fa fa-check"></i> Software Developer </li>

							<li class="list-group-item"><i class="fa fa-check"></i> Data Analyst</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Digital Marketing</li>
							<li class="list-group-item"><i class="fa fa-check"></i> Data Scientist</li>

							<li class="list-group-item"><i class="fa fa-check"></i> HR and Sales </li>
						</ul>
     <p> Here in the Training you can charge on monthly basis. </p>
      <a class="btn btn-grad mr-3" href="<?php echo site_url('home/explore_programs'); ?>">Enroll Now</a>
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section id="corporate" class="about-area pt-100 pb-70 bg-white">
	<div class="container">

		<div class="row">
			<div class="col-xl-6">
				<h1 class="text-center"> Alumni Testimonials </h1>
				<div class="col-md-12 testimonials testimonials-border py-6">
					<div class="owl-carousel testi-full owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="1" data-items-md="1" data-items-xs="1">
						<?php
							foreach($alumnispk as $fbow){
								echo '<div class="item">
									<div class="testimonials-wrap">
										<div class="testi-text">
											<div>'.trim($fbow->txt_message).'</div>
											<div class="testi-avatar"> <img src="<?php echo base_url();?>https://learn.techmagnox.com'.$fbow->txt_profile_pic.'" onerror="this.src=`'.base_url().'assets/images/thumbnails/default-avatar.png`" alt="avatar"> </div>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_name).'</h6>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_organization).'</h6>
											<h6 class="mb-0 mt-3">'.trim($fbow->txt_position).'</h6>
										</div>
									</div>
								</div>';
							}
						?>
					</div>
				</div>
			</div>
            <div class="col-xl-6">
				<div class="section-title mb-50 text-left">
					<h4 class="mb-0">Magnox Degree Holders Work in Companies like:

					</h4><br />
					<div class="row">
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/tcs.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/wipro.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/pws.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/micro.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/ibm.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/cap.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/thetaone.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/nasscom.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/indusnet.png" width="100" />
						</div>
							<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/mazars.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/monnet.png" width="100" />
						</div>
						<div class="col-sm-4 col-xs-4 text-center">
							<img src="<?php echo base_url();?>assets/images/brand/exide.png" width="100" />
						</div>
					</div>
				</div>
				<br><a href="<?php echo site_url('home/hiring_partners'); ?>" class="btn btn-grad btn-sm pull-right">Know More</a><br>
			</div>
		</div>
	</div>
</section>

<section class="testimonilas-area  white-bg">
	<div class="container">

		<div class="row">
			<div class="col-md-12">
				<div class="section-title text-center">
					<div class="section-title-heading">
						<h1 class="primary-color">Mentorship Programs of On-Demand Skill sets</h1>
					</div>
				</div>
			</div>
		</div>

		<div class="row">
			<div class="col-12">
				<div class="owl-carousel owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="3" data-items-lg="3" data-items-md="2" data-items-sm="2" data-items-xs="1">
					<?php
					if(!empty($programs)){
						$i=1;
						foreach($programs as $prow){
							$prog_id = $prow->id;
							$title = trim($prow->title);
							$fee = trim($prow->feetype);
							$amt = (int)$prow->total_fee;
							$dis = (int)($prow->discount);
							if($dis!=0){
								$famt = floatval($amt*(1-(floatval($dis/100))));
							}else{
								$famt = 0;
							}
							$curdate = strtotime(date('Y-m-d'));
							$ldt = strtotime(date('Y-m-d',strtotime($prow->aend_date)));
							$sdt = strtotime(date('Y-m-d',strtotime($prow->astart_date)));
							$eddt = strtotime(date('Y-m-d',strtotime($prow->end_date)));
							$stdt = strtotime(date('Y-m-d',strtotime($prow->start_date)));
							$category = trim($prow->category);
							if($category!='Seminar Program' && $category!='Webinar Program'){
								$category = (($prow->prog_level=='3')? 'Certification' : (($prow->prog_level=='2')? 'Mentorship' : 'Live Buddies')).' Program';
							}
					?>
					<div class="post">
						<a class="post-title" style="margin:0 !important;" href="<?php echo base_url('get_program_details/?id='.base64_encode($prog_id)); ?>">
						<img src="https://learn.techmagnox.com/assets/img/banner/<?php echo $prow->banner; ?>" style="width:100%; height: 230px;" alt="" onerror="this.src='https://learn.techmagnox.com/assets/img/sample.jpg'">
						</a>
						<div class="post-info" style="height:210px;">
							<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!"><?php echo $category; ?></a></span>

							<a class="post-title" href="<?php echo base_url('get_program_details/?id='.base64_encode($prog_id)); ?>"><?php echo $title; ?></a>
							<p class="mb-0" style="font-size:14px;">
							<?php
								  $dur = intval(trim($prow->duration));
								  echo 'Duration: '.$dur.' '.trim($prow->dtype).(($dur==1)? '':'s').';	Total Hours: '.trim($prow->prog_hrs).' Hrs<br>';

								  echo (($fee=='Paid')? (($famt==0)? 'Rs '.$amt : 'Rs. strike>'.$amt.'</strike> '.$famt) : $fee).'<br>';
								  if($stdt == $eddt){
									  echo 'On: '.date('jS M Y',strtotime($prow->start_date)).'<br>';
								  }else{
									  echo 'From: '.date('jS M Y',strtotime($prow->start_date)).' To '.date('jS M Y',strtotime($prow->end_date)).'<br>';
								  }
								  echo 'Deadline: <span class="text-danger">'.(($curdate<=$ldt)? date('jS M Y',strtotime($prow->aend_date)) : 'Expired').'</span>';
							?>
							</p>
						</div>
					</div>
					<?php } } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="title text-center">

					<h2>Advisors and Mentors
						<a class="btn btn-grad btn-sm pull-right" href="<?php echo site_url('home/explore_programs'); ?>" style="color:#fff">Explore All</a>
					</h2>
					<p>World Class Advisors and Industry Mentors to strengthen your base, guide you.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="5" data-items-xs="1">
					<?php if(!empty($ilist)){
						foreach($ilist as $irow){
							$name = trim($irow->txt_instructor_name);
							$designation = trim($irow->txt_instructor_desg);
							$photo_sm = trim($irow->txt_profile_pic);
					?>
					<div class="item">
						<div class="team-item">
							<div class="team-avatar">
								<img class="img-responsive" src="<?php echo 'https://learn.techmagnox.com/'.$photo_sm; ?>" onerror="this.src='<?= base_url('assets/images/people/default-avatar.png'); ?>'">
							</div>
							<div class="team-desc">
								<h5 class="team-name"><?= $name; ?></h5>
								<span class="team-position"><?= $designation; ?></span>
								<p class="text-justify"><?= substr(trim($irow->txt_instructor_dtls), 0, 60).'...'; ?></p>
							</div>
						</div>
					</div>
					<?php } } ?>

				</div>
				<a href="<?php echo site_url('home/instructor/ilist'); ?>" class="btn btn-grad btn-sm pull-right">All Instructors</a>
			</div>
		</div>
	</div>
</section>

<section class="team-area pt-95 pb-70 gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<div class="section-title mb-50 text-center">
					<div class="section-title-heading mb-20">
						<h1 class="primary-color">FAQ</h1>      <br>
					</div>

				</div>
			</div>
		</div>

			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="team-wrapper mb-30">
                      <div class="accordion accordion-grad" id="accordion3">
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="h6 mb-0" data-toggle="collapse" href="#collapse-7">How many free samples can i redeem?</a>
							</div>
							<div class="collapse show" id="collapse-7" data-parent="#accordion3">
								<div class="accordion-content">Due to the limited quantity, each member's account is only entitled to 1 unique free sample. You can check out up to 4 free samples in each checkout. We take such matters very seriously and will look into individual cases thoroughly. </div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-8">What are the payment methods available?</a>
							</div>
							<div class="collapse" id="collapse-8" data-parent="#accordion3">
								<div class="accordion-content"> At the moment, we only accept Credit/Debit cards and Paypal payments. Paypal is the easiest way to make payments online. While checking out your order. Be sure to fill in correct details for fast & hassle-free payment processing.</div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-9">Can i pay without a paypal account?</a>
							</div>
							<div class="collapse" id="collapse-9" data-parent="#accordion3">
								<div class="accordion-content"> Yes! It is commonly misunderstood that a Paypal account is needed in order to make payments through Paypal. The truth is you DO NOT need one, although we strongly recommend you sign up to enjoy the added ease of use. </div>
							</div>
						</div>
					</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="team-wrapper mb-30">
      <div class="accordion accordion-grad" id="accordion4">
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="h6 mb-0" data-toggle="collapse" href="#collapse-1">How many free samples can i redeem?</a>
							</div>
							<div class="collapse show" id="collapse-1" data-parent="#accordion4">
								<div class="accordion-content">Due to the limited quantity, each member's account is only entitled to 1 unique free sample. You can check out up to 4 free samples in each checkout. We take such matters very seriously and will look into individual cases thoroughly. </div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-2">What are the payment methods available?</a>
							</div>
							<div class="collapse" id="collapse-2" data-parent="#accordion4">
								<div class="accordion-content"> At the moment, we only accept Credit/Debit cards and Paypal payments. Paypal is the easiest way to make payments online. While checking out your order. Be sure to fill in correct details for fast & hassle-free payment processing.</div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-3">Can i pay without a paypal account?</a>
							</div>
							<div class="collapse" id="collapse-3" data-parent="#accordion4">
								<div class="accordion-content"> Yes! It is commonly misunderstood that a Paypal account is needed in order to make payments through Paypal. The truth is you DO NOT need one, although we strongly recommend you sign up to enjoy the added ease of use. </div>
							</div>
						</div>
					</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
