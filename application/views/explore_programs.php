<style>
#loader{
	position: relative;
	top:10%;
	left:45%;
	width: 200px;
	height: 200px;
}
.item {
	transition: all .5s linear;
}
.hidden {
	display: none;
	opacity: 0
	transition: all .5s linear;
}
.not_hidden {
	display: block;
	opacity: 1;
	transition: all .5s linear;
}
</style>
<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4">Explore Programs</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12">
				<form action="#" method="GET" id="frmFilter">
					<div class="row justify-content-center">
						<div class="col-md-4">
							<div class="form-group">
								<input type="text" name="title" id="title" class="form-control" placeholder="Enter program name">
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<div class="form-check-inline">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios1" value="All" checked>
									<label class="form-check-label" for="exampleRadios1">
										All
									</label>
								</div>
								<div class="form-check-inline">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios2" value="free">
									<label class="form-check-label" for="exampleRadios2">
										Free
									</label>
								</div>
								<div class="form-check-inline">
									<input class="form-check-input" type="radio" name="exampleRadios" id="exampleRadios3" value="paid">
									<label class="form-check-label" for="exampleRadios3">
										Paid
									</label>
								</div>
							</div>
						</div>
						<div class="col-md-4">
							<div class="form-group">
								<select class="form-control" name="ptype" id="ptype">
									<option value="">Select Type</option>
									<option value="live buddies program">Live Buddies Program</option>
									<option value="mentorship program">Mentorship Program</option>
									<option value="certification program">Certification Program</option>
								</select>
							</div>
						</div>
					</div>
					
				</form>
			</div>
		
			<div class="col-md-12">
				<div id='loader' style='display: none;'>
				  <img src='https://cdn.dribbble.com/users/563824/screenshots/3633228/untitled-5.gif'>
				</div>
				<div id="filterData"></div>
			</div>
			
		</div>
	</div>
</section>
<script>
	function getFilterPrograms()
	{
		$('#loader').show();
		$('#filterData').html("");
		$.ajax({
			url: baseURL+'getFilteredData',
			type: 'GET',
			data: { program: 'programs' },
			success: (res)=>{
				$('#loader').hide();
				$('#filterData').html(res);
			},
			error: (errors)=>{
				$('#loader').hide();
			}
		});
	}
	
	$(window).on('load',function() {
		getFilterPrograms();
	});
	$('#title').on('keyup',()=>{
		filterPrograms();
	});
	$('#ptype').on('change', ()=>{
		filterPrograms();
	});
	$('input:radio[name="exampleRadios"]').on('change',()=>{
		filterPrograms();
	});
	
	function filterPrograms()
	{
		var title = $('#title').val().trim().toLowerCase();
		var ptype = $('#ptype').val().trim();
		var price = $('input:radio[name="exampleRadios"]:checked').val();
		
		
		if(title === "" && ptype === "" && price === "All"){
			$('.item').removeClass('hidden').addClass('not_hidden');
		}else{
			$('.item').removeClass('not_hidden').addClass('hidden');
			if(title !== ""){
				$(".item").each(function() {
					var txt = $(this).data("title").toLowerCase(),
						price = $(this).data("price").toLowerCase(),
						type = $(this).data("type").toLowerCase();

					if (txt.indexOf(title) > -1 || price.indexOf(title) > -1 || type.indexOf(title) > -1) {
						$(this).removeClass('hidden').addClass('not_hidden');
					}
				});
				//$( '.item[data-title="' + title + '"]' ).removeClass('hidden').addClass('not_hidden');
			}
			if(ptype !== ""){
				$( '.item[data-type="' + ptype + '"]' ).removeClass('hidden').addClass('not_hidden');
			}
			if(price !== "All"){
				$( '.item[data-price="' + price + '"]' ).removeClass('hidden').addClass('not_hidden');
			}
		}
	}
	
</script>