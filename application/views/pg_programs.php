<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4"><?=$pageTitle; ?></h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="bg-light">
	<div class="container h-100">
		<div class="row jsutify-content-center">
			<div class="col-md-4">
				<div class="item">
					<div class="post">
						<img src="<?php echo base_url();?>assets/images/program/sparc.jpg" alt="">
						<div class="post-info">
							<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Workshop</a></span>
							
							<a class="post-title" href="#!">SPARC online Workshop</a>
							<p class="mb-0">Indo US SPARC online Workshop 
							on
							APPLICATIONS OF ELECROTHERAPY 
							IN HEALTHCARE </p><br />
							<a href="https://sparcaeh2021.in/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Website</a> 
							<a href="https://learn.techmagnox.com/purdueRegister" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Enroll</a>
							<a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="item">
					<div class="post">
						<img src="<?php echo base_url();?>assets/images/program/indogfoe.jpg" alt="">
						<div class="post-info">
							<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Symposium</a></span>
							
							<a class="post-title" href="#!">INDOGFOE 2020</a>
							<p class="mb-0">11th Indo German Frontiers of Engineering Symposium organized by IIT Kharagpur & the Alexander von Humboldt Foundation </p><br />
							<a href="https://indogfoe2020.in/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Website</a> 
	<a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="item">
					<div class="post">
						<img src="<?php echo base_url();?>assets/images/program/adms.jpg" alt="">
						<div class="post-info">
							<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Program</a></span>
							
							<a class="post-title" href="#!">MMST 2020 @ IIT Kharagpur</a>
							<p class="mb-0">Interested in Research in Medical Science and Technology Join MMST @ SMST, IIT Kharagpur </p>
							<br /><a href="http://gate.iitkgp.ac.in/mmst/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">GATE Website</a> <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<div class="item">
					<div class="post">
						<img src="<?php echo base_url();?>assets/images/program/sathi.jpg" alt="">
						<div class="post-info">
							<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!">Training</a></span>
							
							<a class="post-title" href="#!">SATHI DST, Govt of India</a>
							<p class="mb-0">Avail the Facilities of SATHI, DST & IIT Kharagpur</p>
							<br />
							<a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a> <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Details</a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>