<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4">LMS - Features</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
    <div class="row">
			<div class="col-sm-9 col-md-9">
<ul class="list-group list-group-borderless list-group-icon-primary-bg mb-4">
    <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Live Class: </b> This works well in low bandwidth as well. Live Class through Video Conference is one of the most important Feature of this product. The Live Class link can be send to an indivual or to the whole class by clicking a button. The students will get a alert for the class and can direcly enter the class from a link. There are different features that can be accesses during the class as well like doubts, resources, messages, notices etc.  </p>  </li>
	<li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Attendance  : </b>Incase of Online Learning Classes the attendance will be collected automatically. Attendance Reports are also there to check which students attendance is how much or yearly attendance report for internal marks. </p> </li>
	<li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Virtual Labs : </b> Virtual Labs are a facility given to the students for their hands-on experience of the laboratory. Now we have Programing / Coding and Database Platform. We are coming with several other labs soon. </p>  </li>
    <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Lectures : </b> Here you can upload any Lecture file like Video, Youtube link, ppt etc.  </p>  </li>
    
      <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Resources  : </b> Any type of Resources in the Program will be uploaded here like Books, Suggestions, Question Papers etc.  </p>  </li>

     <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Assignment  : </b> Teachers will upoload Assignment of any particular Course, an assignment is a task given to the students to complete and submit within a particular deadline. The Students will complete the Assignment and submit it within the Deadline. Teacher will go through the submissions of the students and add marks to each students those who has submitted the assignment </p>  </li>
     <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Examination  : </b> Examination is a very important part of the application. The Exam system is highly scalable and can accomodate any number of students. Different Sub Modules like Question Bank, Entry of New Questions, Test Structure, Seperate APP for Test giving by Students, Reports and Answer Sheet.  </p>  </li>
     
     <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Notice   : </b> Notice is a one way communication, the teachers will send the notice and students will view it. A Notice will also come with a notification and the students can see it.</p>  </li>
	 <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Messages and Chats   : </b> Messages and Chats are created for bothway communication between the teacher and students. </p>  </li>	
      <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Communication   : </b> We have different means and different types of communication. Notice, Message, Chat, Group Chat etc. One Way Communication, Both Way Communication, Chat Room, Notification etc. </p>  </li>
      <li class="list-group-item"><i class="fa fa-check"></i><p class="feature-box-desc"> <b>Doubt Box: </b> Through Doubt Box students can submit their doubt either during the class or after the class. Teachers will answer it when he or she feels comfortable.</p>  </li>					
					</ul>
		</div>
			<div class="col-sm-3 col-md-3 bg-light"><img src="<?php echo base_url();?>assets/images/1.jpg" alt=""></div></div>
	</div>
</section>