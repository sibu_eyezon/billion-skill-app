<!-- =======================
Banner innerpage -->
  <link rel="stylesheet" type="text/css" href="assets/css/gallery.css" />
<!-- =======================
Banner innerpage -->
<section class="pricing-page pricing pricing-center">
		<div class="container">
			<div class="row">
				<div class="col-md-8 mb-0 mb-md-5">
					<div class="p-0 text-center text-md-left">
						<h2>Gallery</h2>
						<p>Gothough our events Gallery. </p>
					</div>
				</div>
				<div class="col-md-4 text-center text-md-right align-self-center mb-5 ">
					<a class="btn btn-grad" href="#!">Start 30 days free trial!</a>
				</div>
			</div>
		</div></section>
 <section class="portfolio pb-0">
		<div class="container">
			<div class="row">
				<div class="col-md-12 p-0">
					<div class="nav justify-content-center">
						<ul class="nav-tabs nav-tabs-style-2 text-center px-2 p-md-0 m-0 mb-4">
							<li class="nav-filter active" data-filter="*">All Works</li>
							<li class="nav-filter" data-filter=".marketing">Marketing</li>
							<li class="nav-filter" data-filter=".digital">Digital</li>
							<li class="nav-filter" data-filter=".photo">Photography</li>
						</ul>
					</div>
					<div class="portfolio-wrap grid items-4 items-padding">
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item digital">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/01.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/01.jpg" data-fancybox="portfolio" data-caption="Fly in time"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Fly in time</a></h6>
									<p>Branding, Watch</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item photo">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/02.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/02.jpg" data-fancybox="portfolio" data-caption="Waiting window"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Waiting window</a></h6>
									<p>Photography</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item digital marketing">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/03.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/03.jpg" data-fancybox="portfolio" data-caption="Green Book"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Green Book</a></h6>
									<p>Graphic</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item digital">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/04.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/04.jpg" data-fancybox="portfolio" data-caption="Reebok Sneakers"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Reebok Sneakers</a></h6>
									<p>Sport, Running</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item photo">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/05.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/05.jpg" data-fancybox="portfolio" data-caption="Parking garage"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Parking garage</a></h6>
									<p>Car, Building</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item digital marketing photo">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/06.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/06.jpg" data-fancybox="portfolio" data-caption="The Pink Chair"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">The Pink Chair </a></h6>
									<p>Workspace</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item photo marketing">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/07.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/07.jpg" data-fancybox="portfolio" data-caption="Black & white"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Black & white</a></h6>
									<p>Photography</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item digital marketing">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/08.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/08.jpg" data-fancybox="portfolio" data-caption="Catch the plane"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Catch the plane</a></h6>
									<p>Graphic</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item digital marketing">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/09.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/09.jpg" data-fancybox="portfolio" data-caption="Nature book"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Nature book</a></h6>
									<p>Book</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item marketing">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/10.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/10.jpg" data-fancybox="portfolio" data-caption="Book on desk"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Book on desk</a></h6>
									<p>Work Desk</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item digital">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/11.jpg" alt="">
								</div>
								<div class="portfolio-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/portfolio/11.jpg" data-fancybox="portfolio" data-caption="Work desk"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Work desk</a></h6>
									<p>Workspace</p>
								</div>
							</div>
						</div>
						<!-- portfolio-card -->
						<div class="portfolio-card isotope-item photo">
							<div class="portfolio-card-body">
								<div class="portfolio-card-header">
									<img src="<?php echo base_url();?>assets/images/port/12.jpg" alt="">
								</div>
								<div class="port-card-footer">
									<a class="full-screen" href="<?php echo base_url();?>assets/images/port/12.jpg" data-fancybox="port" data-caption="Green Tree"><i class="ti-fullscreen"></i></a>
									<h6 class="info-title"><a href="#" title="">Green Tree</a></h6>
									<p>Nature</p>
								</div>
							</div>
						</div>
					</div>
					<!-- port wrap -->
				</div>
			</div>
		</div>
	</section>
