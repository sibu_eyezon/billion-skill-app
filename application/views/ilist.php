<style>
#loader{
	position: relative;
	top:10%;
	left:45%;
	width: 200px;
	height: 200px;
}
</style>
<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4">Advisors and Instructors</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
		<div id='loader' style='display: none;'>
		  <img src='https://cdn.dribbble.com/users/563824/screenshots/3633228/untitled-5.gif'>
		</div>
		<div class="row justify-content-center" id="filterData">
		</div>
	</div>
</section>
<script>
	$(window).on('load',function() {
		getFilterInstructors();
	});
	function getFilterInstructors()
	{
		$('#loader').show();
		$('#filterData').html("");
		$.ajax({
			url: baseURL+'getFilteredTeachers',
			type: 'GET',
			success: (res)=>{
				$('#loader').hide();
				$('#filterData').html(res);
			},
			error: (errors)=>{
				$('#loader').hide();
			}
		});
	}
</script>