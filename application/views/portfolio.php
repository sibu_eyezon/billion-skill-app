<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4">Portfolio</h2>
				<p>We have 60+ B2B Clients in Different Sectors including Government, Academic and Corporate</p>
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
		<div class="row justify-content-center">
				
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/01.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/02.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/03.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/04.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/05.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/06.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/27.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/08.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/09.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/10.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/11.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/12.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/26.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/14.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/15.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/16.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/17.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/18.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/19.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/20.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/21.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/22.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/13.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/25.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/07.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/13.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/20.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-2">
				<div class="card">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/portfolio/19.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			
		</div>
	
	</div>
</section>