<!-- =======================
Banner innerpage -->


<section class="testimonilas-area pt-95 pb-70 white-bg">
	<div class="container">

   <div class="row">
				<div class="col-sm-12 text-center mt-5 mb-4">
					<h2 class=" display-4">Live Buddies</h2>
					<h5>Most Experience seniors who can help you grow</h5>
				</div>
				<div class="col-md-2 mt-2">
					<div class="feature-box f-style-5 h-100 icon-grad">
						<div class="feature-box-icon"><i class="ti-panel"></i></div>
						<h5>100% Career Support</h5>

					</div>
				</div>
				<div class="col-md-2 mt-2">
					<div class="feature-box f-style-5 h-100 icon-grad">
						<div class="feature-box-icon"><i class="ti-palette"></i></div>
						<h5>Paid Internships</h5>

					</div>
				</div>
				<div class="col-md-2 mt-2">
					<div class="feature-box f-style-5 h-100 icon-grad">
						<div class="feature-box-icon"><i class="ti-gift"></i></div>
						<h5>Live Projects</h5>

					</div>
				</div>
					<div class="col-md-2 mt-2">
					<div class="feature-box f-style-5 h-100 icon-grad">
						<div class="feature-box-icon"><i class="ti-panel"></i></div>
						<h5 >Case Studies</h5>

					</div>
				</div>
				<div class="col-md-2 mt-2">
					<div class="feature-box f-style-5 h-100 icon-grad">
						<div class="feature-box-icon"><i class="ti-palette"></i></div>
						<h5 >Doubt Clearing</h5>

					</div>
				</div>
				<div class="col-md-2 mt-2">
					<div class="feature-box f-style-5 h-100 icon-grad">
						<div class="feature-box-icon"><i class="ti-gift"></i></div>
						<h5 >Industrial Mentors</h5>

					</div>
				</div>

		</div>
	</div>
</section>
<!-- =======================
Banner innerpage -->
<section id="about" class="about-area pt-100 pb-70">
    <div class="container">
		<div class="row">

			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Engineering</h3>
						<P>Our mentors come from strong industrial backgrounds with more than 10 years of experience
                        to provide you with the highest standards of industrial training and exposure </p>
					</div>
				</div>
			</div>

			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Management</h3>
      <p>We bring business gurus from various domains to help you walk their footsteps and help unleash the next biz guru in you</p>
					</div>
				</div>
			</div>

			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Entrepreneurship</h3>
						Manage your information, academics, events, alumni & placement.
					</div>
				</div>
			</div>

		</div>
	</div>
</section>

<section class="testimonilas-area pt-95 pb-70 white-bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<div class="section-title mb-50 text-center">
					<div class="section-title-para">
						<p class="gray-color text-justify">Our Objective is to bring a revolution in the way Learning and Hiring are managed in an organization. We aim to create a more transparent and bias free plaform to create more efficient environment for the employeer and the employees.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>

<section class="testimonilas-area  white-bg">
	<div class="container">
		
		<div class="row">
			<div class="col-md-12">
				<div class="section-title text-center">
					<div class="section-title-heading">
						<h1 class="primary-color">Live Buddies Programs of On-Demand Skill sets</h1>
					</div>
				</div>
			</div>
		</div>
		
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="3" data-items-lg="3" data-items-md="2" data-items-sm="2" data-items-xs="1">
					<?php
					if(!empty($programs)){
						$i=1;
						foreach($programs as $prow){
							$prog_id = $prow->id;
							$title = trim($prow->title);
							$fee = trim($prow->feetype);
							$amt = (int)$prow->total_fee;
							$dis = (int)($prow->discount);
							if($dis!=0){
								$famt = floatval($amt*(1-(floatval($dis/100))));
							}else{
								$famt = 0;
							}
							$curdate = strtotime(date('Y-m-d'));
							$ldt = strtotime(date('Y-m-d',strtotime($prow->aend_date)));
							$sdt = strtotime(date('Y-m-d',strtotime($prow->astart_date)));
							$eddt = strtotime(date('Y-m-d',strtotime($prow->end_date)));
							$stdt = strtotime(date('Y-m-d',strtotime($prow->start_date)));
							$category = trim($prow->category);
							if($category!='Seminar Program' && $category!='Webinar Program'){
								$category = (($prow->prog_level=='3')? 'Certification' : (($prow->prog_level=='2')? 'Mentorship' : 'Live Buddies')).' Program';
							}
					?>
					<div class="post">
						<a class="post-title" style="margin:0 !important;" href="<?php echo base_url('get_program_details/?id='.base64_encode($prog_id)); ?>">
						<img src="https://learn.techmagnox.com/assets/img/banner/<?php echo $prow->banner; ?>" style="width:100%; height: 230px;" alt="" onerror="this.src='https://learn.techmagnox.com/assets/img/sample.jpg'">
						</a>
						<div class="post-info" style="height:210px;">
							<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="#!"><?php echo $category; ?></a></span>
							
							<a class="post-title" href="<?php echo base_url('get_program_details/?id='.base64_encode($prog_id)); ?>"><?php echo $title; ?></a>
							<p class="mb-0" style="font-size:14px;">
							<?php
								  $dur = intval(trim($prow->duration));
								  echo 'Duration: '.$dur.' '.trim($prow->dtype).(($dur==1)? '':'s').';	Total Hours: '.trim($prow->prog_hrs).' Hrs<br>';
								  
								  echo (($fee=='Paid')? (($famt==0)? 'Rs '.$amt : 'Rs. strike>'.$amt.'</strike> '.$famt) : $fee).'<br>';
								  if($stdt == $eddt){
									  echo 'On: '.date('jS M Y',strtotime($prow->start_date)).'<br>';
								  }else{
									  echo 'From: '.date('jS M Y',strtotime($prow->start_date)).' To '.date('jS M Y',strtotime($prow->end_date)).'<br>';
								  }
								  echo 'Deadline: <span class="text-danger">'.(($curdate<=$ldt)? date('jS M Y',strtotime($prow->aend_date)) : 'Expired').'</span>';
							?>
							</p>
						</div>
					</div>
					<?php } } ?>
				</div>
			</div>
		</div>
	</div>
</section>

<section>
	<div class="container">
		<div class="row">
			<div class="col-12 col-lg-12">
				<div class="title text-center">

					<h2>Advisors and Mentors
						<a class="btn btn-grad btn-sm pull-right" href="<?php echo site_url('home/explore_programs'); ?>" style="color:#fff">Explore All</a>
					</h2>
					<p>World Class Advisors and Industry Mentors to strengthen your base, guide you.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel owl-grab dots-primary" data-arrow="false" data-dots="true" data-items-xl="5" data-items-xs="1">
					<?php if(!empty($ilist)){ 
						foreach($ilist as $irow){ 
							$name = trim($irow->txt_instructor_name);
							$designation = trim($irow->txt_instructor_desg);
							$photo_sm = trim($irow->txt_profile_pic); 
					?>
					<div class="item">
						<div class="team-item">
							<div class="team-avatar">
								<img class="img-responsive" src="<?php echo 'https://learn.techmagnox.com/'.$photo_sm; ?>" onerror="this.src='<?= base_url('assets/images/people/default-avatar.png'); ?>'">
							</div>
							<div class="team-desc">
								<h5 class="team-name"><?= $name; ?></h5>
								<span class="team-position"><?= $designation; ?></span>
								<p class="text-justify"><?= substr(trim($irow->txt_instructor_dtls), 0, 60).'...'; ?></p>
							</div>
						</div>
					</div>
					<?php } } ?>

				</div>
				<a href="<?php echo site_url('home/instructor/ilist'); ?>" class="btn btn-grad btn-sm pull-right">All Instructors</a>
			</div>
		</div>
	</div>
</section>

<section class="team-area pt-95 pb-70 gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<div class="section-title mb-50 text-center">
					<div class="section-title-heading mb-20">
						<h1 class="primary-color">FAQ</h1>      <br>
					</div>

				</div>
			</div>
		</div>

			<div class="row">
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="team-wrapper mb-30">
                      <div class="accordion accordion-grad" id="accordion3">
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="h6 mb-0" data-toggle="collapse" href="#collapse-1">What is Live Buddies?</a>
							</div>
							<div class="collapse show" id="collapse-1" data-parent="#accordion3">
								<div class="accordion-content">It is a live interaction session led by students who have interned / got placements at your dream companies.</div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-2">Will I get 1-o-1 mentorships?</a>
							</div>
							<div class="collapse" id="collapse-2" data-parent="#accordion3">
								<div class="accordion-content"> You will get a time slot per week by your buddy for guidance and doubt clearing.</div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-3">Is this a subscription program?</a>
							</div>
							<div class="collapse" id="collapse-3" data-parent="#accordion3">
								<div class="accordion-content"> Yes. You can choose your own buddy and subscription amount based on the types of resources / guidance you are looking for.</div>
							</div>
						</div>
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-4">What types of resources are available in mentor connect program?</a>
							</div>
							<div class="collapse" id="collapse-4" data-parent="#accordion3">
								<div class="accordion-content"> You can get guidance on getting placements in particular internship/job profiles, clearing concepts in certain domains, personal interview mockups, peer-to-peer doubt clearing sessions and many more.</div>
							</div>
						</div>
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-5">Will I get a certificate?</a>
							</div>
							<div class="collapse" id="collapse-5" data-parent="#accordion3">
								<div class="accordion-content"> Yes, certificates would be available once you have completed atleast one month on the program.</div>
							</div>
						</div>
					</div>
					</div>
				</div>
				<div class="col-xl-6 col-lg-6 col-md-6">
					<div class="team-wrapper mb-30">
					<div class="accordion accordion-grad" id="accordion4">
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="h6 mb-0" data-toggle="collapse" href="#collapse-6">How do I join the community?</a>
							</div>
							<div class="collapse" id="collapse-6" data-parent="#accordion4">
								<div class="accordion-content"> Anyone is given access to the community for free. </div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-7">Do you offer any discounts?</a>
							</div>
							<div class="collapse show" id="collapse-7" data-parent="#accordion4">
								<div class="accordion-content">The prices are already discounted. More discounts are available after enrolling via the college / university program.</div>
							</div>
						</div>
						<!-- item -->
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-8">What kind of payment methods are accepted?</a>
							</div>
							<div class="collapse" id="collapse-8" data-parent="#accordion4">
								<div class="accordion-content"> We accept all online payment methods like debit/credit card, netbanking, UPI payment. We do not accept any offline payment methods like cash or cheque. </div>
							</div>
						</div>
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-9">What happens next once I fill the registration fees?</a>
							</div>
							<div class="collapse" id="collapse-9" data-parent="#accordion4">
								<div class="accordion-content"> You will receive login details and the next steps via an email. </div>
							</div>
						</div>
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-10">Is EMI option available?</a>
							</div>
							<div class="collapse" id="collapse-10" data-parent="#accordion4">
								<div class="accordion-content"> You can either pay the course fees in full initially or you can opt for our zero interest monthly EMI option. </div>
							</div>
						</div>
						<div class="accordion-item">
							<div class="accordion-title">
								<a class="collapsed" data-toggle="collapse" href="#collapse-11">Why should I come here instead of going to other sources?</a>
							</div>
							<div class="collapse" id="collapse-11" data-parent="#accordion4">
								<div class="accordion-content"> There’s a ton of materials available online but you won’t find personally curated materials, live projects, case studies, 1-o-1 mentor guidance elsewhere. </div>
							</div>
						</div>
					</div>
					</div>
				</div>

			</div>
		</div>
	</div>
</section>
