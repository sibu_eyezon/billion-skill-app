<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class="display-4"><?= $blog[0]->title; ?></h2>
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="blog-page pb-0">
	<div class="container">
		<div class="row">
			
			<!-- sidebar start -->
			<aside class="col-md-3 sidebar order-last order-md-first">

				<!-- Posts Widget -->
				<div class="widget widget-post">
					<h5 class="widget-title">Recent Posts</h5>
					<?php
						if(!empty($othr_blogs)){
							foreach($othr_blogs as $oblg){
								$bg_image = trim($oblg->banner);
								if($bg_image!=""){
									$remoteFile = 'https://learn.techmagnox.com/'.$bg_image;
									$handle = @fopen($remoteFile, 'r');
									if(!$handle){
										$csrc = 'https://learn.techmagnox.com/'.$bg_image;
									}else{
										$csrc = base_url('assets/images/blog/grid/03.jpg');
									}
								}else{
									$csrc = base_url('assets/images/blog/grid/03.jpg');
								}
								echo '<div class="widget-post clearfix">
										<div class="widget-image">
											<img class="border-radius-3" src="'.$csrc.'" alt="">
										</div>
										<div class="details">
											<a href="#">'.trim($oblg->title).'</a>
											<p class="date">'.date('M d, Y', strtotime($oblg->create_date_time)).'</p>
										</div>
									</div>';
							}
						}
					?>
				</div>

				<!-- Tag Widget -->
				<div class="widget">
					<h5 class="widget-title">Skills</h5>
					<div class="tags">
						<?php
							foreach($skills as $bks){
								echo '<a href="javascript:;">'.trim($bks->name).'</a>';
							}
						?>
					</div>
				</div>
			</aside>
			<!-- sidebar end -->
			
			<!-- blog start -->
			<div class="col-md-9 mb-6 order-first order-md-first">
				<div class="post-item">
					<div class="post-item-wrap">
						<div class="post-image">
							<?php
							$blg_image = trim($blog[0]->banner);
							if($blg_image!=""){
								$remoteFile = 'https://learn.techmagnox.com/'.$blg_image;
								$handle = @fopen($remoteFile, 'r');
								if(!$handle){
									$blsrc = 'https://learn.techmagnox.com/'.$blg_image;
								}else{
									$blsrc = base_url('assets/images/blog/grid/03.jpg');
								}
							}else{
								$blsrc = base_url('assets/images/blog/grid/03.jpg');
							}
							?>
							<a href="#"> <img src="<?= $blsrc; ?>" alt=""  class="w-100"> </a>
						</div>
						<div class="post-item-desc">
							<span class="post-meta"><?= date('M y',strtotime($blog[0]->create_date_time)); ?>,</span>
							<span class="post-meta"><a href="#">Admin,</a></span>
							<span class="post-meta"><a href="#"><i class="ti-comment-alt"></i>06 Comments</a></span>
							<br><br>
							<?php
							echo trim($blog[0]->body);
							?>
						</div>
					</div>
				</div>
				<div class="divider mb-4"></div>
				<div class="row">
					<div class="tags col-12 col-sm-8 text-center text-sm-left">
						<?php
							foreach($category as $bkc){
								echo '<a href="javascript:;">'.trim($bkc->name).'</a>';
							}
						?>
					</div>
					<div class="col-12 col-sm-4 text-center text-sm-right">
						<ul class="social-icons si-colored-bg light">
							<li class="social-icons-item mb-2 social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
							<li class="social-icons-item mb-2 social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							<li class="social-icons-item mb-2 social-gplus"><a class="social-icons-link" href="#"><i class="fa fa-google-plus"></i></a></li>
							<li class="social-icons-item mb-2 social-linkedin"><a class="social-icons-link" href="#"><i class="fa fa-linkedin"></i></a></li>
						</ul>
					</div>
				</div>
				<div class="divider mt-3"></div>
			</div>
			<!-- blog end -->
		</div>
	</div>
</section>