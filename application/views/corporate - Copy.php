<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-grad">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4 display-md-1 mb-2 mb-md-n4 mt-9">Corporate Training</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
		<h3 class="white-color f-700 text-primary text-center">Advantages</h3>
		<br>
		<h6 class="text-center">Designed for all your explicit training needs. We deliver outcome centric active. Learning with the help of award wining platform, top class faculty members, specialzed contents:</h6>
		<div class="row">
			
			<div class="col-sm-3">
				<div class="card border-0" style="height:200px;">
					<div class="card-body">
					<h4 class="card-title text-center text-info"><i class="fa fa-rss-square" style="font-size: 3rem;"></i><br>Online Classroom and Training</h4>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="card border-0" style="height:200px;">
					<div class="card-body">
					<h4 class="card-title text-center text-info"><i class="fa fa-window-restore" style="font-size: 3rem;"></i><br>Blending Training</h4>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="card border-0" style="height:200px;">
					<div class="card-body">
					<h4 class="card-title text-center text-info"><i class="fa fa-users" style="font-size: 3rem;"></i><br>Top class faculty members</h4>
					</div>
				</div>
			</div>
			<div class="col-sm-3">
				<div class="card border-0" style="height:200px;">
					<div class="card-body">
					<h4 class="card-title text-center text-info"><i class="fa fa-th-list" style="font-size: 3rem;"></i><br>Self-Paced Training</h4>
					</div>
				</div>
			</div>
			
		</div>
		<h3 class="white-color f-700 text-primary text-center">Technologies, We traine</h3>
		<br>
		<div class="row justify-content-center">
		
			<div class="col-sm-3">
				<img src="<?php echo site_url('assets/images/bg/machine-learning.jpg'); ?>" class="img-fluid w-100"/>
			</div>
			<div class="col-sm-3">
				<img src="<?php echo site_url('assets/images/bg/data-science.jpg'); ?>" class="img-fluid w-100"/>
			</div>
			<div class="col-sm-3">
				<img src="<?php echo site_url('assets/images/bg/blockchain.jpg'); ?>" class="img-fluid w-100"/>
			</div>
			<div class="col-sm-3">
				<img src="<?php echo site_url('assets/images/bg/engineering-draw.jpg'); ?>" class="img-fluid w-100"/>
			</div>
			<div class="col-sm-3">
				<img src="<?php echo site_url('assets/images/bg/management.jpg'); ?>" class="img-fluid w-100"/>
			</div>
			<div class="col-sm-3">
				<img src="<?php echo site_url('assets/images/bg/oracle.jpg'); ?>" class="img-fluid w-100"/>
			</div>
			<div class="col-sm-3">
				<img src="<?php echo site_url('assets/images/bg/agile-development.jpg'); ?>" class="img-fluid w-100"/>
			</div>
			<div class="col-sm-3">
				<img src="<?php echo site_url('assets/images/bg/dbms.jpg'); ?>" class="img-fluid w-100"/>
			</div>
			
		</div>
	</div>
</section>