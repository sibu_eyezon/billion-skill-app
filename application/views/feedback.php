<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class="display-4">Feedback Form</h2>
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
		<div class="row">
			<div class="col-md-8 mx-auto">
				<?php
					if($this->session->flashdata('errors')!=""){
						echo '<div class="alert alert-warning alert-dismissible">
						  <button type="button" class="close" data-dismiss="alert">&times;</button>
						  '.$this->session->flashdata('errors').'
						</div>';
					}
				?>
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">We will be happy to receive your feedback</h4>
					</div>
					<div class="card-body">
						<form action="<?php echo site_url('home/insertFeedback'); ?>" method="POST">
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="">Firstname</label>
										<input type="text" name="fname" id="fname" class="form-control"/>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="">Lastname</label>
										<input type="text" name="lname" id="lname" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-6">
									<div class="form-group">
										<label for="">Email</label>
										<input type="email" name="email" id="email" class="form-control"/>
									</div>
								</div>
								<div class="col-sm-6">
									<div class="form-group">
										<label for="">Mobile</label>
										<input type="text" name="mobile" id="mobile" class="form-control"/>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12">
									<div class="form-group">
										<label for="">Message</label>
										<textarea name="msg" id="msg" class="form-control w-100" rows="10" cols="80"></textarea>
									</div>
								</div>
							</div>
							<div class="row">
								<div class="col-sm-12 justify-content-center">
									<button type="submit" class="btn btn-md btn-success">Submit</button>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>