<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-grad">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4 display-md-1 mb-2 mb-md-n4 mt-9">People</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="team social-hover team-overlay pb-0">
		<div class="container">
			<div class="row">
				<div class="col-12 col-lg-8 mx-auto">
					<div class="title text-center">
						<h2>Founders and Creative Team</h2>
					</div>
				</div>
			</div>
			<div class="row">
				<!-- Team item -->
                <div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/nrm.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name"> Prof N R Mandal</h5>
							<span class="team-position">Chief Advisor </span>
							<p style="text-align:justify">Ex Professor & Dean of Students Affairs of IIT Kharagpur. He is the Fellow of Royal Institution of Naval Architects, Institution of Engineers. His specialized fields are Energy, Engineering Design, Alumninum Wealding and more.</p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/raj.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Dr. Rajarshee Ray</h5>
							<span class="team-position">Senior Technical Advisor</span>
							<p style="text-align:justify">Founder and CTO of SoS Corp. joins our Board. He has done his PhD from The University of Texas & Health Mgmt Course from Harvard University. He was the Chief R&D Architect in Sun Microsystems, Senior Engineer at Intel Corp., USA etc. He will be the key player to introduce AI & Deep Learning. </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/guha.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Dr. Debasish Guha</h5>
							<span class="team-position">Mentor</span>
							<p style="text-align:justify">Completed his BE from Javepur Unversty and ME & PhD from IIT Kharagpur in the Department of Mechancal Engneering. He works as the Director of TCS in the Telecommunication Field.</p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>

				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/Abhijit_Mukherjee7.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Prof. Abhijit Mukherjee</h5>
							<span class="team-position">Associate Professor</span>
							<p style="text-align:justify">Prof. Abhijit Mukherjee, an Associate Professor of IIT Kharagpur, He has done his PhD and Graduated from the University of Kentucky, USA and completed postdoctoral work at the University of Texas at Austin, USA. He has been conferred the National Geoscience Award by the President of India.</p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/sougam.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Mr. Sougam Maity</h5>
							<span class="team-position">CEO and Co-Founder</span>
							<p style="text-align:justify">He has completed his BTech Degree from CEM Kolaghat in Computer Science and Engineering. He was also involved in in different projects at IIT Kharagpur. Has done projects in NLP (Named Entity Recognition, Spell Checking), Virtual Lab and others</p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/arnab.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Mr. Arnab Nath</h5>
							<span class="team-position">Senior Consultant </span>
							<p  style="text-align:justify">Completed  integrated MSc from IIT Kharagpur in the Dept of Mathametics. Arnab Nath has 15 years experience in the IT field. He has worked in Wipro, Deloittee. He is presently working as Senior Manager in Deloittee.  </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center" >
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/jahir.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Md. Jahir Tarafder</h5>
							<span class="team-position">Senior Developer</span>
							<p style="text-align:justify">Jahir has around 3 years experience in Codeignetor, Node Js, React Js. Fast learner and very smooth coder. </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/subhajit.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Mr. Subhajit Biswas</h5>
							<span class="team-position">Project Manager </span>
							<p  style="text-align:justify">Subhajit has completed 13 years as Team Lead in TCS before joining Magnox. He has experience in .NET and Oracle. He has worked in insurance and reInsurance Project. Subhajit has completed his BTech in Computer Science and Engineering from College of Engineering and Management, Kolaghat </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/hazra.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Mr. Abhinava Hazra</h5>
							<span class="team-position">Senior Engineer </span>
							<p  style="text-align:justify">Abhinava has 4 years experience in Hardware and Software field. He has knowledge and experience in IOT, Robotics, Node Js, React Js and React Native.</p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/sourav.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name"> Mr. Sourab Jana</h5>
							<span class="team-position">Sr. Techncal Engineer</span>
							<p style="text-align:justify">Sourav has more than 4+ years experience in Android and Web Development. He has very good knowledge in Sensor Network and embedded technologies. He is working in Codeignetor, Node Js, React Js and more.</p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/madhabi.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name"> Ms. Madhabi Patra</h5>
							<span class="team-position">Marketing Manager</span>
							<p style="text-align:justify">Madhabi has completed her BTech in Computer Science from College of Engineering and Management Kolaghat and has more than 6 years experience in B2B sales and marketing. </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/sardar.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Mr. Suvayan Sardar</h5>
							<span class="team-position">Software Developer </span>
							<p  style="text-align:justify">Suvayan has 2+ Years experience in PHP MVC and Codeignetor. </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				<div class="col-sm-6 col-md-3">
					<div class="team-item text-center" >
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/neelam.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Ms. Neelam Sinha</h5>
							<span class="team-position">Operations</span>
							<p style="text-align:justify">Neelam has gratuated in Electronics and Instrumentation and is handeling the operation now.  </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
				<!-- Team item -->
				 <div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/saumya.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Mr. Saumya Parui</h5>
							<span class="team-position">Senior Software Developer </span>
							<p  style="text-align:justify">Saumya Parui is a Software Developer and has more than 6 year experience in Software Development like PHP MVC and Node JS.  </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
                <div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/Subham.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Mr. Subham Paul</h5>
							<span class="team-position">Junior Software Developer </span>
							<p  style="text-align:justify">He has done his BTech from IEM Kolkata. He has done internships on Java, Android and Node JS.  </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
                <div class="col-sm-6 col-md-3">
					<div class="team-item text-center">
						<div class="team-avatar">
							<img src="<?php echo base_url()?>assets/images/people/malabika.jpg" alt="" style="border-radius: 50%; height:200px">
						</div>
						<div class="team-desc">
							<h5 class="team-name">Ms. Malabika Bera</h5>
							<span class="team-position">Designer </span>
							<p  style="text-align:justify">She has done BA Hons and done Animation and Design courses. She is specialized in Drawing and Arts. She is into animation, content writing etc.  </p>
							<ul class="social-icons si-colored-on-hover">
								<li class="social-icons-item social-facebook"><a class="social-icons-link" href="#"><i class="fa fa-facebook"></i></a></li>
								<li class="social-icons-item social-instagram"><a class="social-icons-link" href="#"><i class="fa fa-instagram"></i></a></li>
								<li class="social-icons-item social-twitter"><a class="social-icons-link" href="#"><i class="fa fa-twitter"></i></a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	