<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4">About the Company</h2>
				<p>
				<span class="text-info">Our Vision</span> is to create a bridge between the <span class="text-warning">Academics</span> & the <span class="text-danger">Professional</span>.<br>
				We are  <b>Nasscom 10K startup</b> incubated at  <b>STEP IIT Kharagpur</b>, member of <b>Microsoft Bizspark</b>  and funded by <b>Webel Venture Funding</b>.
				</p>
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section id="about" class="about-area pt-100 pb-70">
    <div class="container">
		<div class="row">
		
			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">People</h3>
						Know your skills  - Get better
						Opportunity. Get adequate exposure.
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Professional</h3>
						Get perfect people for the right job, increase your productivity.
					</div>
				</div>
			</div>
			<div class="col-xl-4 col-lg-4">
				<div class="card">
					<div class="card-body">
						<h3 class="card-title">Academic</h3>
						Manage your information, academics, events, alumni & placement.
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<section class="testimonilas-area pt-95 pb-70 gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<div class="section-title mb-50 text-center">
					<div class="section-title-heading mb-20">
						<h1 class="primary-color">Recognitions</h1>
					</div>
					<div class="section-title-para">
						<p class="gray-color">Belis nisl adipiscing sapien sed malesu diame lacus eget erat Cras mollis scelerisqu Nullam arcu liquam here was consequat.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-12">
				<div class="owl-carousel arrow-dark arrow-hover" data-dots="false" data-items-xl="3" data-items-lg="3" data-items-md="2" data-items-sm="2" data-items-xs="1">
					
					<div class="item">
						<div class="post">
							<img src="<?php echo site_url(); ?>assets/images/blog/08.jpg" alt="">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="javascript:;">Funded</a></span>
								
								<a class="post-title" href="javascript:;">Tieup with Vixplor for AI and Machine Learning</a>
								<p class="mb-0">
								We are being selected by WVCL. The Fund has been given by Govt of West Bengal.
								</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="post">
							<img src="<?php echo site_url(); ?>assets/images/blog/08.jpg" alt="">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="javascript:;">Funded</a></span>
								
								<a class="post-title" href="javascript:;">Funded by Webel Venture Capital Limited</a>
								<p class="mb-0">
								We are being selected by WVCL. The Fund has been given by Govt of West Bengal.
								</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="post">
							<img src="<?php echo site_url(); ?>assets/images/blog/01.jpg" alt="">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="javascript:;">Awards</a></span>
								
								<a class="post-title" href="javascript:;">Selected within best 10 Startup by ICC</a>
								<p class="mb-0">
								Our Idea has been selected as the best 10 startups within Eastern and Western India by ICC in their event Startup Pad.
								</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="post">
							<img src="<?php echo site_url(); ?>assets/images/blog/02.jpg" alt="">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="javascript:;">Recognition</a></span>
								
								<a class="post-title" href="javascript:;">Selected for Startup Camp at GES, IIT Kharagpur</a>
								<p class="mb-0">
								Selected at Global Entrepreneurship submit at IIT Kharagpur to present my Idea in front of more than 500 people from all over India.
								</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="post">
							<img src="<?php echo site_url(); ?>assets/images/blog/03.jpg" alt="">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="javascript:;">Team</a></span>
								
								<a class="post-title" href="javascript:;">Dr. Raj Ray joined into our Board of Advisors</a>
								<p class="mb-0">
								Founder of SoS Corp. Done his PhD from The University of Texas & Health Mgmt Course from Harvard University. Wasthe Chief R&D Architect in Sun Microsystems, Senior Engineer at Intel Corp., USA etc.
								</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="post">
							<img src="<?php echo site_url(); ?>assets/images/blog/04.jpg" alt="">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="javascript:;">Recognition</a></span>
								
								<a class="post-title" href="javascript:;">Invited by ICC to American Center</a>
								<p class="mb-0">
								Indian Chamber of Commerce has invited to the American Center to present our idea of BillionSkills.com
								</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="post">
							<img src="<?php echo site_url(); ?>assets/images/blog/9.jpg" alt="">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="javascript:;">Recognition</a></span>
								
								<a class="post-title" href="javascript:;">Selected at Bihar Hackerton</a>
								<p class="mb-0">
								Shortlisted at Bihar Hackerton
								</p>
							</div>
						</div>
					</div>
					<div class="item">
						<div class="post">
							<img src="<?php echo site_url(); ?>assets/images/blog/10.jpg" alt="">
							<div class="post-info">
								<span class="post-tag bg-grad text-white mb-3 clearfix"><a href="javascript:;">Recognition</a></span>
								
								<a class="post-title" href="javascript:;">At London Club with TIE Kolkata</a>
								<p class="mb-0">
								Shortlisted at Bihar Hackerton
								</p>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
		
	</div>
</section>
<section class="testimonilas-area pt-95 pb-70 white-bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<div class="section-title mb-50 text-center">
					<div class="section-title-para">
						<p class="gray-color text-justify">Our Objective is to bring a revolution in the way Learning and Hiring are managed in an organization. We aim to create a more transparent and bias free plaform to create more efficient environment for the employeer and the employees.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section class="team-area pt-95 pb-70 gray-bg">
	<div class="container">
		<div class="row">
			<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<div class="section-title mb-50 text-center">
					<div class="section-title-heading mb-20">
						<h1 class="primary-color">Associations</h1>
					</div>
					<div class="section-title-para">
						<p class="gray-color">We are associating with more organization for growth.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="team-list">
			<div class="row">
				<div class="col-xl-3 col-lg-3 col-md-6">
					<div class="team-wrapper mb-30">
						<div class="team-thumb">
							<img src="<?php echo site_url(); ?>assets/images/association/1.png" alt="">
						</div>
						<div class="team-teacher-info text-center">
							<p>Incubated @</p>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6">
					<div class="team-wrapper mb-30">
						<div class="team-thumb">
							<img src="<?php echo site_url(); ?>assets/images/association/2.png" alt="">
						</div>
						<div class="team-teacher-info text-center">
							<p>Incubated @</p>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6">
					<div class="team-wrapper mb-30">
						<div class="team-thumb">
							<img src="<?php echo site_url(); ?>assets/images/association/3.png" alt="">
						</div>
						<div class="team-teacher-info text-center">
							<p>Member & Funded @</p>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6">
					<div class="team-wrapper mb-30">
						<div class="team-thumb">
							<img src="<?php echo site_url(); ?>assets/images/association/4.png" alt="">
						</div>
						<div class="team-teacher-info text-center">
							<p>Funded @</p>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6">
					<div class="team-wrapper mb-30">
						<div class="team-thumb">
							<img src="<?php echo site_url(); ?>assets/images/association/5.png" alt="">
						</div>
						<div class="team-teacher-info text-center">
							<p>Member @</p>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6">
					<div class="team-wrapper mb-30">
						<div class="team-thumb">
							<img src="<?php echo site_url(); ?>assets/images/association/6.png" alt="">
						</div>
						<div class="team-teacher-info text-center">
							<p>Recognized @</p>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6">
					<div class="team-wrapper mb-30">
						<div class="team-thumb">
							<img src="<?php echo site_url(); ?>assets/images/association/7.png" alt="">
						</div>
						<div class="team-teacher-info text-center">
							<p>Recognized @</p>
						</div>
					</div>
				</div>
				<div class="col-xl-3 col-lg-3 col-md-6">
					<div class="team-wrapper mb-30">
						<div class="team-thumb">
							<img src="<?php echo site_url(); ?>assets/images/association/8.png" alt="">
						</div>
						<div class="team-teacher-info text-center">
							<p>Recognized @</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>