<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class="display-4">Blogs</h2>
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="blog-page pb-0">
	<div class="container">
		<div class="row">
			<div class="col-md-12 blog-grid blog-grid-3 portfolio-wrap" data-isotope='{ "itemSelector": ".post-item", "layoutMode": "fitRows" }'>
			<?php
				if(!empty($blogs)){
					foreach($blogs as $blg){
						$bg_image = trim($blg->banner);
						if($bg_image!=""){
							$remoteFile = 'https://learn.techmagnox.com'.$bg_image;
							$handle = @fopen($remoteFile, 'r');
							if($handle){
								$csrc = 'https://learn.techmagnox.com'.$bg_image;
							}else{
								$csrc = base_url('assets/images/blog/grid/03.jpg');
							}
						}else{
							$csrc = base_url('assets/images/blog/grid/03.jpg');
						}
						echo '<div class="post-item">
							<div class="post-item-wrap">
								<div class="post-image">
									<a href="#"> <img src="'.$csrc.'" alt=""> </a>
								</div>
								<div class="post-item-desc">
									<h4><a href="'.base_url('home/blog_details/'.$blg->id).'">'.trim($blg->title).'</a></h4>
									<p>'.substr(strip_tags(trim($blg->body)),0, 100).' ...</p>
									<a href="'.base_url('home/blog_details/'.$blg->id).'" class="item-link">Continue reading<i class="ti-minus"></i></a>
								</div>
							</div>
						</div>';
					}
				}
			?>
			
			</div>
		</div>
	</div>
</section>
<script src="<?= base_url(); ?>assets/vendor/fitvids/jquery.fitvids.js"></script>
<script src="<?= base_url(); ?>assets/vendor/isotope/isotope.pkgd.min.js"></script>