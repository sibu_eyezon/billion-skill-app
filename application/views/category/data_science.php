<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4">Data Science</h5>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="bg-light">

	<div class="container ">
		<div class="row">
			<div class="col-sm-8 col-md-8">
		<p style="text-align:justify">Data Science is the process of extraction of Actionable Insights from vast volumes of Raw Data i.e. both structured and unstructured. 
        <br>
        It is a concept to unify statistics, data analysis, informatics, and their related methods in order to understand and analyze actual phenomena" with data. It uses techniques, methods, processes, algorithms,systems and theories drawn from many fields within the context of mathematics, statistics, computer science, information science, and domain knowledge. It is related to data mining, machine learning and big data.   <br>
       <b>About the Program </b>: The online course on data science  is a one-stop solution for all your data science learning needs. It offers you complete guidance and tutorial with experience teachers, downloadable resources, hands-on learning experience and job/ internship consultancy. 
        
        
      </p><a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Download Brochure</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Request for Program</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Send Feedback</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Instructors List</a>
       </div>
        <div class="col-sm-4 col-md-4" style="text-align:right">
        <iframe width="400" height="240" src="https://www.youtube.com/embed/-ETQ97mXXF0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br> <br>
       </div>
	</div>
    </div></section>
    <section class="bg-white"><div class="container ">
    <div class="row">
				<div class="col-12 col-lg-12 mx-auto">
					
						<br> 
						<h5 class=" display-7 text-left">Why Data Science Program ?</h5>
					
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Booming Job Market</h6>
						<p class="feature-box-desc">Residence certainly elsewhere something she preferred cordially law. Age his surprise formerly Mrs perceive few moderate. Of in <strong> power match on</strong> truth worse would an match learn. </p>
						<a class="mt-3" href="#">Know more!</a>
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Excellent Instructors</h6>
						<p class="feature-box-desc">Consulted perpetual of pronounce me delivered. Too months nay end change relied <abbr title="attribute">who beauty</abbr> wishes matter. Shew of john real park so rest we on. Ignorant occasion for thoughts</p>
						<a class="mt-3" href="#">Know more!</a>
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Post Program Facilities </h6>
						<p class="feature-box-desc">Why end might ask civil again spoil. Dinner she our horses depend remember at children by reserved to vicinity. Oh song well four only head busy it. In affronting delightful simplicity own.</p>
						<a class="mt-3" href="#">Know more!</a>
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Complete Knowledge</h6>
						<p class="feature-box-desc">Residence certainly elsewhere something she preferred cordially law. Age his surprise formerly Mrs perceive few moderate. Of in <strong> power match on</strong> truth worse would an match learn. </p>
						<a class="mt-3" href="#">Know more!</a>
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Excellent Instructors</h6>
						<p class="feature-box-desc">Consulted perpetual of pronounce me delivered. Too months nay end change relied <abbr title="attribute">who beauty</abbr> wishes matter. Shew of john real park so rest we on. Ignorant occasion for thoughts</p>
						<a class="mt-3" href="#">Know more!</a>
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Post Program Facilities </h6>
						<p class="feature-box-desc">Why end might ask civil again spoil. Dinner she our horses depend remember at children by reserved to vicinity. Oh song well four only head busy it. In affronting delightful simplicity own.</p>
						<a class="mt-3" href="#">Know more!</a>
					</div>
				</div>
			</div>
    </div>
</section>
<section class="bg-light">

	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-md-8">
          <br> 
						<h5 class=" display-7 text-left">The instructor led Certification Training Program</h5>
            		</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 1: Python</h4>
							<p>Python is the most popular language used in Data Science. In this section, our instructors will take you through the basics of Python and areas where it can be used. You will learn how to use some of the popular  tools for Data Analysis such as Numpy, Pandas, and Matplotlib. So Module 1 includes the following :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Environment set-up</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Basic Python Coding</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Array, Functions, File Input</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Jupyter overview</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Python Numpy</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Python Pandas</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Python Matplotlib</a>
								
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- timeline item 2 -->
			<div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 2: Statistics</h4>
							<p>When working with data, the knowledge of statistics is necessary and an important skill set that you must have. In this module, you will learn  :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Basic About Statitics</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> statistical concepts used in data science</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Difference between population and sample</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Types of variables</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Measures of central tendency</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Measures of variability</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Coefficient of variance</a>
								<a href="#" class="list-group-item list-group-item-action"><span>08</span> kewness and Kurtosis</a>
                                
							</div>
						</div>
					</div>
				</div>
			</div>

             <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 3: Inferential statistics</h4>
							<p>Inferential statistics allows you to make predictions ("inferences") from that data. With inferential statistics, you take data from samples and make generalizations about a population :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Normal distribution</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Test hypotheses</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Central limit theorem</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Confidence interval</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> T-test</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Type I and II errors</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Student's T distribution</a>
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 4: Machine Learning Skills</h4>
							<p>Machine learning is a key component of any Data Science syllabus. It involves mathematics and algorithm models to help students understand how a machine learns and adapts to everyday changes :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Fundamental statistical concepts</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Statistical analysis</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Modeling methods</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Logistic Regression</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Supervised machine learning</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Unsupervised machine learning</a>
								
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Clustering Assignment (Optional)</a>
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 5: Text Mining and NLP</h4>
							<p>Text Mining or Text Analytics uses Natural Language Processing (NLP) to convert unstructured texts in the database and documents into normal and structured data that can be analyzed or used to drive machine learning algorithms. Concepts covered in this subject area:</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Handling unstructured text data</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Tokenization and vectorization of text data</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Natural Language Processing</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Supervised & unsupervised text classification</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Supervised machine learning</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Unsupervised machine learning</a>
								
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Sentiment analysis of social media data</a>
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            
			<!-- timeline item 3 -->
			
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-sm"></div>
			</div>
    </div>
</section>