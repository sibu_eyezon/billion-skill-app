<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4">Internet of Things (IOT)</h5>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="bg-light">

	<div class="container ">
		<div class="row">
			<div class="col-sm-8 col-md-8">
		<p style="text-align:justify">The internet of things, or IoT, is a system of interrelated computing devices, mechanical and digital machines, objects, animals or people that are provided with unique identifiers (UIDs) and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction. . 
        <br>
                                     The Internet of Things (IoT) describes the network of physical objects—"things"—that are embedded with sensors, software, and other technologies for the purpose of connecting and exchanging data with other devices and systems over the internet.   <br> 
        
       <b>About the Program </b>: Robotics and IOT are the most Cutting Edge Technologies that is available in the Market. Here we shall teach hands-on on the devices and teach you how to create small projects.   
        
        
      </p><a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Download Brochure</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Request for Program</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Send Feedback</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Instructors List</a>
       </div>
        <div class="col-sm-4 col-md-4" style="text-align:right">
        <iframe width="400" height="240" src="https://www.youtube.com/embed/h0gWfVCSGQQ" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br> <br>
       </div>
	</div>
    </div></section>
    <section class="bg-white"><div class="container ">
    <div class="row">
				<div class="col-12 col-lg-12 mx-auto">
					
						<br> 
						<h5 class=" display-7 text-left">Why Internet of Things (IOT) ?</h5>
					
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Cutting Edge Technologies</h6>
						<p class="feature-box-desc">IOT and Robotics are the latest trends of todays market. It has a trillion dollar market value and lot of good jobs are coming in this technology. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title"> Better Jobs</h6>
						<p class="feature-box-desc"> If you are a fan of Computer Scince then AI and ML are the most interesting subjects and the most popular topic nowadays. You can solve amazing real life problems which was impossible without it.</p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Excellent Instructors</h6>
						<p class="feature-box-desc">Our Faculty Members are from Industries, Retired Professor or Alumni from Eminent Institutes like IIT Kharagpur. So we have excellent hands and instructors join us to explore your career.</p>
						
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Most Interesting  </h6>
						<p class="feature-box-desc">It describes a wide range of technologies that reduce human intervention in processes. Human intervention is reduced by predetermining decision criteria, subprocess relationships, related actions. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Live Projects</h6>
						<p class="feature-box-desc">We are about to collaborate with eminent institutes for this program to get better guidance, syllabus and repositories so that you can learn the best. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Easy to Learn </h6>
						<p class="feature-box-desc">Our Techincal Team is busy in creating the contents which will be easy for people to understand.  </p>
						
					</div>
				</div>
			</div>
    </div>
</section>
<section class="bg-light">

	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-md-8">
          <br> 
						<h5 class=" display-7 text-left">The instructor led Certification Training Program</h5>
            		</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 1: Introduction to IoT</h4>
							<p>It is a system of interrelated computing devices, mechanical and digital machines, objects, animals or people that are provided with unique identifiers and the ability to transfer data over a network without requiring human-to-human or human-to-computer interaction.. So Module 1 includes the following :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Defining IoT</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Characteristics of IoT</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Physical design of IoT</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Logical design of IoT</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Functional  blocks  of  IoT </a>
							    <a href="#" class="list-group-item list-group-item-action"><span>06</span> Different Components and their use </a>
								 <a href="#" class="list-group-item list-group-item-action"><span>07</span> Communication  models  & APIs</a>
                                  <a href="#" class="list-group-item list-group-item-action"><span>08</span> Tasks</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- timeline item 2 -->
			<div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 2: Design and Development</h4>
							<p>This is the most important course of this program. Details of the Course is given below  :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Design Methodology </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span>Embedded computing logic </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Microcontroller</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Microcontroller</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> IoT system building blocks</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Arduino</a>
                              
                              <a href="#" class="list-group-item list-group-item-action"><span>07</span> Tutorial</a>
                               <a href="#" class="list-group-item list-group-item-action"><span>08</span> Project</a>
                                
							</div>
						</div>
					</div>
				</div>
			</div>

             <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 3: Raspberry Pi</h4>
							<p>Raspberry Pi is a series of small single-board computers developed in the United Kingdom by the Raspberry Pi Foundation in association with Broadcom. The Details of Raspberry Pi will be taught here   :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Introduction to Raspberry Pi</a>
                                  <a href="#" class="list-group-item list-group-item-action"><span>06</span> Booting Up RPi- Operating System and Linux Commands </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span>Working with RPi using Python and Sensing Data using Python</a>
                              
                              
								<a href="#" class="list-group-item list-group-item-action"><span>07</span>Tutorial</a>
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            
            
            
			<!-- timeline item 3 -->
			
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-sm"></div>
			</div>
    </div>
</section>