<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4">PHP MVC with MYSQL</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="bg-light">

	<div class="container ">
		<div class="row">
			<div class="col-sm-8 col-md-8">
		<p style="text-align:justify">PHP MVC is an application design pattern that separates the application data and business logic (model) from the presentation (view). MVC stands for Model, View & Controller. The controller mediates between the models and views. 
        <br>
        The main aim of MVC Architecture is to separate the Business logic & Application data from the USER interface.     <br> 
        MySQL, the most popular Open Source SQL database management system, is developed, distributed, and supported by Oracle Corporation. <br />
       <b>About the Program </b>: PHP MVC with MYSQL is the most popular combination. It is very useful for small and medium application. Since the coding is very easy it is widely used and around 80% programing jobs in this world are made from PHP MYSQL. Around 80% application in this World are made out of this technology. You can make web application mostly from PHP.   
        
        
      </p><a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Download Brochure</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Request for Program</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Send Feedback</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Instructors List</a>
       </div>
        <div class="col-sm-4 col-md-4" style="text-align:right">
        <iframe width="400" height="240" src="https://www.youtube.com/embed/mpQts3ezPVg" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br> <br>
       </div>
	</div>
    </div></section>
    <section class="bg-white"><div class="container ">
    <div class="row">
				<div class="col-12 col-lg-12 mx-auto">
					
						<br> 
						<h5 class=" display-7 text-left">Why PHP MYSQL ?</h5>
					
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Most Popular Web Technology</h6>
						<p class="feature-box-desc">Artificial Intellgence and Machine Learning has hit job market like never. Around 5 Lakhs jobs are there only in Indian Market as per data. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title"> Easy Job Market</h6>
						<p class="feature-box-desc"> If you are a fan of Computer Scince then AI and ML are the most interesting subjects and the most popular topic nowadays. You can solve amazing real life problems which was impossible without it.</p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Excellent Instructors</h6>
						<p class="feature-box-desc">Our Faculty Members are from Industries, Retired Professor or Alumni from Eminent Institutes like IIT Kharagpur. So we have excellent hands and instructors join us to explore your career.</p>
						
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Best Choice for Easy Projects</h6>
						<p class="feature-box-desc">Residence certainly elsewhere something she preferred cordially law. Age his surprise formerly Mrs perceive few moderate. Of in <strong> power match on</strong> truth worse would an match learn. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Live Projects</h6>
						<p class="feature-box-desc">We are about to collaborate with eminent institutes for this program to get better guidance, syllabus and repositories so that you can learn the best. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Easy to Learn </h6>
						<p class="feature-box-desc">Our Techincal Team is busy in creating the contents which will be easy for people to understand.  </p>
						
					</div>
				</div>
			</div>
    </div>
</section>
<section class="bg-light">

	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-md-8">
          <br> 
						<h5 class=" display-7 text-left">The instructor led Certification Training Program</h5>
            		</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 1: HTML</h4>
							<p>The HyperText Markup Language, or HTML is the standard markup language for documents designed to be displayed in a web browser. It can be assisted by technologies such as Cascading Style Sheets and scripting languages such as JavaScript. So Module 1 includes the following :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Basics, Elements, Attributes</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Style, Formating</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Colors, Images, Tables, Lists etc</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Classes, Java Script etc</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Forms and API </a>
							    <a href="#" class="list-group-item list-group-item-action"><span>06</span> References </a>
								 <a href="#" class="list-group-item list-group-item-action"><span>07</span> Tutorial </a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- timeline item 2 -->
			<div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 2: CSS</h4>
							<p>Cascading Style Sheets is a style sheet language used for describing the presentation of a document written in a markup language such as HTML :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Cost Function</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Intuition I and Intuition II</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Difference between population and sample</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Gradient Descent</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Gradient Descent For Linear Regression</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Practice exercise</a>
                              <a href="#" class="list-group-item list-group-item-action"><span>07</span> Tutorial</a>
                                
							</div>
						</div>
					</div>
				</div>
			</div>

             <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 3: PHP </h4>
							<p>The PHP Hypertext Preprocessor (PHP) is a programming language that allows web developers to create dynamic content that interacts with databases. PHP is basically used for developing web based software applications  :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>  Environment Setup</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Syntax, Variables, Constants</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Loop, Array, String</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Files</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Session and Cookies</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Email, File Upload</a>
                              
								<a href="#" class="list-group-item list-group-item-action"><span>07</span>Tutorial</a>
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 4: Advanced PHP</h4>
							<p>PHP is a MUST for students and working professionals to become a great Software Engineer specially when they are working in Web Development Domain :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Pre Defined Variables</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Regular Expression</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Date and Time</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> AJAX</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span>OOPs</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span>Tutorial</a>
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 5: CodeIgniter</h4>
							<p>CodeIgniter is a powerful PHP framework with a very small footprint, built for developers who need a simple and elegant toolkit to create full-featured web applications. CodeIgniter was created by EllisLab, and is now a project of the British Columbia Institute of Technology. Concepts covered in this subject area are as follows:</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Overview and Installing</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Application Architecture and MVC Framework </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Backpropagation Intuition</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Implementation Note: Unrolling Parameters</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Concepts and Configuration</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Working with Database</a>
								
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Error Handeling </a>
								<a href="#" class="list-group-item list-group-item-action"><span>08</span> File Uploading </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>09</span> Tutorial </a>
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 6: MYSQL</h4>
							<p>MySQL is a freely available open source Relational Database Management System (RDBMS) that uses Structured Query Language (SQL). SQL is the most popular language for adding, accessing and managing content in a database. Concepts covered in this subject area are as follows:</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Getting Started with MySQL</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> SQL Queries  </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Joins</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Triggers and Stored Procedures</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Tutorial</a>
                                
							</div>
						</div>
					</div>
				</div>
			</div>
             <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 7: More CodeIgniter</h4>
							<p>More Details about CodeIgniter. Concepts covered in this subject area are as follows:</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Sending Email</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Form Validation </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Session Management</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Flashdata and Dumpdata</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Cookie Management</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Common Features</a>
								
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Page Caching and Redirection</a>
								 <a href="#" class="list-group-item list-group-item-action"><span>08</span> Application and Profiling</a>
								
                                <a href="#" class="list-group-item list-group-item-action"><span>09</span> Benchmarking</a>
                                 <a href="#" class="list-group-item list-group-item-action"><span>10</span> Adding JS and CSS </a>
                                 <a href="#" class="list-group-item list-group-item-action"><span>11</span> Internationalization and Security </a>
                                  <a href="#" class="list-group-item list-group-item-action"><span>12</span> Tutorial </a>
                                   <a href="#" class="list-group-item list-group-item-action"><span>13</span> Project </a>
							</div>
						</div>
					</div>
				</div>
			</div>
            
            
			<!-- timeline item 3 -->
			
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-sm"></div>
			</div>
    </div>
</section>