<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4">Machine Learning</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<!-- =======================
Banner innerpage -->
<section class="bg-light">

	<div class="container ">
		<div class="row">
			<div class="col-sm-8 col-md-8">
		<p style="text-align:justify">Machine learning is a method of data analysis that automates analytical model building through experience and by the use of data.
        <br>
        It is a branch of artificial intelligence based on the idea that systems can learn from data, identify patterns and make decisions with minimal human intervention. Machine learning algorithms are used in a wide variety of applications, such as in medicine, email filtering, and computer vision, where it is difficult or unfeasible to develop conventional algorithms to perform the needed tasks.    <br>
       <b>About the Program </b>: The online course on Machine Learning can make you Ariticial Intelligence expert, since AI is the most upcoming Trend in market and a topic on Advance Computer Science you will learn Computer Science and also can boost your career. It offers you complete guidance and tutorial with experience teachers, downloadable resources, hands-on learning experience and job/ internship consultancy. 
        
        
      </p><a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Download Brochure</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Request for Program</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Send Feedback</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Instructors List</a>
       </div>
        <div class="col-sm-4 col-md-4" style="text-align:right">
        <iframe width="400" height="240" src="https://www.youtube.com/embed/ukzFI9rgwfU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br> <br>
       </div>
	</div>
    </div></section>
    <section class="bg-white"><div class="container ">
    <div class="row">
				<div class="col-12 col-lg-12 mx-auto">
					
						<br> 
						<h5 class=" display-7 text-left">Why Data Science Program ?</h5>
					
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Excellent Job Market</h6>
						<p class="feature-box-desc">Artificial Intellgence and Machine Learning has hit job market like never. Around 5 Lakhs jobs are there only in Indian Market as per data. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title"> Most Interesting Topic</h6>
						<p class="feature-box-desc"> If you are a fan of Computer Scince then AI and ML are the most interesting subjects and the most popular topic nowadays. You can solve amazing real life problems which was impossible without it.</p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Excellent Instructors </h6>
						<p class="feature-box-desc">Our Faculty Members are from Industries, Retired Professor or Alumni from Eminent Institutes like IIT Kharagpur. So we have excellent hands and instructors join us to explore your career.</p>
						
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Post Program Facilities</h6>
						<p class="feature-box-desc">Residence certainly elsewhere something she preferred cordially law. Age his surprise formerly Mrs perceive few moderate. Of in <strong> power match on</strong> truth worse would an match learn. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Collaborations</h6>
						<p class="feature-box-desc">We are about to collaborate with eminent institutes for this program to get better guidance, syllabus and repositories so that you can learn the best. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Repositories </h6>
						<p class="feature-box-desc">Our Techincal Team is busy in creating the contents which will be easy for people to understand.  </p>
						
					</div>
				</div>
			</div>
    </div>
</section>
<section class="bg-light">

	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-md-8">
          <br> 
						<h5 class=" display-7 text-left">The instructor led Certification Training Program</h5>
            		</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 1: Introduction</h4>
							<p>Welcome to Machine Learning! In this module, we introduce the core idea of teaching a computer to learn concepts using data. So Module 1 includes the following :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Welcome to Machine Learning</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Machine Learning Honor Code</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Supervised Learning</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Unsupervised Learning</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Practice exercise</a>
							
								
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- timeline item 2 -->
			<div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 2: Linear Regression with One Variable</h4>
							<p>Linear regression predicts a real-valued output based on an input value  :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Cost Function</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Intuition I and Intuition II</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Difference between population and sample</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Gradient Descent</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Gradient Descent For Linear Regression</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Practice exercise</a>
                              
                                
							</div>
						</div>
					</div>
				</div>
			</div>

             <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 3: Linear Algebra Review</h4>
							<p>This optional module provides a refresher on linear algebra concepts. Basic understanding of linear algebra is necessary for the rest of the course. Gothrough the sub divisions  :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Matrices and Vectors</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Addition and Scalar Multiplication</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Matrix Vector Multiplication</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Matrix Multiplication Properties</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Inverse and Transpose</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Practice Exercise</a>
                              
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 4: Linear Regression with Multiple Variables</h4>
							<p>What if your input has more than one value? In this module, we show how linear regression can be extended to accommodate multiple input features :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Multiple Features</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Gradient Descent for Multiple Variables</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Gradient Descent in Practice I and Practice II</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Features and Polynomial Regression</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Normal Equation</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Normal Equation </a>
								
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Practice Exercise</a>
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 5: Neural Networks</h4>
							<p>Neural networks is a model inspired by how the brain works. It is widely used today in many applications: when your phone interprets. we introduce the backpropagation algorithm that is used to help learn parameters for a neural network. At the end of this module, you will be implementing your own neural network for digit recognition. Concepts covered in this subject area are as follows:</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Cost Function</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Backpropagation Algorithm </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Backpropagation Intuition</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Implementation Note: Unrolling Parameters</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Gradient Checking</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Random Initialization</a>
								
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Practice Exercise</a>
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
             <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 5: Machine Learning System Design</h4>
							<p>To optimize a machine learning algorithm, you'll need to first understand where the biggest improvements can be made. In this module, we discuss how to understand the performance of a machine learning system with multiple parts, and also how to deal with skewed data. Concepts covered in this subject area are as follows:</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Cost Function</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Backpropagation Algorithm </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Backpropagation Intuition</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Implementation Note: Unrolling Parameters</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Gradient Checking</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Random Initialization</a>
								
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Practice Exercise</a>
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
             <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 5: Unsupervised Learning</h4>
							<p>We use unsupervised learning to build models that help us understand our data better. Concepts covered in this subject area are as follows:</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Introduction</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> K-Means Algorithm </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Optimization Objective</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Implementation Note: Unrolling Parameters</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Random Initialization</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Choosing the Number of Clusters</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span>  Practice Exercise</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span>  Project</a>
								
                             
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            
			<!-- timeline item 3 -->
			
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-sm"></div>
			</div>
    </div>
</section>