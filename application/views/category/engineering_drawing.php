<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4">Engineering Drawing</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="bg-light">

	<div class="container ">
		<div class="row">
			<div class="col-sm-8 col-md-8">
		<p style="text-align:justify">The sole purpose of communication is to convey information. We come straight to conveying engineering information. It is the information which let one know about various aspects of an engineering product, i.e. shape, dimensions, material, etc. The final goal is to achieve the finished product – a bicycle, a hand pump, a ship, a simple nut and bolt joint. Naturally to get to the goal, one will start with product conceptualisation and proceed towards generation of relevant design information, design drawings, production drawings, etc. The design and production drawings are nothing but conveying specific information about the engineering product – the shape and its dimensions. The entire communication is through the drawings.  <br> 
        
       <b>About the Program </b>: The course has been planned in two Modules - 8 weeks module and 16 weeks module. This Course will be conducted by <b>Prof N R Mandal, Ex- Professor and Dean, IIT Kharagpur</b>. In his own words "Engineering drawing is referred to as language of Engineers. The Design Engineer talks with the Production Engineer through this language. If we aspire to be good Engineers, along with other knowledge, we must also master this language: The language of Engineers – Engineering Drawing "   
        
        
      </p><a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Download Brochure</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Request for Program</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Send Feedback</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Instructors List</a>
       </div>
        <div class="col-sm-4 col-md-4" style="text-align:right">
        <iframe width="400" height="240" src="https://www.youtube.com/embed/cmR9cfWJRUU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br> <br>
       </div>
	</div>
    </div></section>
    <section class="bg-white"><div class="container ">
    <div class="row">
				<div class="col-12 col-lg-12 mx-auto">
					
						<br> 
						<h5 class=" display-7 text-left">Why Engineering Drawing ?</h5>
					
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Cutting Edge Technologies</h6>
						<p class="feature-box-desc">IOT and Robotics are the latest trends of todays market. It has a trillion dollar market value and lot of good jobs are coming in this technology. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title"> Better Jobs</h6>
						<p class="feature-box-desc"> If you are a fan of Computer Scince then AI and ML are the most interesting subjects and the most popular topic nowadays. You can solve amazing real life problems which was impossible without it.</p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Excellent Instructors</h6>
						<p class="feature-box-desc">Our Faculty Members are from Industries, Retired Professor or Alumni from Eminent Institutes like IIT Kharagpur. So we have excellent hands and instructors join us to explore your career.</p>
						
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Most Interesting  </h6>
						<p class="feature-box-desc">It describes a wide range of technologies that reduce human intervention in processes. Human intervention is reduced by predetermining decision criteria, subprocess relationships, related actions. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Live Projects</h6>
						<p class="feature-box-desc">We are about to collaborate with eminent institutes for this program to get better guidance, syllabus and repositories so that you can learn the best. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Easy to Learn </h6>
						<p class="feature-box-desc">Our Techincal Team is busy in creating the contents which will be easy for people to understand.  </p>
						
					</div>
				</div>
			</div>
    </div>
</section>
<section class="bg-light">

	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-md-8">
          <br> 
						<h5 class=" display-7 text-left">The instructor led Certification Training Program</h5>
            		</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 1: 8 Weeks Module</h4>
							<p>Duration: 8 weeks; Contact Hours:6 hrs per week, Home assignments: Participants will be given home assignments on alternate weeks. The following topics will be covered during the said duration of 8 weeks. Introduction, Line Types, Shapes; Isometric Drawing; Orthogonal Projections; Sectional Projections; Dimensioning.</p>
							
						</div>
					</div>
				</div>
			</div>

			<!-- timeline item 2 -->
			<div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 2: 16 Weeks Module</h4>
							<p>Duration – 16 weeks; Contact Hours – Total 90 hrs; Home assignments – Participants will be given home assignments on alternate weeks. The following topics will be covered during the said duration of 16 weeks. Introduction - Line Types, Shapes; Isometric Drawing; Orthogonal Projections - 1st angle projection, 3rd angle projection; Sectional Projections; Dimensioning -  Linear dimension, Angular dimension, Leader; True Shape; Intersection of Two Members; Development Of Surfaces.</p>
							
						</div>
					</div>
				</div>
			</div>

            
            
            
			<!-- timeline item 3 -->
			
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-sm"></div>
			</div>
    </div>
</section>