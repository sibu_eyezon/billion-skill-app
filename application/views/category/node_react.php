<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h5 class=" display-4"> Node and React JS</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section class="bg-light">

	<div class="container ">
		<div class="row">
			<div class="col-sm-8 col-md-8">
		<p style="text-align:justify">Node.js is an free, open-source server environment. It is a cross-platform which allows you to run JavaScript on the server. It runs on the V8 engine and executes JavaScript code outside a web browser. Node.js uses asynchronous programming. . 
        <br>
                                      React.js is an open-source front-end JavaScript library for building user interfaces or UI components, it is the base in the development of single-page or mobile applications.   <br> 
        MySQL, the most popular Open Source SQL database management system, is developed, distributed, and supported by Oracle Corporation. <br />
       <b>About the Program </b>: Both Nodejs and React are javascript languages that can be executed both client and server-side. Developers can execute the Reactjs code directly in the Nodejs environment. The React DOM has components specifically designed to work with Nodejs that reduce lines of code, making server-side rendering comparatively easy.   
        
        
      </p><a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Download Brochure</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Request for Program</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Send Feedback</a>
      <a href="https://learn.techmagnox.com/" target="_blank" class="btn btn-sm btn-grad text-white ml-3 mb-0">Instructors List</a>
       </div>
        <div class="col-sm-4 col-md-4" style="text-align:right">
        <iframe width="400" height="240" src="https://www.youtube.com/embed/JnvKXcSI7yk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><br> <br>
       </div>
	</div>
    </div></section>
    <section class="bg-white"><div class="container ">
    <div class="row">
				<div class="col-12 col-lg-12 mx-auto">
					
						<br> 
						<h5 class=" display-7 text-left">Why NODE JS, REACT JS ?</h5>
					
					
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Advance Technology</h6>
						<p class="feature-box-desc">Artificial Intellgence and Machine Learning has hit job market like never. Around 5 Lakhs jobs are there only in Indian Market as per data. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title"> High Paid Jobs</h6>
						<p class="feature-box-desc"> If you are a fan of Computer Scince then AI and ML are the most interesting subjects and the most popular topic nowadays. You can solve amazing real life problems which was impossible without it.</p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Excellent Instructors</h6>
						<p class="feature-box-desc">Our Faculty Members are from Industries, Retired Professor or Alumni from Eminent Institutes like IIT Kharagpur. So we have excellent hands and instructors join us to explore your career.</p>
						
					</div>
				</div>
			</div>
            <div class="row">
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Javascript </h6>
						<p class="feature-box-desc">Residence certainly elsewhere something she preferred cordially law. Age his surprise formerly Mrs perceive few moderate. Of in <strong> power match on</strong> truth worse would an match learn. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
						
						<h6 class="feature-box-title">Live Projects</h6>
						<p class="feature-box-desc">We are about to collaborate with eminent institutes for this program to get better guidance, syllabus and repositories so that you can learn the best. </p>
						
					</div>
				</div>
				<div class="col-md-4 mt-30">
					<div class="feature-box f-style-2 icon-grad h-100">
					
						<h6 class="feature-box-title">Easy to Learn </h6>
						<p class="feature-box-desc">Our Techincal Team is busy in creating the contents which will be easy for people to understand.  </p>
						
					</div>
				</div>
			</div>
    </div>
</section>
<section class="bg-light">

	<div class="container">
		<div class="row">
			<div class="col-sm-8 col-md-8">
          <br> 
						<h5 class=" display-7 text-left">The instructor led Certification Training Program</h5>
            		</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 1: ReactJS</h4>
							<p>ReactJS used for handling the view layer for web and mobile apps. ReactJS allows us to create reusable UI components. It is currently one of the most popular JavaScript libraries and has a strong foundation and large community behind it. So Module 1 includes the following :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Overview</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Environment Setup</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> JSX and Components</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> State</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Props Overview and Validation </a>
							    <a href="#" class="list-group-item list-group-item-action"><span>06</span> Component API  </a>
								 <a href="#" class="list-group-item list-group-item-action"><span>07</span> Component Life Cycle</a>
                                  <a href="#" class="list-group-item list-group-item-action"><span>08</span> Tutorial</a>
							</div>
						</div>
					</div>
				</div>
			</div>

			<!-- timeline item 2 -->
			<div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 2: More ReactJS </h4>
							<p>ReactJS used for handling the view layer for web and mobile apps. In the last Module we discussed till Component, now we shall proceed more  :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Forms </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Events</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Refs and Keys</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> Router</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Flux</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Animations</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>06</span> Higher Order Components</a>
                              <a href="#" class="list-group-item list-group-item-action"><span>07</span> Tutorial</a>
                               <a href="#" class="list-group-item list-group-item-action"><span>08</span> Project</a>
                                
							</div>
						</div>
					</div>
				</div>
			</div>

             <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 3: Node Js </h4>
							<p>Node.js is a very powerful JavaScript-based framework/platform built on Google Chrome's JavaScript  V8  Engine.  It  is  used  to developI/O  intensive  web  applications  like  video streaming  sites,  single-page  applications,and  other  web  applications.   :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span> Introduction</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Environment Setup</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Creating Application</a>
								<a href="#" class="list-group-item list-group-item-action"><span>04</span> REPL Terminal</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> NPM</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span>Callback Concept</a>
                              
								<a href="#" class="list-group-item list-group-item-action"><span>07</span>Tutorial</a>
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 4: Node Js Part II</h4>
							<p>In the Earlier module we have covered the Node Js module till the Callback Concept. Now we shall start the other important parts of Node Js :</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Event Loop</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Event Emitter</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Buffers</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> Streams</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span>File System</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span>Global Objects</a>
								
                                
							</div>
						</div>
					</div>
				</div>
			</div>
            <div class="row no-gutters">
				<div class="timeline-top"></div>
				
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-md-8 py-2">
					<div class="card">
						<div class="card-body">
							<div class="float-right small">Lec : 20 Hrs, Tut : 30 Hrs</div>
							<h4 class="mb-2">Module 5: Node Js Part III</h4>
							<p>After Module 2, we shall move to the most advance concepts of Node Js. Concepts covered in this subject area are as follows:</p>
							<div class="list-group-number list-unstyled list-group-borderless">
								<a href="#" class="list-group-item list-group-item-action"><span>01</span>Utility Modules</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>02</span> Web Module </a>
                                <a href="#" class="list-group-item list-group-item-action"><span>03</span> Express Framework</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>04</span> RESTful API</a>
								<a href="#" class="list-group-item list-group-item-action"><span>05</span> Scaling an Application</a>
								<a href="#" class="list-group-item list-group-item-action"><span>06</span> Packaging</a>
                                <a href="#" class="list-group-item list-group-item-action"><span>07</span> Tutorial </a>
                                 <a href="#" class="list-group-item list-group-item-action"><span>08</span>  Project </a>
							</div>
						</div>
					</div>
				</div>
			</div>
            
            
			<!-- timeline item 3 -->
			
				<div class="col-md-1 text-center timeline-line flex-column d-none d-md-flex">
					<div class="timeline-dot"></div>
				</div>
				<div class="col-sm"></div>
			</div>
    </div>
</section>