<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4">Corporate Training</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section id="corporate" class="about-area pt-100 pb-70 bg-white" style="background-image:<?php echo base_url('assets/images/blog/22.jpg'); ?>'">
	<div class="container">
	
		<div class="row justify-content-center">
			
			<div class="col-md-10 mb-3 mx-auto">
				<div class="card shadow">
					<div class="card-body">
						<h3 class="card-title" style="line-height:40px; color:#099"><b>Skill your workforce in new age technologies <button type="button" class="btn btn-grad mb-0 mr-3 pull-right">Lets Talk</button> </b> </h3>
                    
						<div class="row">
							<div class="col-md-4">
								<div class="feature-box h-100 icon-primary">
									<div class="feature-box-icon"><i class="ti-settings"></i></div>
									<h5>Customized Curriculum</h5>
									<p class="feature-box-desc">Designed by industry experts and Senior Professors for your need to achieve your business goals & meet team's skills gaps.</strong> </p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="feature-box h-100 icon-primary">
									<div class="feature-box-icon"><i class="ti-support"></i></div>
									<h5>Blended Learning</h5>
									<p class="feature-box-desc">Most effective Learning method with maximum input in less time. High class videos, efficient Trainees, dedicated support and Hands-on.</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="feature-box h-100 icon-primary">
									<div class="feature-box-icon"><i class="ti-support"></i></div>
									<h5>Top Class Instructors & Advisors</h5>
									<p class="feature-box-desc">Industry Experts and Faculty Members with experience more than atleast 12-30 years of experience in the field will be there.</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="feature-box h-100 icon-primary">
									<div class="feature-box-icon"><i class="ti-bolt"></i></div>
									<h5>Dedicated Support</h5>
									<p class="feature-box-desc">We shall engage dedicated Manager to support your queries for higher completion rates. </p>
								</div>
							</div>
							<!-- feature 3 -->
							<div class="col-md-4">
								<div class="feature-box h-100 icon-primary">
									<div class="feature-box-icon"><i class="ti-bolt"></i></div>
									<h5>Hands On Learning </h5>
									<p class="feature-box-desc">Virtual labs and Problem solving in Live projects will help them have better Experience and learning curve. </p>
								</div>
							</div>
							<!-- feature 3 -->
							<div class="col-md-4">
								<div class="feature-box h-100 icon-primary">
									<div class="feature-box-icon"><i class="ti-bolt"></i></div>
									<h5>Analysis and Feedback</h5>
									<p class="feature-box-desc">Option to visualize learners progress through assessment and will send feedback. Our team will sit one-to-one incase of slow progress.  </p>
								</div>
							</div>
						</div>
                 
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
<section>
	<div class="container h-100">
		<h3 class="white-color f-700 text-primary text-center"><b>Topics of the Trainings :  </b> </h3>
		<div class="row mt-4">
			<!-- feature 1 -->
			<div class="col-md-4 mb-4">
				<div class="feature-box h-100 bg-white shadow p-4">
					<h5>Oracle Database</h5>
					
				</div>
			</div>
			<!-- feature 2 -->
			<div class="col-md-4 mb-4">
				<div class="feature-box h-100 bg-white shadow p-4">
					<h5>PostGre SQL</h5>
				</div>
			</div>
			<div class="col-md-4 mb-4">
				<div class="feature-box h-100 bg-white shadow p-4">
					<h5>Ajile Technologies</h5>
					
				</div>
			</div>
			<!-- feature 3 -->
			<div class="col-md-4 mb-4">
				<div class="feature-box h-100 bg-white shadow p-4">
					<h5>AutoCad</h5>
					
				</div>
			</div>
			<!-- feature 3 -->
			<div class="col-md-4 mb-4">
				<div class="feature-box h-100 bg-white shadow p-4">
					<h5>Full Stack Developers</h5>
					
				</div>
			</div>
			<!-- feature 3 -->
			<div class="col-md-4 mb-4">
				<div class="feature-box h-100 bg-white shadow p-4">
					<h5>Data Analytics with Python</h5>
					
				</div>
			</div>
		</div>
	</div>
</section>