<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-grad">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4 display-md-1 mb-2 mb-md-n4 mt-9">Career Support</h2>
			  
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section id="corporate" class="about-area pt-100 pb-70">
	<div class="container">
		<h3 class="white-color f-700 text-primary text-left">Let us go through the different options which we have to develop to achieve our career dreams.</h3>
		<div class="row">
			
			<div class="col-xl-3 mb-3">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Live Projects</h4>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-3">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Challenges</h4>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-3">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Video Resumes</h4>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-3">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Job Consultancy</h4>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-3">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Career Preparation</h4>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-3">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Job Boards</h4>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-3">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Assessments</h4>
					</div>
				</div>
			</div>
			<div class="col-xl-3 mb-3">
				<div class="card">
					<div class="card-header">
						<h4 class="card-title">Hiring Partners</h4>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>
<section id="corporate" class="about-area pt-100 pb-70 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="section-title mb-50 text-center">
					<div class="section-title-heading mb-20">
						<h1 class="text-left">Live Projects 
						<button type="button" class="btn btn-warning float-right">Apply Now</button>
						</h1>
					</div>
					<div class="section-title-para">
						<p class="text-left">Live Projects can support you to experience scalable projects in the industry. It can be via online and offline mode with the topics below: </p>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
					<img src="<?php echo site_url('assets/images/career/mysql.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
					<img src="<?php echo site_url('assets/images/career/php.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
					<img src="<?php echo site_url('assets/images/career/node.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
					<img src="<?php echo site_url('assets/images/career/python.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
					<img src="<?php echo site_url('assets/images/career/react.png'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/career/java.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/career/codeigniter.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
					<img src="<?php echo site_url('assets/images/career/bootstarp.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
						<img src="<?php echo site_url('assets/images/career/spring.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			<div class="col-md-2 mb-3">
				<div class="card shadow">
					<div class="card-body">
					<img src="<?php echo site_url('assets/images/career/oracle.jpg'); ?>" class="img-fluid w-100"/>
					</div>
				</div>
			</div>
			
		</div>
	</div>
</section>

<section id="corporate" class="about-area pt-100 pb-70">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="section-title mb-50 text-center">
					<div class="section-title-heading mb-20">
						<h1 class="text-left">Challenges
						<button type="button" class="btn btn-warning float-right">View All</button>
						</h1>
					</div>
					<div class="section-title-para">
						<p class="text-left">We help the students by organizing different types of challenges so that it can help them to learn more and sharpen their skills.</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row justify-content-center">
			
			<div class="col-md-3 mb-3">
				<div class="card shadow">
					<div class="card-body">
						<h4 class="card-title">Quizes</h4>
					</div>
					<div class="card-footer">
						<a href="<?php echo site_url('home/challenges/quizes'); ?>" class="btn btn-sm btn-info btn-block">View Details</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 mb-3">
				<div class="card shadow">
					<div class="card-body">
						<h4 class="card-title">Coding Challenges</h4>
					</div>
					<div class="card-footer">
						<a href="<?php echo site_url('home/challenges/coding'); ?>" class="btn btn-sm btn-info btn-block">View Details</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 mb-3">
				<div class="card shadow">
					<div class="card-body">
						<h4 class="card-title">Hackathons</h4>
					</div>
					<div class="card-footer">
						<a href="<?php echo site_url('home/challenges/hackathons'); ?>" class="btn btn-sm btn-info btn-block">View Details</a>
					</div>
				</div>
			</div>
			<div class="col-md-3 mb-3">
				<div class="card shadow">
					<div class="card-body">
						<h4 class="card-title">Gaming Challenges</h4>
					</div>
					<div class="card-footer">
						<a href="<?php echo site_url('home/challenges/gaming'); ?>" class="btn btn-sm btn-info btn-block">View Details</a>
					</div>
				</div>
			</div>
		
		</div>
	</div>
</section>

<section id="corporate" class="pt-100 pb-70 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-xl-12">
				<div class="section-title mb-50 text-center">
					<div class="section-title-heading mb-20">
						<h1 class="text-left">Video Resume
						<button type="button" class="btn btn-warning float-right">Create your resume</button>
						</h1>
					</div>
					<div class="section-title-para">
						<p class="text-left">A video resume will increae your chances of getting into a better job</p>
					</div>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xl-6 offset-xl-3 col-lg-8 offset-lg-2 col-md-10 offset-md-1">
				<img src="<?php echo site_url('assets/images/video_resume.png'); ?>" class="img-fluid w-100 shadow"/>
			</div>
		</div>
	</div>
</section>