<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4">Contact Us</h2>
			  
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
		<div class="row">
			<div class="col-12 col-lg-8 mx-auto">
				<div class="title text-center">
					<h2>How can we assist you?</h2>
					<p>Magnox customer support is available free of charge. Connection charges can vary when calling from outside the area, abroad or from a mobile phone.</p>
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-6 mb-5">
				<div class="contact-box h-100 bg-overlay-dark-7 px-3 py-4" style="background:url(<?php echo base_url(); ?>assets/images/bg/contact.jpg) no-repeat; background-size: cover; background-position: center center; ">
					<!-- Phone -->
					<div class="contact-info all-text-white">
						<div class="contact-box-icon"><i class="ti-map-alt"></i></div>
						<h5 class="mb-2">Address</h5>
						<p>Magnox Technologies Pvt. Ltd., 7th Floor, Nasscom Warehouse, Mani Bhandar (7th Floor), Webel Bhaban, Sector V, Saltlake, Kolkata - 700091 </p>
						<p>Magnox Technologies Pvt. Ltd., 2nd Floor, Flot No : 72,  Prembazar, Hijli Co-Operative Society, Kharagpur - 721306</p>
					</div>
					<!-- Email -->
					<div class="contact-info all-text-white">
						<div class="contact-box-icon"><i class="ti-email"></i></div>
						<h5 class="mb-2">E-mail</h5>
						<p>sougam@techmagnox.com<br>magnox.iitkgp@gmail.com</p>
					</div>
					<!-- Phone -->
					<div class="contact-info all-text-white">
						<div class="contact-box-icon"><i class="ti-panel"></i></div>
						<h5 class="mb-2">Phone</h5>
						<p>(+91) 6292259474<br>
							(+91) 6292259479<br>
							(+91) 6292259475<br>
							(+91) 6292259479<br></p>
					</div>
				</div>
			</div>
			<div class="col-md-6 align-items-center">
                <div class="h-100">
                    <p>Please fill in the form and an expert from the admissions office will call you in the next 4 working hours. </p>
                        <div class="row contact-msg" style="display: none;">
                            <div class="col-12">
                                <div class="alert alert-success" role="alert">
                                    Your message was sent successfully.
                                </div>
                            </div>
                        </div>
                        <div class="alert" id="form_submit_msg" style="display: none;"></div>
                        <div class="row" >
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- name -->
                                <span class="form-group">
                                <input id="first_name" name="name" type="text" class="form-control" placeholder="First Name">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- email -->
                                <span class="form-group">
                                <input id="last_name" name="email" type="text" class="form-control" placeholder="Last Name">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- name -->
                                <span class="form-group">
                                <input id="phone" name="name" type="text" class="form-control" placeholder="Phone">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- email -->
                                <span class="form-group">
                                <input id="email" name="text" type="email" class="form-control" placeholder="E-mail">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- name -->
                                <span class="form-group">
                                <input id="place" name="name" type="text" class="form-control" placeholder="Place">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- email -->
                                <span class="form-group">
                                    <select class="form-control mb-3" id="experience">
                                        <option value="">Select Experience Level</option>
                                        <option value="School Student">School Student</option>
                                        <option value="College Student">College Student</option>
                                        <option value="Freshers">Freshers</option>
                                        <option value="Less than 2 Yrs Exp">Less than 2 Yrs Exp</option>
                                        <option value="Less than 5 Yrs Exp">Less than 5 Yrs Exp</option>
                                        <option value="Less than 10 Yrs Exp">Less than 10 Yrs Exp</option>
                                        <option value="More than 10 Yrs Exp">More than 10 Yrs Exp</option>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-12" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <span class="form-group">
                                    <textarea class="form-control" id="message" rows="3" placeholder="message"></textarea>
                                </span>
                            </div>
                            <!-- submit button -->
                            <div class="col-md-12 text-center"><button class="btn btn-dark btn-block" onclick="sendMessage()">Send Message</button></div>
                        </div>
                        <!-- End main form -->
                    <!-- </form> -->
                </div>
            </div>
		</div>
	</div>
</section>
<script>
    $(document).ready(function() {
        $('#first_name').keyup(function(){
            $(this).removeClass('is-invalid');
            $(this).attr('placeholder','First Name');
        });

        $('#last_name').keyup(function(){
            $(this).removeClass('is-invalid');
            $(this).attr('placeholder','Last Name');
        });

        $('#experience').change(function(){
            $(this).removeClass('is-invalid');
            $('#experience option:first').html('Select Experience Level');
            $(this).removeAttr('style');
        });

        $('#email').keyup(function(){
            let value = $(this).val();
            let emailRegx = /^[A-za-z0-9._$#]{1,}@[A-Za-z0-9]{3,}[.]{1}[A-Za-z0-9.]{1,}$/
            if(emailRegx.test(value)){
                $(this).removeClass('is-invalid'); 
                $(this).addClass('is-valid');
                $(this).attr('style','color:#28a745');
            }else{
                $(this).addClass('is-invalid');
                $(this).attr('style','color: #dc3545');
            }
        });

        $('#phone').keyup(function(){
            let value = $(this).val();
            let phoneRegx = /^[4-9][0-9]{9}$/
            if(phoneRegx.test(value)){
                $(this).removeClass('is-invalid'); 
                $(this).addClass('is-valid');
                $(this).attr('style','color:#28a745');
            }else{
                $(this).addClass('is-invalid');
                $(this).attr('style','color: #dc3545');
            }            
        });

        $('#place').keyup(function(){
            $(this).removeClass('is-invalid');
            $(this).attr('placeholder','Place');
        });
    });

    function sendMessage(){
        let first_name  = $('#first_name').val();
        let last_name   = $('#last_name').val();
        let phone       = $('#phone').val();
        let email       = $('#email').val();
        let place       = $('#place').val();
        let experience  = $('#experience').val();
        let message     = $('#message').val();
        
        let isFirstName  = false;
        let isLastName   = false;
        let isEmail      = false;
        let isPhone      = false;
        let isPlace      = false;
        let isExperience = false;

        if(first_name){
            $('#first_name').removeClass('is-invalid');
            $('#first_name').attr('placeholder','First Name');
            isFirstName  = true;
        }else{
            $('#first_name').addClass('is-invalid');
            $('#first_name').attr('placeholder','First Name is required');
        }

        if(last_name){
            $('#last_name').removeClass('is-invalid');
            $('#last_name').attr('placeholder','Last Name');
            isLastName   = true;
        }else{
            $('#last_name').addClass('is-invalid');
            $('#last_name').attr('placeholder','Last Name is required');
        }

        if(phone){
            let phoneRegx = /^[4-9][0-9]{9}$/
            if(phoneRegx.test(phone)){
                $('#phone').removeClass('is-invalid'); 
                $('#phone').addClass('is-valid');
                $('#phone').attr('style','color:#28a745');
                isPhone = true;
            }else{
                $('#phone').addClass('is-invalid');
                $('#phone').attr('style','color: #dc3545');
            }
        }else{
            $('#phone').addClass('is-invalid');
            $('#phone').attr('placeholder','Phone Number is required');
        }

        if(email){
            let emailRegx = /^[A-za-z0-9._$#]{1,}@[A-Za-z0-9]{3,}[.]{1}[A-Za-z0-9.]{1,}$/
            if(emailRegx.test(email)){
                $('#email').removeClass('is-invalid'); 
                $('#email').addClass('is-valid');
                $('#email').attr('style','color:#28a745');
                isEmail      = true;
            }else{
                $('#email').addClass('is-invalid');
                $('#email').attr('style','color: #dc3545');
            }
        }else{
            $('#email').addClass('is-invalid');
            $('#email').attr('placeholder','Email is required');
        }

        if(place){
            $('#place').removeClass('is-invalid');
            $('#place').attr('placeholder','Place');
            isPlace = true;
        }else{
            $('#place').addClass('is-invalid');
            $('#place').attr('placeholder','Place is required');
        }

        if(experience){
            $('#experience').removeClass('is-invalid');
            $('#experience option:first').html('Select Experience Level');
            $('#experience').removeAttr('style');
            isExperience = true;
        }else{
            $('#experience').addClass('is-invalid');
            $('#experience option:first').html('Experience Level is required');
            $('#experience').attr('style','color: #dc3545');
        }

        if(isFirstName && isLastName && isPhone && isEmail && isPlace && isExperience ){
            $.ajax({
                url  : baseURL+'home/sendMessage',
                type : 'POST',
                data : {
                    first_name : first_name,
                    last_name  : last_name,
                    phone      : phone,
                    email      : email,
                    place      : place,
                    experience : experience,
                    message    : message
                },
                success: function(data){
                    $('#first_name').val('');
                    $('#last_name').val('');
                    $('#phone').val('');
                    $('#email').val('');
                    $('#place').val('');
                    $('#experience').val('');
                    $('#message').val('');  
                    $('#email').removeClass('is-valid');                  
                    $('#phone').removeClass('is-valid');
                    let resp = JSON.parse(data);
                    $('#form_submit_msg').fadeIn('slow').addClass(resp.class);
                    $('#form_submit_msg').fadeIn('slow').html(resp.msg);
                    setTimeout(function(){
                        $('#form_submit_msg').removeClass(resp.class).fadeOut('slow');
                        $('#form_submit_msg').html('').fadeOut('slow');
                    }, 10000);
                }
            })
        }
    }
</script>