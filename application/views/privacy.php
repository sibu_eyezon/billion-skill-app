<!-- =======================
Banner innerpage -->
<div class="left pattern-overlay-1 bg-light">
	<div class="container">
		<div class="row">
			<div class="col-md-12 align-self-center">
				<h2 class=" display-4">Privacy Policy</h2>
				
			</div>
		</div>
	</div>
</div>
<!-- =======================
Banner innerpage -->
<section>
	<div class="container h-100">
		<div class="row">
			<div class="col-md-8 mr-auto ml-auto">
			  <div class="card">
				<div class="card-body">
					Magnox provides a platform for the Training Program. Magnox will not be responsible for the Training Contents and the Copyright of the contents belong to the Training Partner and in such case Magnox will not be responsible for any copyright infringement issue.<br><br>
					The Teacher will not use the copyright material of other teacher without the permission of the owner of the content.
				</div>
			  </div>
			</div>
		</div>
	</div>
</section>