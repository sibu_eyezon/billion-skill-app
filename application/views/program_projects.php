<style>
.is-invalid::placeholder {
    color: #dc3545 !important;
}
</style>
<?php
	//banner img setting
	$src = '';
	$banner = trim($prog[0]->banner);
	$intro = trim($prog[0]->intro_video_link);
	//Admission deadline
	$curdate = strtotime(date('d-m-Y'));
	$ldt = strtotime(date('d-m-Y',strtotime($prog[0]->aend_date)));
	$sdt = strtotime(date('d-m-Y',strtotime($prog[0]->astart_date)));
	//Fee Details
	$fee = trim($prog[0]->feetype);
	$amt = (int)$prog[0]->total_fee;
	$dis = (int)($prog[0]->discount);
	if($dis!=0){
		$famt = floatval($amt*(1-(floatval($dis/100))));
	}else{
		$famt = 0;
	}
	$cert = trim($prog[0]->certificate_sample);
	if($cert!=""){
		$remoteFile = 'https://learn.techmagnox.com//uploads/programs/'.$cert;
		$handle = @fopen($remoteFile, 'r');
		if(!$handle){
			$csrc = 'https://learn.techmagnox.com/uploads/programs/'.$cert;
		}else{
			$csrc = 'https://learn.techmagnox.com/uploads/programs/certificate20.jpg';
		}
	}else{
		$csrc = 'https://learn.techmagnox.com/uploads/programs/certificate20.jpg';
	}
?>
<section class="bg-light">
	<div class="container h-100">
		<div class="row">
			<div class="col-md-12 col-lg-6 align-self-center ">
				<div class="title text-left">
					<h2><?php echo trim($prog[0]->title);?></h2>
					<p class="mb-0">
					<?php echo date('jS M Y',strtotime($prog[0]->start_date)); ?> to <?php echo date('jS M Y',strtotime($prog[0]->end_date)); ?>,</strong> Instructor Led Training Program (Online)
					</p>
					<div class="row">
						<div class="col-6">
							<ul class="list-group list-group-borderless list-group-icon-primary-bg">
								<li class="list-group-item"><i class="fa fa-check"></i>Starting From <?php echo date('jS M Y',strtotime($prog[0]->start_date));?></li>
								<li class="list-group-item"><i class="fa fa-check"></i><?php echo "Duration {$prog[0]->duration} {$prog[0]->dtype}";?></li>
								<li class="list-group-item"><i class="fa fa-check"></i><?php echo "{$prog[0]->prog_hrs} Hours";?></li>
							</ul>
						</div>
						<div class="col-6">
							<ul class="list-group list-group-borderless list-group-icon-primary-bg">
								<li class="list-group-item"><i class="fa fa-check"></i><?php echo (!empty($procourse))? count($procourse) : '';?> Courses</li>
								<li class="list-group-item"><i class="fa fa-check"></i><?php echo (!empty($skills))? count($skills):'';?> Skills and Tools</li>
								<li class="list-group-item"><i class="fa fa-check"></i>Live Industry Project</li>
							</ul>
						</div>
					</div>
					<div class="text-center">
						<p>
                        <?php
                            if(!empty($prog[0]->feetype)){
                                if($prog[0]->feetype === 'Paid'){
                                    if(!empty($prog[0]->discount) || $prog[0]->discount > 0){
                                        $percent = ($prog[0]->total_fee * $prog[0]->discount) / 100;
                                        $descount_price = $prog[0]->total_fee - (int)$percent;                                        
                                        echo "<h6><b>Fee : <del>Rs {$prog[0]->total_fee}</del>&nbspRs {$descount_price}</b></h6>";
                                    }else{
                                        echo "<h6><b>Fee : Rs {$prog[0]->total_fee}</b></h6>";
                                    }
                                }
                            }
                        ?>
                        <a class="btn btn-grad mr-2" href="https://learn.techmagnox.com/programAdmission/?id=<?php echo base64_encode($prog[0]->id);?>">Enroll Now</a> 
                        <?php
                            if($prog[0]->program_brochure!=null){
                                if(file_exists('https://learn.techmagnox.com/uploads/programs/'.$prog[0]->program_brochure)){
                                    echo '<a href="https://learn.techmagnox.com/uploads/programs/'.$prog[0]->program_brochure.'" class="btn btn-dark">Download Brochure</a>';
                                }
                            }
                        ?>
                        <h6 >Deadline : <?php echo ($prog[0]->aend_date)? date('jS F Y',strtotime($prog[0]->aend_date)): '';?></h6>
						</p>
                    </div>
				</div>
			</div>
			<div class="col-md-10 col-lg-6 mx-md-auto align-self-center ">
				<img class="rounded img-responsive w-100" src="<?php echo 'https://learn.techmagnox.com/assets/img/banner/'.$banner; ?>" onerror="this.src=`https://learn.techmagnox.com/assets/img/sample.jpg`" alt="">
				<?php
					if($intro!=""){
						echo '<div class="position-absolute left-0 bottom-0 ml-4 ml-md-n2 mb-3">
								<a class="btn btn-grad" data-fancybox href="'.$intro.'"> <i class="fa fa-play text-white"></i>Play Video </a>
							</div>';
					}
				?>
				
			</div>
		</div>
	</div>
</section>
<?php if(!empty($ind_proj)){ ?>
<section class="team team-grid bg-white" >
    <div class="container">
        <div class="row mt-1">
            <!-- feature 1 -->
            <div class="col-md-12">
                <div class="title ">
                    <h3 style="text-align:left">Live Industry Projects
					</h3>
                </div>
            </div>
        </div>
        <div class="row">
			<?php foreach($ind_proj as $iproj){ ?>
            <div class="col-md-4 mt-0">
                <div class="feature-box f-style-2 icon-grad h-100">
                    <h3 class="feature-box-title"><?php echo trim($iproj->txt_project); ?></h3>
                    <div class="feature-box-desc"><?php echo trim($iproj->txt_project_dtls); ?></div>
                    <!--<a class="mt-3" href="#">Know More</a>-->
                </div>
            </div>
			<?php } ?>
        </div>
    </div>
</section>
<?php } ?>
<section class="contact-page  bg-light">
    <div class="container">
        <div class="row">
            <div class="col-12 col-lg-8 mx-auto">
                <div class="title text-center">
                    <h3>Still have Queries ? Contact Us</h3>
                </div>
            </div>
        </div>
        <div class="row ">
            <div class="col-md-6 mb-5">
                <div class="contact-box h-100 bg-overlay-dark-7 px-3 py-4" style="background:url(<?php echo base_url();?>assets/images/bg/04.jpg) no-repeat; background-size: cover; background-position: center center; ">
                    <!-- Phone -->
                    <div class="contact-info all-text-white">
                        <h5 class="mb-2">Address</h5>
                        <p>Magnox Technologies Pvt, Ltd. 7th Floor, Nasscom Warehouse, Mani Bhandar (7th Floor), Webel Bhaban, Sector V, Saltlake
                            Kolkata - 91 
                        </p>
                    </div>
                    <!-- Email -->
                    <div class="contact-info all-text-white">
                        <h5 class="mb-2">E-mail</h5>
                        <p>sougam@techmagnox.com, magnox.iitkgp@gmail.com</p>
                    </div>
                    <!-- Phone -->
                    <div class="contact-info all-text-white">
                        <h5 class="mb-2">Phone</h5>
                        <p>(+91) 6292259475, (+91) 6292259474, (+91) 6292259479</p>
                    </div>
                </div>
            </div>
            <!-- google map -->
            <!-- contact form -->
            <div class="col-md-6">
                <div class="h-100">
                    <p>Please fill in the form and an expert from the admissions office will call you in the next 4 working hours. </p>
                    <!-- <form class="contact-form" id="contact-form" name="contactform" method="post" action="https://wizixo.webestica.com/assets/include/contact-action.php"> -->
                        <!-- Start form message -->
                        <div class="row">
                            <div class="col-12">
                                <div class="alert alert-success contact-msg" style="display: none" role="alert">
                                    Your message was sent successfully.
                                </div>
                            </div>
                        </div>
                        <!-- End form message -->
                        <!-- Start main form -->
                        <div class="alert" id="form_submit_msg"></div>
                        <div class="row" >
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- name -->
                                <span class="form-group">
                                <input id="first_name" name="name" type="text" class="form-control" placeholder="First Name">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- email -->
                                <span class="form-group">
                                <input id="last_name" name="email" type="text" class="form-control" placeholder="Last Name">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- name -->
                                <span class="form-group">
                                <input id="phone" name="name" type="text" class="form-control" placeholder="Phone">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- email -->
                                <span class="form-group">
                                <input id="email" name="text" type="email" class="form-control" placeholder="E-mail">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- name -->
                                <span class="form-group">
                                <input id="place" name="name" type="text" class="form-control" placeholder="Place">
                                </span>
                            </div>
                            <div class="col-md-6" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <!-- email -->
                                <span class="form-group">
                                    <select class="form-control mb-3" id="experience">
                                        <option value="">Select Experience Level</option>
                                        <option value="School Student">School Student</option>
                                        <option value="College Student">College Student</option>
                                        <option value="Freshers">Freshers</option>
                                        <option value="Less than 2 Yrs Exp">Less than 2 Yrs Exp</option>
                                        <option value="Less than 5 Yrs Exp">Less than 5 Yrs Exp</option>
                                        <option value="Less than 10 Yrs Exp">Less than 10 Yrs Exp</option>
                                        <option value="More than 10 Yrs Exp">More than 10 Yrs Exp</option>
                                    </select>
                                </span>
                            </div>
                            <div class="col-md-12" style="margin-bottom:5px; margin-top:5px; padding-bottom:10px">
                                <span class="form-group">
                                    <textarea class="form-control" id="message" rows="3" placeholder="message"></textarea>
                                </span>
                            </div>
                            <!-- submit button -->
                            <div class="col-md-12 text-center"><button class="btn btn-dark btn-block" onclick="sendMessage()">Send Message</button></div>
                        </div>
                        <!-- End main form -->
                    <!-- </form> -->
                </div>
            </div>
        </div>
    </div>
</section>
<script>
    $(document).ready(function() {
        $('#first_name').keyup(function(){
            $(this).removeClass('is-invalid');
            $(this).attr('placeholder','First Name');
        });

        $('#last_name').keyup(function(){
            $(this).removeClass('is-invalid');
            $(this).attr('placeholder','Last Name');
        });

        $('#experience').change(function(){
            $(this).removeClass('is-invalid');
            $('#experience option:first').html('Select Experience Level');
            $(this).removeAttr('style');
        });

        $('#email').keyup(function(){
            let value = $(this).val();
            let emailRegx = /^[A-za-z0-9._$#]{1,}@[A-Za-z0-9]{3,}[.]{1}[A-Za-z0-9.]{1,}$/
            if(emailRegx.test(value)){
                $(this).removeClass('is-invalid'); 
                $(this).addClass('is-valid');
                $(this).attr('style','color:#28a745');
            }else{
                $(this).addClass('is-invalid');
                $(this).attr('style','color: #dc3545');
            }
        });

        $('#phone').keyup(function(){
            let value = $(this).val();
            let phoneRegx = /^[4-9][0-9]{9}$/
            if(phoneRegx.test(value)){
                $(this).removeClass('is-invalid'); 
                $(this).addClass('is-valid');
                $(this).attr('style','color:#28a745');
            }else{
                $(this).addClass('is-invalid');
                $(this).attr('style','color: #dc3545');
            }            
        });

        $('#place').keyup(function(){
            $(this).removeClass('is-invalid');
            $(this).attr('placeholder','Place');
        });
    });

    function sendMessage(){
        let first_name  = $('#first_name').val();
        let last_name   = $('#last_name').val();
        let phone       = $('#phone').val();
        let email       = $('#email').val();
        let place       = $('#place').val();
        let experience  = $('#experience').val();
        let message     = $('#message').val();
        
        let isFirstName  = false;
        let isLastName   = false;
        let isEmail      = false;
        let isPhone      = false;
        let isPlace      = false;
        let isExperience = false;

        if(first_name){
            $('#first_name').removeClass('is-invalid');
            $('#first_name').attr('placeholder','First Name');
            isFirstName  = true;
        }else{
            $('#first_name').addClass('is-invalid');
            $('#first_name').attr('placeholder','First Name is required');
        }

        if(last_name){
            $('#last_name').removeClass('is-invalid');
            $('#last_name').attr('placeholder','Last Name');
            isLastName   = true;
        }else{
            $('#last_name').addClass('is-invalid');
            $('#last_name').attr('placeholder','Last Name is required');
        }

        if(phone){
            let phoneRegx = /^[4-9][0-9]{9}$/
            if(phoneRegx.test(phone)){
                $('#phone').removeClass('is-invalid'); 
                $('#phone').addClass('is-valid');
                $('#phone').attr('style','color:#28a745');
                isPhone = true;
            }else{
                $('#phone').addClass('is-invalid');
                $('#phone').attr('style','color: #dc3545');
            }
        }else{
            $('#phone').addClass('is-invalid');
            $('#phone').attr('placeholder','Phone Number is required');
        }

        if(email){
            let emailRegx = /^[A-za-z0-9._$#]{1,}@[A-Za-z0-9]{3,}[.]{1}[A-Za-z0-9.]{1,}$/
            if(emailRegx.test(email)){
                $('#email').removeClass('is-invalid'); 
                $('#email').addClass('is-valid');
                $('#email').attr('style','color:#28a745');
                isEmail      = true;
            }else{
                $('#email').addClass('is-invalid');
                $('#email').attr('style','color: #dc3545');
            }
        }else{
            $('#email').addClass('is-invalid');
            $('#email').attr('placeholder','Email is required');
        }

        if(place){
            $('#place').removeClass('is-invalid');
            $('#place').attr('placeholder','Place');
            isPlace = true;
        }else{
            $('#place').addClass('is-invalid');
            $('#place').attr('placeholder','Place is required');
        }

        if(experience){
            $('#experience').removeClass('is-invalid');
            $('#experience option:first').html('Select Experience Level');
            $('#experience').removeAttr('style');
            isExperience = true;
        }else{
            $('#experience').addClass('is-invalid');
            $('#experience option:first').html('Experience Level is required');
            $('#experience').attr('style','color: #dc3545');
        }

        if(isFirstName && isLastName && isPhone && isEmail && isPlace && isExperience ){
            $.ajax({
                url  : baseURL+'home/sendMessage',
                type : 'POST',
                data : {
                    first_name : first_name,
                    last_name  : last_name,
                    phone      : phone,
                    email      : email,
                    place      : place,
                    experience : experience,
                    message    : message
                },
                success: function(data){
                    $('#first_name').val('');
                    $('#last_name').val('');
                    $('#phone').val('');
                    $('#email').val('');
                    $('#place').val('');
                    $('#experience').val('');
                    $('#message').val('');  
                    $('#email').removeClass('is-valid');                  
                    $('#phone').removeClass('is-valid');
                    let resp = JSON.parse(data);
                    $('#form_submit_msg').fadeIn('slow').addClass(resp.class);
                    $('#form_submit_msg').fadeIn('slow').html(resp.msg);
                    setTimeout(function(){
                        $('#form_submit_msg').removeClass(resp.class).fadeOut('slow');
                        $('#form_submit_msg').html('').fadeOut('slow');
                    }, 10000);
                }
            })
        }
    }
</script>
