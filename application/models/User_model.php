<?php
defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

class User_model extends CI_Model {
	
	public function insertData($tablename, $data){
		return $this->db->insert($tablename, $data);
	}
	public function insertDataRetId($tablename, $data){
		$this->db->insert($tablename, $data);
		return $this->db->insert_id();
	}
	
	public function get_all_counters()
	{
		$query = "select (select count(prog.id) from program prog left join prog_admission pa on pa.prog_id=prog.id where prog.status='approved' and prog.category != 'Webinar Program' AND prog.category != 'Seminar Program' and pa.view_mode = '1') as program_count, (select count(id) from user_auth where user_type = 'student') as student_count, (select count(id) from instructor_list) as intrsuctor_count, (select count(prog.id) from program prog left join prog_admission pa on pa.prog_id=prog.id where prog.status='approved' and prog.category = 'Webinar Program' OR prog.category = 'Seminar Program' and pa.view_mode = '1') as event_count";
		
		return $this->db->query($query)->result();
	}
	
	public function get_limited_skills()
	{
		$this->db->order_by('add_datetime', 'DESC');
		$this->db->limit(12);
		return $this->db->get('skills')->result();
	}
	
	public function get_all_skills()
	{
		$this->db->order_by('add_datetime', 'DESC');
		return $this->db->get('skills')->result();
	}
	
	public function get_all_cities()
	{
		$this->db->order_by('name', 'ASC');
		return $this->db->get('city')->result();
	}
	
	public function get_all_industries()
	{
		$this->db->order_by('name', 'ASC');
		return $this->db->get('industry')->result();
	}
	
	public function get_all_designations()
	{
		$this->db->order_by('name', 'ASC');
		return $this->db->get('designation')->result();
	}
	
	public function get_all_degrees()
	{
		$this->db->order_by('degree_name', 'ASC');
		return $this->db->get('degree')->result();
	}
	
	public function get_all_feedback()
	{
		$this->db->where('status', '1');
		$this->db->order_by('create_datetime', 'DESC');
		return $this->db->get('feedback')->result();
	}
	
	public function get_all_alumni_speak()
	{
		$this->db->where('yn_valid', '1');
		$this->db->order_by('dat_sys_date', 'DESC');
		return $this->db->get('alumni_speak')->result();
	}
	
	public function get_limited_programs()
	{
		$this->db->select("prog.*, pa.*, acayear.yearnm, CONCAT(ud.first_name,' ', ud.last_name) as uname")->from('program prog');
		$this->db->join('user_details ud', 'ud.id=prog.user_id', 'inners');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('prog.status', 'approved');
		$this->db->where('pa.view_mode', '1');
		$this->db->order_by('prog.date_added', 'DESC');
		$this->db->limit(5);
		return $this->db->get()->result();
	}
	
	public function get_all_programs()
	{
		$this->db->select('prog.*, pa.*, acayear.yearnm')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('prog.status', 'approved');
		$this->db->where('pa.view_mode', '1');
		$this->db->order_by('prog.title', 'ASC');
		return $this->db->get()->result();
	}
	
	public function filter_programs($page)
	{
		$this->db->select('prog.*, pa.*, acayear.yearnm')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('prog.status', 'approved');
		if($page == 'events'){
			$this->db->where("prog.category = 'Webinar Program' OR prog.category = 'Seminar Program'");
		}else{
			$this->db->where("prog.category != 'Webinar Program' AND prog.category != 'Seminar Program'");
		}
		$this->db->where('pa.view_mode', '1');
		$this->db->order_by('pa.aend_date', 'DESC');
		return $this->db->get()->result();
	}
	
	public function get_selective_programs($page, $where)
	{
		$this->db->select('prog.*, pa.*, acayear.yearnm')->from('program prog');
		$this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
		$this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
		$this->db->where('prog.status', 'approved');
		if($page == 'events'){
			$this->db->where("prog.category = 'Webinar Program' OR prog.category = 'Seminar Program'");
		}else{
			$this->db->where("prog.category != 'Webinar Program' AND prog.category != 'Seminar Program'");
		}
		if($where != ""){
			$this->db->where($where);
		}
		$this->db->where('pa.view_mode', '1');
		$this->db->order_by('pa.aend_date', 'DESC');
		return $this->db->get()->result();
	}
	
	public function filter_instructor()
	{
		$this->db->select('*')->from('instructor_list');
		$this->db->order_by('txt_instructor_name', 'ASC');
		$this->db->where('txt_active_flag', 'Y');
		return $this->db->get()->result();
	}
	
	public function getProgramOrg($prog_id)
	{
		$this->db->select('org.*')->from('pro_organization org');
		$this->db->join('pro_map_org pmo', 'pmo.org_id=org.id', 'inner');
		$this->db->where('pmo.program_id', $prog_id);
		$this->db->order_by('org.add_date', 'DESC');
		return $this->db->get()->result();
	}
	
	
	public function get_all_filter_jobs($title, $location, $type, $skills, $industry, $position, $degree)
	{
		$sql2 = 'SELECT jobs_id fom jobs_skills where skills_id='.$skills;
		/*$array_like = explode(',', $title);
		$like_statements = array();
		foreach($array_like as $value) {
			$like_statements[] = "jb.title LIKE '%" . $value . "%'";
		}
		$like_string = "(" . implode(' OR ', $like_statements) . ")";*/
		
		$this->db->select('jb.*, city.name as location, desig.name as position, org.name as org_name, ind.name as industry, deg.degree_name, deg.short')->from('jobs jb');
		
		$this->db->join('organization org', 'org.id=jb.org_id', 'inner');
		$this->db->join('industry ind', 'ind.id=jb.indus_id', 'left');
		$this->db->join('degree deg', 'deg.id=jb.min_qualification_id', 'left');
		$this->db->join('designation desig', 'desig.id=jb.designation_id', 'left');
		$this->db->join('city', 'city.id=jb.location_id', 'left');
		if($title!=""){
			$this->db->like('lower(jb.title)', strtolower($title));
		}
		if($location!=""){
			$this->db->where('jb.location_id', $location);
		}
		if($type!=""){
			$this->db->like('jb.type', $type);
		}
		if($skills!=""){
			$this->db->where_in('jb.id', $sql2);
		}
		if($industry!=""){
			$this->db->where('jb.indus_id', $industry);
		}
		if($position!=""){
			$this->db->where('jb.designation_id', $position);
		}
		if($degree!=""){
			$this->db->where('jb.min_qualification_id', $degree);
		}
		$this->db->order_by('jb.create_date_time', 'DESC');
		return $this->db->get()->result();
	}
	public function get_job_details($id)
	{
		$this->db->select('jb.*, city.name as location, desig.name as position, org.name as org_name, ind.name as industry, deg.degree_name, deg.short')->from('jobs jb');
		
		$this->db->join('organization org', 'org.id=jb.org_id', 'inner');
		$this->db->join('industry ind', 'ind.id=jb.indus_id', 'left');
		$this->db->join('degree deg', 'deg.id=jb.min_qualification_id', 'left');
		$this->db->join('designation desig', 'desig.id=jb.designation_id', 'left');
		$this->db->join('city', 'city.id=jb.location_id', 'left');
		$this->db->where('jb.id', $id);
		return $this->db->get()->result();
	}
	public function get_jobs_skills($id)
	{
		$this->db->select('skills.name, skills.slogo')->from('skills');
		$this->db->join('jobs_skills js', 'js.skills_id=skills.id', 'inner');
		$this->db->where('js.jobs_id', $id);
		return $this->db->get()->result();
	}
	
	public function get_all_blogs()
	{
		$this->db->where('active', true);
		$this->db->order_by('id', 'DESC');
		return $this->db->get('blogs')->result();
	}
	public function get_blog_details_by_id($id)
	{
		$this->db->where('active', true);
		$this->db->where('id', $id);
		return $this->db->get('blogs')->result();
	}
	public function get_other_blogs_by_id($id)
	{
		$this->db->where('active', true);
		$this->db->where_not_in('id', $id);
		$this->db->limit(3);
		return $this->db->get('blogs')->result();
	}
	public function get_blogs_skills($skills)
	{
		$this->db->where_in('id', json_decode($skills));
		return $this->db->get('skills')->result();
	}
	public function get_blogs_categories($categories)
	{
		$this->db->where_in('id', json_decode($categories));
		return $this->db->get('category')->result();
	}
	
	
	/*=================================TEMPORARY FUNCTIONS==============================*/
	public function checkValidUser($email)
	{
		$this->db->select('ua.*');
		$this->db->join('user_details ud', 'ud.id=ua.user_id', 'inner');
		$this->db->where(array('ua.email'=>$email, 'ud.email'=>$email, 'user_type'=>'student'));
		return $this->db->get('user_auth ua')->result();
	}
	public function checkReduntAdmission($prog_id, $userid)
	{
		$this->db->where('prog_id', $prog_id);
		$this->db->where('cand_id', $userid);
		return $this->db->get('adm_can_apply')->num_rows();
	}
	public function insertStudProgAdm($prog_id, $userid, $acayear, $enroll, $pamt, $discount)
	{
		$this->db->trans_off();
		$this->db->trans_strict(false);
		
		$this->db->trans_start();
		$data3['prog_id'] = $prog_id;
		$data3['cand_id'] = $userid;
		$data3['approve_flag'] = '1';
		$data3['prog_status'] = 0;
		$data3['apply_datetime'] = date('Y-m-d H:i:s');
		if($this->db->insert('adm_can_apply',$data3)){
			$data4['stud_id'] = $userid;
			$data4['prog_id'] = $prog_id;
			$data4['aca_yearid'] = $acayear;
			$data4['enrollment_no'] = $enroll;
			$data4['status'] = 0;
			$data4['totalfees'] = $pamt;
			$data4['discount'] = $discount;
			$data4['admission_date'] = date('Y-m-d H:i:s');
			$data4['add_datetime'] = date('Y-m-d H:i:s');
			if(!$this->db->insert('stud_prog_cons',$data4)){
				$this->db->trans_rollback();
			}
		}else{
			$this->db->trans_rollback();
		}
		$this->db->trans_complete();
		
		return $this->db->trans_status();
	}
}

?>