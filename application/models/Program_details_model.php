<?php
    defined ( 'BASEPATH' ) or exit ( 'No direct script access allowed' ); 

    class Program_details_model extends CI_Model {
        public function __construct() {
            parent::__construct();
        }

        public function getProgramById($id){
            $this->db->select('prog.*, pa.*, acayear.yearnm')->from('program prog');
            $this->db->join('prog_admission pa', 'pa.prog_id=prog.id', 'left');
            $this->db->join('acayear', 'acayear.sl=pa.aca_year', 'left');
            $this->db->where('prog.id', $id);
            //$this->db->where('prog.status', 'approved');
            return $this->db->get()->result();
        }

        public function getBenefitsByProgId($id){
            $this->db->select('txt_benefit, txt_benefit_dtls');
            $this->db->where('program_id', $id);
            return $this->db->get('program_benefits')->result();
        }

        public function getAllInstructorsByIdRole($prog, $role, $userId, $status){
            $this->db->select("pur.id AS purid, ud.id, CONCAT(ud.salutation,' ',ud.first_name,' ',ud.last_name) AS name, ud.email, ud.phone, ud.photo_sm, ud.about_me, ud.organization, ud.designation, ud.facebook_link, ud.linkedin_link, prog.title, pur.role, pur.status")->from('user_details ud');
            $this->db->join('pro_users_role pur', 'pur.user_id=ud.id', 'inner');
            $this->db->join('program prog', 'prog.id=pur.program_id', 'inner');
            if($prog!='all'){
                $this->db->where('pur.program_id', $prog);
            }
            if($role!='All'){
                $this->db->where('pur.role', $role);
            }
            if($userId!=null){
                $this->db->where('prog.user_id', $userId);
            }
            $this->db->where('pur.status', $status);
            $this->db->where('prog.status', 'approved');
            $this->db->order_by('pur.add_date', 'DESC');
            return $this->db->get()->result();
        }

        public function getAllFAQ($program_id){
            $this->db->select('txt_question, txt_answer');
            $this->db->where('yn_valid', 1);
            $this->db->where_in('program_id', $program_id);
            return $this->db->get('program_faqs')->result();
        }
		
		public function getAllAlumniSpeak($pid)
		{
			$this->db->where('yn_valid', 1);
            $this->db->where_in('program_id', $pid);
            return $this->db->get('alumni_speak')->result();
		}
		
		public function getLimitedIndustrialProjects($pid)
		{
			$this->db->where('yn_valid', 1);
            $this->db->where_in('program_id', $pid);
			$this->db->limit(3);
            return $this->db->get('program_project')->result();
		}
		public function getAllIndustrialProjects($pid)
		{
			$this->db->where('yn_valid', 1);
            $this->db->where_in('program_id', $pid);
            return $this->db->get('program_project')->result();
		}

        public function getAllSemsByProgId($prog_id){
            $this->db->select('ps.*')->from('pro_semister ps');
            $this->db->join('pro_map_sem pms', 'pms.sem_id=ps.id', 'inner');
            $this->db->where('pms.program_id', $prog_id);
            $this->db->order_by('ps.title', 'ASC');
            return $this->db->get()->result();
        }

        public function getAllCourse($program_id){
            $this->db->select('pc.title, pc.overview,pc.lec, pc.tut, pc.prac, pc.c_code, pmc.sem_id')->from('pro_course pc');
            $this->db->join('pro_map_course pmc','pmc.course_id = pc.id');
            $this->db->where('pc.prog_id',$program_id);
            $this->db->where('pmc.program_id',$program_id);
            return $this->db->get()->result();
        }

        public function getContactUs($data){
            $this->db->set($data);
            return $this->db->insert('contact_us');
        }
		
		public function getAllProgramSkills($skills_set)
		{
			$this->db->select('name, slogo');
			$this->db->where_in('id', $skills_set);
			return $this->db->get('skills')->result();
		}
    }

?>