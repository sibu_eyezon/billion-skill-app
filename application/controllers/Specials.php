<?php
defined('BASEPATH') OR exit('No direct script access allowed');

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

require_once "vendor/autoload.php";

class Specials extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
	}
	
	public function MailSystem($to, $cc, $subject, $message)
	{
		$email = "admin@billionskills.com";
        $password = "c6spQD82CVfch7aT";

		$mail = new PHPMailer(true);
		//Enable SMTP debugging.
		$mail->SMTPDebug = 0;                               
		//Set PHPMailer to use SMTP.
		$mail->isSMTP();            
		//Set SMTP host name  
		$mail->CharSet = 'utf-8';// set charset to utf8
		//$mail->Encoding = 'base64';
		$mail->SMTPAuth = true;// Enable SMTP authentication
		$mail->SMTPSecure = 'ssl';// Enable TLS encryption, `ssl` also accepted

		$mail->Host = 'lotus.arvixe.com';// Specify main and backup SMTP servers
		$mail->Port = 465;// TCP port to connect to
		$mail->SMTPOptions = array(
			'ssl' => array(
				'verify_peer' => false,
				'verify_peer_name' => false,
				'allow_self_signed' => true
			)
		);		                     
		//Provide username and password     
		$mail->Username = $email;                 
		$mail->Password = $password;                                                                         

		$mail->From = $email;
		$mail->FromName = 'Magnox Learning+';

		$mail->addAddress($to);

		$mail->isHTML(true);

		$mail->Subject = $subject;
		$mail->Body = $message;
		//$mail->AltBody = "This is the plain text version of the email content";

		return ($mail->send())? 1 : $mail->ErrorInfo;
	}
	
	private function getCourseDetails($program_id){
		$sems = $this->program_details_model->getAllSemsByProgId($program_id);
		$getCourse = $this->program_details_model->getAllCourse($program_id);
		$sendDate  = [];
		if(count($sems) <= 1){
			return $getCourse;
		}
	}
	public function srm_program_details()
	{
		$data['pageTitle'] = 'Program Details | BillionSkills';
		$program_id = (int)base64_decode($_GET['id']);
		$data['prog'] = $this->program_details_model->getProgramById($program_id);
		$data['benefits'] = $this->program_details_model->getBenefitsByProgId($program_id);
		$data['instructors'] = $this->program_details_model->getAllInstructorsByIdRole($program_id, 'Teacher', null, 'accepted');
		$data['sems'] = $this->program_details_model->getAllSemsByProgId($program_id);
		$data['courses'] = $this->getCourseDetails($program_id);
		$pid = array(0, $program_id);
		$data['faq'] = $this->program_details_model->getAllFAQ($pid);
		$data['alumspeak'] = $this->program_details_model->getAllAlumniSpeak($pid);
		$data['ind_proj'] = $this->program_details_model->getLimitedIndustrialProjects($pid);
		$data['alumnispk'] = $this->user_model->get_all_alumni_speak();
		$data['pageName'] = 'specials/srmprogram_details';
		$this->load->view('index', $data);
	}
	
	public function srm_programAdmission()
	{
		$data['pageTitle'] = 'Program Admission | BillionSkills';
		$program_id = (int)base64_decode($_GET['id']);
		$data['prog'] = $this->program_details_model->getProgramById($program_id);
		$data['pageName'] = 'specials/srmprogram-admission';
		$this->load->view('index', $data);
	}
	
	public function user_registration()
	{
		//setup
		$this->load->helper('string');
		$vcode = random_string('numeric', 6);
		$roll = random_string('numeric', 3);
		$userid = 0;
		$res = array('status'=>false, 'msg'=>'Something went wrong.');
		$about = "";
		//validation
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[3]|max_length[255]');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|min_length[3]|max_length[255]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_emails');
		$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|numeric|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('newpass', 'Password', 'trim|required');
		$this->form_validation->set_rules('category', 'Category', 'trim|required');
		$this->form_validation->set_rules('department', 'Department', 'trim|required');
		
		if ($this->form_validation->run() == FALSE){
			$res['msg']=validation_errors();
		}else{
			//POST data
			$fname = ucwords(trim(strtolower($_POST['fname'])));
			$lname = ucwords(trim(strtolower($_POST['lname'])));
			$prog_id = $_POST['pid'];
			$id_num = trim($_POST['identity_num']);
			$cat = trim($_POST['category']);
			$pamt = ($cat=='Student')? 150 : 500;
			$dept = trim($_POST['department']);
			$about = "<p><strong>Department:</strong> ".$dept."<br><strong>Category:</strong> ".$cat."</p>";
			if($id_num != ""){
				$about.= "<p><strong>Identity:</strong> ".$id_num."</p>";
			}
			$email = trim(strtolower($_POST['email']));
			$phone = trim($_POST['mobile']);
			$organization = 'SRM Institute of Science and Technology';
			$password = md5(trim($_POST['newpass']));
			$pdetails = $this->program_details_model->getProgramById($prog_id);
			//mail setup
			$subject = 'Magnox Learning+ Registration';
			$msg = '<html><body>Hello '.trim($fname." ".$lname).',<br><br><h4 style="text-align:center">Thank you for signing up for 2 Days Workshop on Digital Marketing.</h4><hr>';
			//check existing user
			$user = $this->user_model->checkValidUser($email);
			if(empty($user)){
				$data1['first_name'] = trim($fname);
				$data1['last_name'] = trim($lname);
				$data1['email'] = $email;
				$data1['phone'] = $phone;
				$data1['about_me'] = $about;
				$data1['organization'] = $organization;
				$data1['created_date_time'] = date('Y-m-d H:i:s');
				
				$userid = $this->user_model->insertDataRetId('user_details', $data1);
				if($userid){
					$data['user_id'] = $userid;
					$data['first_name'] = trim($fname);
					$data['last_name'] = trim($lname);
					$data['email'] = $email;
					$data['phone'] = $phone;
					$data['password'] = $password;
					$data['verification_code'] = $vcode;
					$data['verification_status'] = true;
					$data['user_type'] = 'student';
					$data['create_date_time'] = date('Y-m-d H:i:s');
					
					$this->user_model->insertData('user_auth', $data);
				}
				$msg.='<br>Please click <a href="https://learn.techmagnox.com/Login/userVerification/?email='.base64_encode($email).'&vercode='.base64_encode($vcode).'" target="_blank">here</a> to verify your email.<br>Log in to your account and complete the payment.<br>';
			}else{
				$userid = $user[0]->user_id;
			}
			//check admission
			if($userid != 0){
				$chk_adm = $this->user_model->checkReduntAdmission($prog_id, $userid);
				if($chk_adm==0){
					$enroll = trim($pdetails[0]->code).$roll;
					if($this->user_model->insertStudProgAdm($prog_id, $userid, $pdetails[0]->aca_year, $enroll, $pamt, 0)){
						$msg.= '<h5 style="text-align:center">You have been enroll to this program with: <br>Enrollment No: '.$enroll.'<br>Roll No: '.$roll.'</h5><hr><br><small><code>This is an autogenerated message. Please do not reply.</code></small><br><br>Thanking you,<br><img src="https://learn.techmagnox.com/assets/img/logo.png" style="width:100px;"/><br>Magnox Learning Plus,<br>Learning and Course Management Platform<br><a href="mailto:sougam@techmagnox.com">sougam@techmagnox.com</a><br><a href="tel:9932242598">9932242598</a></body></html>';
						$this->MailSystem($email, '', $subject, $msg);
						
						$res['status'] = true;
						$res['msg'] = 'You have successfully submitted your details. Login and Pay the Workshop fees of Rs 150 inorder to complete the registration from this link. Please check your email for further instructions and verify the email.';
					}else{
						$res['msg'] = 'Application failed. Please try again.';
					}
				}else{
					$res['msg'] = 'You have alreay applied in this program. Please try log in to your dashboard.';
				}
			}else{
				$res['msg'] = 'Network is slow. Please check your connection.';
			}
		}
		
		echo json_encode($res);
	}
}