<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->load->library('form_validation');
		date_default_timezone_set('Asia/Kolkata');
		ini_set('memory_limit', '-1');
	}
	
	public function index()
	{
		$data['pageTitle'] = 'Home | BillionSkills';
		$data['pageName'] = 'home';
		$data['programs'] = $this->user_model->get_limited_programs();
		$data['skills'] = $this->user_model->get_limited_skills();
		$data['alumnispk'] = $this->user_model->get_all_alumni_speak();
		$data['instructor'] = $this->user_model->filter_instructor();
		$data['counter'] = $this->user_model->get_all_counters();
		$this->load->view('index', $data);
	}
	
	public function error_404()
	{
		$data['pageTitle'] = '404 Error | BillionSkills';
		$data['pageName'] = 'error_404';
		$this->load->view('index', $data);
	}
	
	public function feedback()
	{
		$data['pageTitle'] = 'Feedback | BillionSkills';
		$data['pageName'] = 'feedback';
		$this->load->view('index', $data);
	}
	public function view_feedback()
	{
		$data['pageTitle'] = 'Reviews | BillionSkills';
		$data['pageName'] = 'view_feedback';
		$data['feedback'] = $this->user_model->get_all_feedback();
		$this->load->view('index', $data);
	}
	
	public function refund_policy()
	{
		$data['pageTitle'] = 'Refund Policy | BillionSkills';
		$data['pageName'] = 'refund_policy';
		$this->load->view('index', $data);
	}
	public function privacy()
	{
		$data['pageTitle'] = 'privacy | BillionSkills';
		$data['pageName'] = 'privacy';
		$this->load->view('index', $data);
	}
	public function terms_conditions()
	{
		$data['pageTitle'] = 'Terms and Conditions | BillionSkills';
		$data['pageName'] = 'terms_conditions';
		$this->load->view('index', $data);
	}
	/*=======================================================*/
	
	/*==========================HOME=========================*/
	public function corporate()
	{
		$data['pageTitle'] = 'Corporate Training | BillionSkills';
		$data['pageName'] = 'corporate';
		$this->load->view('index', $data);
	}
	public function higher_education()
	{
		$data['pageTitle'] = 'Higher Education | BillionSkills';
		$data['pageName'] = 'higher_education';
		$this->load->view('index', $data);
	}
	public function consultancy()
	{
		$data['pageTitle'] = 'Consultancy | BillionSkills';
		$data['pageName'] = 'consultancy';
		$this->load->view('index', $data);
	}
	public function partner()
	{
		$data['pageTitle'] = 'Become a Partner | BillionSkills';
		$data['pageName'] = 'partner';
		$this->load->view('index', $data);
	}
	public function collaborations()
	{
		$data['pageTitle'] = 'Collaborations | BillionSkills';
		$data['pageName'] = 'collaborations';
		$this->load->view('index', $data);
	}
	
	public function category($params="")
	{
		$data['pageTitle'] = ucwords(str_replace("_"," ",$params)).' | BillionSkills';
		$data['pageName'] = 'category/'.$params;
		$this->load->view('index', $data);
	}
	public function challenges($params="")
	{
		$data['pageTitle'] = ucwords(str_replace("_"," ",$params)).' | BillionSkills';
		$data['pageName'] = 'challenges/'.$params;
		$this->load->view('index', $data);
	}
	
	public function skills()
	{
		$data['pageTitle'] = 'All Skills | BillionSkills';
		$data['pageName'] = 'skills';
		$this->load->view('index', $data);
	}
	
	public function pg_programs()
	{
		$data['pageTitle'] = 'Post Graduate Programs | BillionSkills';
		$data['pageName'] = 'pg_programs';
		$this->load->view('index', $data);
	}
	
	public function organizations()
	{
		$data['pageTitle'] = 'Institutes/ Colleges | BillionSkills';
		$data['pageName'] = 'organizations';
		$this->load->view('index', $data);
	}
	/*=======================================================*/
	
	/*====================Explore Programs===================*/
	public function explore_programs()
	{
		$data['pageTitle'] = 'Explore Programs | BillionSkills';
		$data['pageName'] = 'explore_programs';
		$this->load->view('index', $data);
	}
	/*=======================================================*/
	/*====================Programs Details===================*/
	public function get_program_details(){
		$data['pageTitle'] = 'Program Details | BillionSkills';
		$program_id = (int)base64_decode($_GET['id']);
		$data['prog'] = $this->program_details_model->getProgramById($program_id);
		$data['benefits'] = $this->program_details_model->getBenefitsByProgId($program_id);
		$data['instructors'] = $this->program_details_model->getAllInstructorsByIdRole($program_id, 'Teacher', null, 'accepted');
		$data['sems'] = $this->program_details_model->getAllSemsByProgId($program_id);
		$data['courses'] = $this->getCourseDetails($program_id);
		$pid = array(0, $program_id);
		$data['faq'] = $this->program_details_model->getAllFAQ($pid);
		$data['alumspeak'] = $this->program_details_model->getAllAlumniSpeak($pid);
		$data['ind_proj'] = $this->program_details_model->getLimitedIndustrialProjects($pid);
		$data['pageName'] = 'program_details';
		$data['alumnispk'] = $this->user_model->get_all_alumni_speak();
		$this->load->view('index', $data);
	}
	
	public function events()
	{
		$data['pageTitle'] = 'Events | BillionSkills';
		$data['pageName'] = 'events';
		$this->load->view('index', $data);
	}

	private function getCourseDetails($program_id){
		$sems = $this->program_details_model->getAllSemsByProgId($program_id);
		$getCourse = $this->program_details_model->getAllCourse($program_id);
		$sendDate  = [];
		if(count($sems) <= 1){
			return $getCourse;
		}
	}
	public function program_projects()
	{
		$data['pageTitle'] = 'Program Projects | BillionSkills';
		$program_id = (int)base64_decode($_GET['pid']);
		$data['prog'] = $this->program_details_model->getProgramById($program_id);
		$pid = array(0, $program_id);
		$data['ind_proj'] = $this->program_details_model->getAllIndustrialProjects($pid);
		$data['pageName'] = 'program_projects';
		$this->load->view('index', $data);
	}
	
	public function hiring()
	{
		$data['pageTitle'] = 'Hire from Us | BillionSkills';
		$data['pageName'] = 'hiring';
		$data['alumnispk'] = $this->user_model->get_all_alumni_speak();
		$data['ilist'] = $this->user_model->filter_instructor();
		$data['programs'] = $this->user_model->get_selective_programs('programs', "pa.prog_level='2'");
		$this->load->view('index', $data);
	}
	/*======================Instructors======================*/
	public function instructor($params="home")
	{
		$data['pageTitle'] = ucfirst($params).' | BillionSkills';
		$data['pageName'] = $params;
		if($params == 'ibenefits'){
			$data['ilist'] = $this->user_model->filter_instructor();
		}
		$this->load->view('index', $data);
	}
	public function getFilteredTeachers()
	{
		$data['ilist'] = $this->user_model->filter_instructor();

		return $this->load->view('instructor_list', $data);
	}
	/*=======================================================*/
	
	/*=======================Magnox LMS======================*/
	public function lms($params="home")
	{
		$data['pageTitle'] = ucfirst($params).' | BillionSkills';
		$data['pageName'] = $params;
		if($params == 'certifications'){
			$data['programs'] = $this->user_model->get_selective_programs('programs', "pa.prog_level='3'");
			$data['ilist'] = $this->user_model->filter_instructor();
		}else if($params == 'live_buddies'){
			$data['programs'] = $this->user_model->get_selective_programs('programs', "pa.prog_level='1'");
			$data['ilist'] = $this->user_model->filter_instructor();
		}else if($params == 'mentor_connect'){
			$data['programs'] = $this->user_model->get_selective_programs('programs', "pa.prog_level='2'");
			$data['ilist'] = $this->user_model->filter_instructor();
		}
		else if($params == 'webinars-workshops'){
			$data['programs'] = $this->user_model->get_selective_programs('events', "");
			$data['ilist'] = $this->user_model->filter_instructor();
		}
		$this->load->view('index', $data);
	}
	/*=======================================================*/
	
	/*========================Company========================*/
	public function about()
	{
		$data['pageTitle'] = 'About the Company | BillionSkills';
		$data['pageName'] = 'about';
		$this->load->view('index', $data);
	}
	public function people()
	{
		$data['pageTitle'] = 'People | BillionSkills';
		$data['pageName'] = 'people';
		$this->load->view('index', $data);
	}
	public function portfolio()
	{
		$data['pageTitle'] = 'Portfolio | BillionSkills';
		$data['pageName'] = 'portfolio';
		$this->load->view('index', $data);
	}
	public function investors()
	{
		$data['pageTitle'] = 'Investors | BillionSkills';
		$data['pageName'] = 'investors';
		$this->load->view('index', $data);
	}
	public function success_stories()
	{
		$data['pageTitle'] = 'Success Stories | BillionSkills';
		$data['pageName'] = 'success_stories';
		$this->load->view('index', $data);
	}
	public function career()
	{
		$data['pageTitle'] = 'Career Support | BillionSkills';
		$data['pageName'] = 'career';
		$data['alumnispk'] = $this->user_model->get_all_alumni_speak();
		$this->load->view('index', $data);
	}
	public function hiring_partners()
	{
		$data['pageTitle'] = 'Hiring Partners | BillionSkills';
		$data['pageName'] = 'partners';
		$this->load->view('index', $data);
	}
	public function contact()
	{
		$data['pageTitle'] = 'Contact Us | BillionSkills';
		$data['pageName'] = 'contact';
		$this->load->view('index', $data);
	}
	public function gallery()
	{
		$data['pageTitle'] = 'Galleria | BillionSkills';
		$data['pageName'] = 'pricing';
		$this->load->view('index', $data);
	}
	public function platform()
	{
		$data['pageTitle'] = 'Platform + | BillionSkills';
		$data['pageName'] = 'features';
		$this->load->view('index', $data);
	}
	/*=======================================================*/
	
	public function associate($params="business")
	{
		$data['pageTitle'] = ucfirst($params).' Associate | BillionSkills';
		$data['params'] = $params;
		$data['pageName'] = 'associate';
		$this->load->view('index', $data);
	}
	/*=======================================================*/
	
	public function getFilteredData()
	{
		$page = trim($_GET['program']);
		
		$data['programs'] = $this->user_model->filter_programs($page);
		
		return $this->load->view('program_list', $data);
	}
	
	public function insertFeedback()
	{
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[3]|max_length[255]');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|min_length[3]|max_length[255]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_emails');
		$this->form_validation->set_rules('mobile', 'Mobile', 'trim|required|numeric|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('msg', 'Message', 'trim|required');
		
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('errors', validation_errors());
		}else{
			$data['first_name'] = ucwords(trim(strtolower($_POST['fname'])));
			$data['last_name'] = ucwords(trim(strtolower($_POST['lname'])));
			$data['email'] = trim(strtolower($_POST['email']));
			$data['mobile'] = trim($_POST['mobile']);
			$data['msg'] = ucwords(trim(strtolower($_POST['msg'])));
			$data['status'] = '0';
			$data['create_datetime'] = date('Y-m-d H:i:s');
			if($this->user_model->insertData('feedback', $data)){
				$this->session->set_flashdata('errors', 'Your feedback has been received by us. We will surely respond to you.');
			}else{
				$this->session->set_flashdata('errors', 'Feedback received error. Something went wrong, please try again');
			}
		}
		redirect(base_url().'home/feedback');
	}
	
	public function insertPartner()
	{
		$this->form_validation->set_rules('fname', 'First Name', 'trim|required|min_length[3]|max_length[255]');
		$this->form_validation->set_rules('lname', 'Last Name', 'trim|required|min_length[3]|max_length[255]');
		$this->form_validation->set_rules('email', 'Email Address', 'trim|required|valid_emails');
		$this->form_validation->set_rules('phone', 'Phone', 'trim|required|numeric|min_length[10]|max_length[10]');
		$this->form_validation->set_rules('org', 'Organization', 'trim|required|max_length[255]');
		$this->form_validation->set_rules('position', 'Designation', 'trim|required|max_length[255]');
		if ($this->form_validation->run() == FALSE){
			$this->session->set_flashdata('errors', validation_errors());
		}else{
			$data['fname'] = ucwords(trim(strtolower($_POST['fname'])));
			$data['lname'] = ucwords(trim(strtolower($_POST['lname'])));
			$data['email'] = trim(strtolower($_POST['email']));
			$data['phone'] = trim($_POST['mobile']);
			$data['organization'] = trim($_POST['org']);
			$data['position'] = trim($_POST['position']);
			$data['address'] = trim($_POST['address']);
			$data['add_datetime'] = date('Y-m-d H:i:s');
			if($this->user_model->insertData('partners', $data)){
				$this->session->set_flashdata('errors', 'Partnership data has been received by us. We will surely respond to you.');
			}else{
				$this->session->set_flashdata('errors', 'Partnership data received error. Something went wrong, please try again');
			}
			/*$subject = "Feedback Received - Billionskills";
			$message = "Hello ".trim($data['first_name'].' '.$data['last_name'])."<br>Your feedback has been received.";*/
		}
		redirect(base_url().'home/partner');
	}
	/*=======================================================*/
	
	public function jobs()
	{
		$data['pageTitle'] = 'Jobs | BillionSkills';
		$data['pageName'] = 'jobs';
		$this->load->view('index', $data);
	}
	public function getFilteredjobs()
	{
		$title = (trim($_GET['search'])=="")? "" : trim($_GET['search']);
		$location = $_GET['location'];
		$type = $_GET['type'];
		$skills = $_GET['skills'];
		$industry = $_GET['industry'];
		$position = $_GET['position'];
		$degree = $_GET['degree'];
		$data['jobs'] = $this->user_model->get_all_filter_jobs($title, $location, $type, $skills, $industry, $position, $degree);
		return $this->load->view('jobs_list', $data);
	}
	public function job_details()
	{
		if(isset($_GET['id'])){
			$id=base64_decode($_GET['id']);
			$data['job'] = $this->user_model->get_job_details($id);
			if(!empty($data['job'])){
				$data['pageTitle'] = trim($data['job'][0]->title).' | BillionSkills';
				$data['pageName'] = 'job_details';
				$this->load->view('index', $data);
			}else{
				redirect(base_url('home/jobs'));
			}
		}else{
			redirect(base_url('home/jobs'));
		}
	}
	/*=======================================================*/
	
	public function blogs()
	{
		$data['pageTitle'] = 'Blogs | BillionSkills';
		$data['pageName'] = 'blogs';
		$data['blogs'] = $this->user_model->get_all_blogs();
		$this->load->view('index', $data);
	}
	public function blog_details($id="")
	{
		if($id==""){
			redirect(base_url('home/blogs'));
		}else{
			$data['blog'] = $this->user_model->get_blog_details_by_id($id);
			$data['skills'] = $this->user_model->get_blogs_skills($data['blog'][0]->skills);
			$data['category'] = $this->user_model->get_blogs_categories($data['blog'][0]->categories);
			$data['othr_blogs'] = $this->user_model->get_other_blogs_by_id($id);
			$data['pageTitle'] = 'Blogs | BillionSkills';
			$data['pageName'] = 'blog-details';
			
			$this->load->view('index', $data);
		}
	}
	
}
